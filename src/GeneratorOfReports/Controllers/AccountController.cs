﻿using GeneratorOfReports.Authentication;
using GeneratorOfReports.Models;
using GeneratorOfReports.Resources;
using GeneratorOfReports.Services;
using GeneratorOfReports.Utilities;
using GeneratorOfReports.ViewModels;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace GeneratorOfReports.Controllers
{
    [AllowAnonymous]
    public class AccountController : BaseController
    {
        #region Properties

        private readonly string authentiacationCookieName = "GORAuthentiacation_Cookie";

        private readonly UserService _userService;

        #endregion

        #region Constructor

        public AccountController()
        {
            _userService = new UserService(UnitOfWork);
        }

        #endregion

        #region Login 

        public ActionResult Login()
        {
            NecessaryLogout();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                // check if user with password and login exist
                var user = _userService.ValidateUser(model.Login, model.Password);
                if (user == null)
                {
                    ModelState.AddModelError("allErrors", Translation.Account_Login_ErrorMessage);
                    return View(model);
                }

                MemberSerializeModel userModel = CreateGORMemberSerializeModel(user);
                CreateAuthenticationCookie(authentiacationCookieName, userModel);
                NotificationUtility.Add(new Notification("success", Translation.Account_Login, Translation.Account_Login_SuccessMessage));
                return RedirectToAction("Home", "Home", null);
            }
            return View(model);
        }

        #endregion

        #region Logout

        [Authorize]
        public ActionResult Logout()
        {
            NecessaryLogout();
            NotificationUtility.Add(new Notification("success", Translation.Account_Logout, Translation.Account_Logout_SuccessMessage));
            return RedirectToAction("Login", "Account", null);
        }

        /// <summary>
        /// If the login user want to do some action available only for logout user, will be unsign from his account.
        /// </summary>
        [NonAction]
        private void NecessaryLogout()
        {
            if (User.Identity.IsAuthenticated)
            {
                Response.Cookies.Add(new HttpCookie(authentiacationCookieName, "") { Expires = DateTime.Now.AddYears(-1) });
                FormsAuthentication.SignOut();
            }
        }

        #endregion

        #region Forgotten Password

        public ActionResult ForgottenPassword()
        {
            NecessaryLogout();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgottenPassword(ForgottenPasswordViewModel model)
        {
            string passwordToken = Encryption.GeneratetPasswordToken;
            User user = _userService.GetUserByEmail(model.Email);
            if (ModelState.IsValid && user != null)
            {
                _userService.SetPasswordToken(model.Email, passwordToken);
                String originalPath = new Uri(HttpContext.Request.Url.AbsoluteUri).OriginalString;
                string path = originalPath.Substring(0, originalPath.LastIndexOf("/"));
                path = path.Substring(0, path.LastIndexOf("/"));
                string resetPasswordPath = path + "/Account/ResetPassword?guid=" + passwordToken;
                string body = string.Format(Translation.Account_ForgottenPassword_Email_Text,
                                            user.FirstName + " " + user.Surname,
                                            DateTime.Today.ToString("D"),
                                            resetPasswordPath);
                bool sended = MailUtility.SendEmail(model.Email, Translation.Account_ForgottenPassword, body);
                if (sended)
                {
                    NotificationUtility.Add(new Notification("success", Translation.Global_Email_Send, Translation.Global_Email_Send_SuccessMessage));
                    return View("Login");
                }
            }
            NotificationUtility.Add(new Notification("error", Translation.Global_Email_Send, Translation.Global_Email_Send_ErrorMessage));
            return View(model);
        }

        #endregion

        #region Reset Password

        [HttpGet]
        public ActionResult ResetPassword(string guid)
        {
            NecessaryLogout();
            return View(new ResetPasswordViewModel { Code = guid });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid && _userService.ResetPassword(model.Code, model.Password))
            {
                NotificationUtility.Add(new Notification("success", Translation.Account_ResetPassword, Translation.Account_ResetPassword_SuccessMessage));
                return RedirectToAction("Login", "Account", null);
            }
            NotificationUtility.Add(new Notification("error", Translation.Account_ResetPassword, Translation.Account_ResetPassword_ErrorMessage));
            return View(model);
        }

        #endregion

        #region User Localization

        [Authorize]
        [HttpGet]
        public ActionResult ChangeRole(string role)
        {
            var user = (User as Principal);
            if (_userService.HasRoleInCompany(user.ID, role))
            {
                user.CurrentRole = role;
                MemberSerializeModel userModel = CreateGORMemberSerializeModel(user);
                CreateAuthenticationCookie(authentiacationCookieName, userModel);
                NotificationUtility.Add(new Notification("success", Translation.Account_Role_Change, Translation.Account_Role_Change_SuccessMessage));
            }
            return RedirectToAction("Home", "Home", null);
        }

        [Authorize]
        [HttpGet]
        public ActionResult ChangeCompany(string company)
        {
            var user = (User as Principal);
            var userLocalityRole = _userService.WorksInCompany(user.ID, company);
            if (userLocalityRole != null)
            {
                user.CurrentLocality = company;
                user.CurrentLocalityID = userLocalityRole.LocalityID;
                user.Roles = _userService.GetRolesInComapny(user.ID, company);
                user.CurrentRole = user.Roles[0];
                MemberSerializeModel userModel = CreateGORMemberSerializeModel(user);
                CreateAuthenticationCookie(authentiacationCookieName, userModel);
                NotificationUtility.Add(new Notification("success", Translation.Account_Company_Change, Translation.Account_Company_Change_SuccessMessage));
            }
            return RedirectToAction("Home", "Home", null);
        }

        #endregion

        #region Utility functions

        [NonAction]
        private void CreateAuthenticationCookie(string name, MemberSerializeModel user)
        {
            string userData = JsonConvert.SerializeObject(user);
            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1, user.FirstName, DateTime.Now, DateTime.Now.AddMinutes(30), false, userData);
            string enTicket = FormsAuthentication.Encrypt(authTicket);
            HttpCookie cookie = new HttpCookie(name, enTicket);
            Response.Cookies.Add(cookie);
        }

        [NonAction]
        private MemberSerializeModel CreateGORMemberSerializeModel(Member user)
            => new MemberSerializeModel()
            {
                ID = user.ID,
                FirstName = user.FirstName,
                LastName = user.LastName,
                CurrentRole = user.CurrentRole,
                CurrentLocality = user.CurrentLocality,
                CurrentLocalityID = user.CurrentLocalityID,
                Roles = user.Roles,
                Localities = user.Localities
            };

        [NonAction]
        private MemberSerializeModel CreateGORMemberSerializeModel(Principal user)
            => new MemberSerializeModel()
            {
                ID = user.ID,
                FirstName = user.FirstName,
                LastName = user.LastName,
                CurrentRole = user.CurrentRole,
                CurrentLocality = user.CurrentLocality,
                CurrentLocalityID = user.CurrentLocalityID,
                Roles = user.Roles,
                Localities = user.Localities,
            };

        #endregion
    }
}