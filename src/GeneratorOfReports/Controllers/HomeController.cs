﻿using GeneratorOfReports.Models;
using GeneratorOfReports.Resources;
using GeneratorOfReports.Services;
using GeneratorOfReports.Utilities;
using GeneratorOfReports.ViewModels;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace GeneratorOfReports.Controllers
{
    [AllowAnonymous]
    public class HomeController : BaseController
    {
        private readonly UserService _userService;

        public HomeController()
        {
            _userService = new UserService(UnitOfWork);
        }

        public ActionResult Home() => View();

        public ActionResult HelpCenter()
        {
            if (User.Identity.IsAuthenticated)
            {
                User user = _userService.Get((User as Authentication.Principal).ID);
                HelpCenterViewModel model = new HelpCenterViewModel { Email = user.Email };
                return View(model);
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HelpCenter(HelpCenterViewModel model)
        {
            if (ModelState.IsValid)
            {
                string body = string.Format(Translation.HelpCenter_Email_Text,
                                            model.Email,
                                            model.Subject,
                                            model.Message);
                MailUtility.SendEmail(MailUtility.EMAIL, Translation.Global_HelpCenter, body);
                NotificationUtility.Add(new Notification("success", Translation.Global_Email_Send, Translation.Global_Email_Send_SuccessMessage));
                return RedirectToAction("HelpCenter", "Home", null);
            }

            NotificationUtility.Add(new Notification("error", Translation.Global_Email_Send, Translation.Global_Email_Send_ErrorMessage));
            return View(model);
        }

        public ActionResult ChangeLanguage(string lang, string ReturnUrl = "")
        {
            if (lang != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(lang);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(lang);
            }

            HttpCookie cookie = new HttpCookie("OG_GOR_Language") { Value = lang };
            Response.Cookies.Add(cookie);

            return Redirect(ReturnUrl);
        }
    }
}