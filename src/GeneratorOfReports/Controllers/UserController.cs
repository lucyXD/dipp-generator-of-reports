﻿using GeneratorOfReports.Authentication;
using GeneratorOfReports.Models;
using GeneratorOfReports.Resources;
using GeneratorOfReports.Services;
using GeneratorOfReports.Utilities;
using GeneratorOfReports.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GeneratorOfReports.Controllers
{
    [Authorize]
    public class UserController : BaseController
    {
        #region Properties

        private readonly UserService _userService;
        private readonly AuditService _auditService;
        private readonly UserLocalityRoleService _userLocalityRoleService;

        #endregion

        #region Constructor

        public UserController()
        {
            _userService = new UserService(UnitOfWork);
            _auditService = new AuditService(UnitOfWork);
            _userLocalityRoleService = new UserLocalityRoleService(UnitOfWork);
        }

        #endregion

        #region User Profile

        public new ActionResult Profile()
        {
            User user = _userService.Get((User as Principal).ID);
            UserDetailsViewModel model = CreateProfileViewModel(user);
            return View(model);
        }

        [HttpPost]
        public ActionResult UpdateProfile(EditProfileViewModel model)
        {
            int ID = (User as Principal).ID;

            // didn't set to change password
            if (!model.ChangePasswordViewModel.ChangePassword)
            {
                ModelState["model.ChangePasswordViewModel.ChangePassword"].Errors.Clear();
                ModelState["model.ChangePasswordViewModel.Password"].Errors.Clear();
                ModelState["model.ChangePasswordViewModel.ConfirmPassword"].Errors.Clear();
            }

            // user cannot change personal number himself
            ModelState["model.User.PersonalNumber"].Errors.Clear();

            FetchErrors();

            // check fo uniqe login and email
            var user = _userService.Get(ID);
            if (!user.Login.Equals(model.User.Login)) CheckUniqueValues("Login", "User.Login", model.User.Login, 0);
            if (!user.Email.Equals(model.User.Email)) CheckUniqueValues("Email", "User.Email", model.User.Email, 0);

            if (ModelState.IsValid)
            {
                _userService.UpdateUser(ID, model);
                NotificationUtility.Add(new Notification("success", Translation.User_EditProfile, Translation.User_EditProfile_SuccessMessage));
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return PartialView("Modals/_EditProfileModal", model);
        }

        #endregion

        #region Overview

        [Authorize(Roles = RolesConstants.USER_ADMIN)]
        public ActionResult Overview() => View();

        [HttpPost]
        public ActionResult LoadUser(int ID, string partialName)
        {
            UserViewModel model = CreateUserViewModel(_userService.Get(ID));
            return PartialView(partialName, model);
        }

        #endregion

        #region Edit

        [HttpPost]
        public ActionResult Edit(UserViewModel model, List<UserLocalityRole> localityRoles)
        {
            model.LocalityWithRoles = localityRoles;

            if (ModelState.IsValid)
            {
                // check fo uniqe login and email
                var user = _userService.Get(model.ID);
                if (user == null) return Json(false, JsonRequestBehavior.AllowGet);
                if (!user.Login.Equals(model.Login)) CheckUniqueValues("Login", model.Login, 0);
                if (!user.Email.Equals(model.Email)) CheckUniqueValues("Email", model.Email, 0);
                if (!user.PersonalNumber.Equals(model.PersonalNumber)) CheckUniqueValues("PersonalNumber", model.PersonalNumber, 0);
                if (ModelState.IsValid)
                {
                    _userService.UpdateUser(user, model);
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            FetchErrors();
            return PartialView("Modals/_UserEditModal", model);
        }

        #endregion

        #region Create

        [HttpPost]
        public ActionResult LoadCreateUserModal()
        {
            UserViewModel model = new UserViewModel();
            return PartialView("Modals/_UserCreateModal", model);
        }

        [HttpPost]
        public ActionResult Create(UserViewModel model, List<UserLocalityRole> localityRoles)
        {
            model.LocalityWithRoles = localityRoles;

            if (ModelState.IsValid && CheckUniqueValues(model, 0))
            {
                string passwordToken = Encryption.GeneratetPasswordToken;
                _userService.CreateUser(model, localityRoles, passwordToken);

                // Send verification email to user with settings password link
                _userService.SetPasswordToken(model.Email, passwordToken);

                String originalPath = new Uri(HttpContext.Request.Url.AbsoluteUri).OriginalString;
                string path = originalPath.Substring(0, originalPath.LastIndexOf("/"));
                path = path.Substring(0, path.LastIndexOf("/"));
                string resetPasswordPath = path + "/Account/ResetPassword?guid=" + passwordToken;

                string body = string.Format(Translation.Accout_CreatePassword_Email_Text,
                                            model.FirstName + " " + model.Surname,
                                            DateTime.Today.ToString("D"),
                                            resetPasswordPath);
                MailUtility.SendEmail(model.Email, Translation.Accout_CreatePassword_Email_Subject, body);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            FetchErrors();
            return PartialView("Modals/_UserCreateModal", model);
        }

        #endregion

        #region Searching

        /// <summary> Load list of users to DataTable. </summary>
        /// <returns> Json with list of users. </returns>
        [HttpPost]
        [ValidateInput(false)]
        [Authorize(Roles = RolesConstants.USER_ADMIN)]
        public ActionResult LoadUsersTableData()
        {
            var draw = Request.Form.GetValues("draw")?.FirstOrDefault();
            var start = Request.Form.GetValues("start")?.FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 10;
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
            var search = Request.Form.GetValues("search[value]")?[0];
            int skip = start != null ? Convert.ToInt32(start) : 0;
            UserSearchViewModel model = FormattingUtility.FormatJson<UserSearchViewModel>(search);
            IEnumerable<User> users = _userService.Search(model, sortColumn, sortColumnDir);
            int recordsTotal = users.Count();
            var data = users
                .Skip(skip)
                .Take(pageSize)
                .Select(u => new
                {
                    u.ID,
                    u.Login,
                    u.FirstName,
                    u.Surname,
                    u.Email
                });
            return Json(new { draw, recordsFiltered = recordsTotal, recordsTotal, data }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Audits

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult LoadAudits(int ID)
        {
            SearchAuditsViewModel model = _auditService.CreateSearchAuditsViewModel(ID);
            return PartialView("Parts/_Audits", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SearchAudits(SearchAuditsViewModel search, int ID, int? PageNumber)
        {
            var actions = _auditService.GetAll(PageNumber, _auditService.Search(ID, search));

            if (actions.Any())
            {
                var model = new AuditsViewModel { HistoryOfActions = _auditService.Format(actions) };
                return PartialView("Tables/_AuditsTable", model);
            }

            return Content(string.Empty);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Audits(SearchAuditsViewModel search, int? PageNumber)
        {
            int ID = (User as Principal).ID;
            return SearchAudits(search, ID, PageNumber);
        }

        #endregion

        #region Utility functions

        private UserDetailsViewModel CreateProfileViewModel(User user)
            => new UserDetailsViewModel
            {
                User = CreateUserViewModel(user),
                EditUser = new EditProfileViewModel { User = CreateUserViewModel(user) },
                SearchAuditsViewModel = _auditService.CreateSearchAuditsViewModel(user.ID)
            };

        private UserViewModel CreateUserViewModel(User user)
        {
            List<UserLocalityRole> localityWithRoles = _userLocalityRoleService.GetByUserID(user.ID);
            return new UserViewModel
            {
                ID = user.ID,
                Login = user.Login,
                DateOfBirth = user.DateOfBirth,
                Surname = user.Surname,
                Title = user.Title,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                PersonalNumber = user.PersonalNumber,
                FirstName = user.FirstName,
                IsActive = user.IsActive,
                LocalityWithRoles = localityWithRoles,
                LocalityWithRolesDict = _userLocalityRoleService.FormatLocalityWithRoles(localityWithRoles)
            };
        }

        #endregion

        #region Validation

        private bool CheckUniqueValues(UserViewModel model, int number)
        {
            // Login
            bool result = CheckUniqueValues("Login", model.Login, number);

            // Email
            result = CheckUniqueValues("Email", model.Email, number) && result;

            // Personal Number
            result = CheckUniqueValues("PersonalNumber", model.PersonalNumber, number) && result;

            return result;
        }

        private bool CheckUniqueValues(string field, string value, int number)
            => CheckUniqueValues(field, field, value, number);

        private bool CheckUniqueValues(string field, string errorfield, string value, int number)
        {
            bool isUnique = true;
            switch (field)
            {
                case "Email":
                    isUnique = _userService.NumberOfEmail(value, number);
                    break;
                case "Login":
                    isUnique = _userService.NumberOfLogin(value, number);
                    break;
                case "PersonalNumber":
                    isUnique = _userService.NumberOfPersonalNumber(value, number);
                    break;
            }
            if (!isUnique)
            {
                ModelState.AddModelError(errorfield, Translation.User_Login_ErrorMessage_Unique);
                return false;
            }
            return true;
        }

        #endregion
    }
}