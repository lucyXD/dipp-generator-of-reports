﻿using GeneratorOfReports.Authentication;
using GeneratorOfReports.Services;
using System.Linq;
using System.Web.Mvc;

namespace GeneratorOfReports.Controllers
{
    [Authorize]
    public class AuthorizationController : BaseController
    {
        #region Properties

        private readonly LocalityService _localityService;

        #endregion

        #region Constructor

        public AuthorizationController()
        {
            _localityService = new LocalityService(UnitOfWork);
        }

        #endregion

        #region Table

        [ValidateInput(false)]
        public ActionResult LoadAuthorizationTableData()
        {
            int exceptID = (User as Principal).CurrentLocalityID;
            var localities = _localityService.GetAll().Where(x => x.ID != exceptID);
            int recordsTotal = localities.Count();
            var data = localities.Select(x => new
            {
                x.ID,
                x.Name,
                All = false,
                Visibility = false,
                Edit = false,
                Remove = false
            });
            return Json(new { recordsFiltered = recordsTotal, recordsTotal, data }, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}