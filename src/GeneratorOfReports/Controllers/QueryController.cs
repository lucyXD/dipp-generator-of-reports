﻿using GeneratorOfReports.Authentication;
using GeneratorOfReports.Models;
using GeneratorOfReports.Services;
using GeneratorOfReports.Utilities;
using GeneratorOfReports.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using GeneratorOfReports.ContextManagement;
using GeneratorOfReports.Resources;
using Newtonsoft.Json;
using GeneratorOfReports.ParameterManagement;

namespace GeneratorOfReports.Controllers
{
    [Authorize(Roles = RolesConstants.QUERY_ADMIN)]
    public class QueryController : BaseController
    {
        #region Properties

        private readonly QueryService _queryService;
        private readonly ReportService _reportService;
        private readonly ParameterService _parameterService;
        private readonly LocalityService _localityService;
        private readonly AuthorizationService _authorizationService;

        #endregion

        #region Constructor

        public QueryController()
        {
            _queryService = new QueryService(UnitOfWork);
            _reportService = new ReportService(UnitOfWork);
            _parameterService = new ParameterService(UnitOfWork);
            _localityService = new LocalityService(UnitOfWork);
            _authorizationService = new AuthorizationService(UnitOfWork);
        }

        #endregion

        #region Overview

        public ActionResult Overview() => View();

        #endregion

        #region Modify Query

        #region Create

        public ActionResult Create()
        {
            QueryModifyViewModel model = null;
            var databases = TypeUtility.GetAll<ContextType>();
            if (databases.Count() == 1)
            {
                model = new QueryModifyViewModel { Database = databases.First().GetDB() };
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(QueryModifyViewModel model)
        {
            Error error = _queryService.CheckQuery(model);
            if (error == null)
            {
                var user = (User as Principal);
                List<AuthorizationQuery> authorizations = _authorizationService.CreateQueryAuthorization(model.Authorization, user.CurrentLocalityID);
                Query query = _queryService.Create(model, authorizations, user.ID);
                if (model.CreateReport) _reportService.CreateSingleReport(query);
                NotificationUtility.Add(new Notification("success", Translation.Query_Create, Translation.Query_Create_SuccessMessage));
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Remove 

        [HttpPost]
        public ActionResult Remove(int ID)
        {
            bool result = _queryService.Remove(ID, (User as Principal).CurrentLocalityID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Copy

        public ActionResult Copy(int ID)
        {
            QueryModifyViewModel model = _queryService.CreateModifyViewModel(ID, (User as Principal).CurrentLocalityID);
            return View("Create", model);
        }

        #endregion

        #region Edit 

        public ActionResult Edit(int ID)
        {
            QueryModifyViewModel model = _queryService.CreateModifyViewModel(ID, (User as Principal).CurrentLocalityID);
            if (model != null)
            {
                return View(model);
            }
            return View("Overview");
        }

        [HttpPost]
        public ActionResult Edit(QueryModifyViewModel model)
        {
            Error error = _queryService.CheckQuery(model);
            if (ModelState.IsValid && error == null)
            {
                var authorizations = _authorizationService.GetAuthorizationQueries(model.Authorization, (User as Principal).CurrentLocalityID);
                _queryService.Update(model, authorizations);
                NotificationUtility.Add(new Notification("success", Translation.Query_Edit, Translation.Query_Edit_SuccessMessage));
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion

        #region Database

        public ActionResult LoadSelectDatabaseModal()
        {
            SelectDBViewModel model = new SelectDBViewModel
            {
                Databases = TypeUtility.GetSelectList<ContextType>()
            };
            return PartialView("Modals/_DBSelectModal", model);
        }

        [HttpPost]
        public ActionResult SelectDatabase(SelectDBViewModel model)
        {
            Database database = null;
            if (ModelState.IsValid)
            {
                ContextType context = ContextWrapper.GetValue<ContextType>(model.Database);
                database = context.GetDB();
            }
            return PartialView("Parts/_DBStructure", database);
        }

        #endregion

        #region Details 

        [HttpPost]
        public ActionResult LoadDetails(int ID)
        {
            QueryDetailsViewModel model = _queryService.CreateQueryDedails(ID, (User as Principal).CurrentLocalityID);
            return PartialView("Parts/_QueryDetails", model);
        }

        #endregion

        #region Parameters

        [HttpPost]
        public ActionResult AddParameter(string name)
        {
            ParameterViewModel model = new ParameterViewModel
            {
                Name = name,
                SQLTypeSelectList = ParameterTypes.GetSelectList(),
                IsParameter = true,
                Hash = Guid.NewGuid().ToString()
            };
            return PartialView("Forms/_ParameterForm", model);
        }

        [HttpPost]
        public ActionResult CheckParameter(ParameterViewModel model)
        {
            if (!model.IsParameter)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else if (ModelState.IsValid)
            {
                // check default value
                ParameterType t = ParameterTypes.GetValue(model.Keyword).Type;
                if (t.IsValidDefault(model.ModelJson))
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                ModelState.AddModelError("value", Translation.Parameter_ErrorMessage_WrongValue);
            }
            model.SQLTypeSelectList = ParameterTypes.GetSelectList();
            return PartialView("Forms/_ParameterForm", model);
        }

        [HttpPost]
        public ActionResult CreateParameterDefaultForm(string type)
        {
            ParameterTypes types = ParameterTypes.GetValue(type);
            if (types == null)
            {
                return Json(Translation.Query_Parameter_DefaultValue_SelectType, JsonRequestBehavior.AllowGet);
            }
            ParameterType t = types.Type;
            return PartialView(t.GetDefaultView(), t.GetModel());
        }

        [ChildActionOnly]
        public ActionResult LoadParameterDefaultForm(string keyword, string modelJson)
        {
            var t = ParameterTypes.GetValue(keyword).Type;
            return PartialView(t.GetDefaultView(), t.GetDefaultModel(modelJson));
        }

        [HttpPost]
        public ActionResult LoadMultiselect(string modelJson)
        {
            var t = ParameterTypes.DB_MULTISELECT.Type;
            return PartialView(t.GetDefaultView(), t.GetDefaultModel(modelJson));
        }

        #endregion

        #region Authorizations

        [HttpPost]
        public ActionResult LoadQueryAuthorizationTableData(int ID)
        {
            if (ID == 0) return RedirectToAction("LoadAuthorizationTableData", "Authorization");

            // user cannot remove own rights 
            int exceptID = (User as Principal).CurrentLocalityID;
            var localities = _queryService.GetAuthorizations(ID).Where(x => x.LocalityID != exceptID).ToList();
            // add possible missings authorization
            var available = _localityService.GetAll();
            var shoulAdd = available.Where(f1 => localities.All(f2 => f2.LocalityID != f1.ID) && f1.ID != exceptID);
            var denyAll = _authorizationService.GetDenyAll();
            localities.AddRange(shoulAdd.Select(x => new AuthorizationQuery { LocalityID = x.ID, Locality = x, Authorization = denyAll }));
            int recordsTotal = localities.Count;
            var data = localities.Select(x => new
            {
                x.Locality.ID,
                x.Locality.Name,
                x.Authorization.Visibility,
                x.Authorization.Edit,
                x.Authorization.Remove,
                All = x.Authorization.Visibility && x.Authorization.Edit && x.Authorization.Remove
            });
            return Json(new { recordsFiltered = recordsTotal, recordsTotal, data }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Validation 

        [HttpPost]
        public ActionResult CheckBasicsSettingsForm(QueryViewModel model)
        {
            ModelState["model.Text"].Errors.Clear();
            if (ModelState.IsValid)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            FetchErrors();
            return PartialView("Forms/_QueryBasicsSettingsForm", model);
        }

        [HttpPost]
        public ActionResult CheckQuery(string query, string database, List<ParameterViewModel> parameters)
        {
            Error error = _queryService.CheckQuery(query, database, parameters);
            if (error != null)
            {
                return PartialView("Tables/_ErrorsTable", new List<Error> { error });
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EvaluateQuery(string query, string database, List<ParameterViewModel> parameters)
        {
            var data = _queryService.EvaluateQuery(query, database, parameters);
            if (data != null)
            {
                var settings = new JsonSerializerSettings().AddSqlConverters();
                var JsonResult = JsonConvert.SerializeObject(new { status = true, result = data }, settings);
                return Content(JsonResult, "application/json");
            }
            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Searching 

        [HttpPost]
        public ActionResult PinTableData(string db, string name)
        {
            ContextType context = ContextWrapper.GetValue<ContextType>(db);
            List<object> data = context.GetTableData(name);
            if (data != null)
            {
                var JsonResult = JsonConvert.SerializeObject(new { status = true, data });
                return Content(JsonResult, "application/json");
            }
            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult LoadQueriesTableData()
        {
            var draw = Request.Form.GetValues("draw")?.FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 10;
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
            var search = Request.Form.GetValues("search[value]")?[0];
            int skip = start != null ? Convert.ToInt32(start) : 0;
            QuerySearchViewModel model = FormattingUtility.FormatJson<QuerySearchViewModel>(search);
            int localityID = (User as Principal).CurrentLocalityID;
            IEnumerable<Query> queries = _queryService.GetVisibleQueries(localityID);
            queries = _queryService.Search(queries, model, sortColumn, sortColumnDir);
            int recordsTotal = queries.Count();
            var data = queries
                .Skip(skip)
                .Take(pageSize)
                .Select(x => new
                {
                    x.ID,
                    x.Name,
                    x.Database,
                    CanDelete = _queryService.CanRemove(x, localityID),
                    CanEdit = _queryService.CanEdit(x, localityID)
                });
            return Json(new { draw, recordsFiltered = recordsTotal, recordsTotal, data }, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}