﻿using System.Collections.Generic;
using System.Web.Mvc;
using GeneratorOfReports.Models;

namespace GeneratorOfReports.Controllers
{
    public abstract class BaseController : Controller
    {
        #region Properties

        public UnitOfWork UnitOfWork { get; private set; }

        #endregion

        #region Constructors

        protected BaseController()
        {
            UnitOfWork = new UnitOfWork();
        }

        #endregion

        #region Utility methods

        public void FetchErrors()
        {
            Dictionary<string, List<string>> errors = new Dictionary<string, List<string>>();

            ModelStateDictionary stateDict = ViewData.ModelState;
            foreach (KeyValuePair<string, ModelState> modelState in stateDict)
            {
                List<string> errorList = new List<string>();
                foreach (ModelError error in modelState.Value.Errors)
                {
                    errorList.Add(error.ErrorMessage);
                }
                errors[modelState.Key] = errorList;
            }

            foreach (KeyValuePair<string, List<string>> modelState in errors)
            {
                foreach (string error in modelState.Value)
                {
                    ModelState.AddModelError(modelState.Key.Substring(modelState.Key.IndexOf(".") + 1), error);
                }
            }
        }

        #endregion
    }
}