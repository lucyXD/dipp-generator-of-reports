﻿using GeneratorOfReports.Authentication;
using GeneratorOfReports.Models;
using GeneratorOfReports.Services;
using GeneratorOfReports.Utilities;
using GeneratorOfReports.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using GeneratorOfReports.ReportGenerator;
using GeneratorOfReports.Resources;
using GeneratorOfReports.ReportGenerator.POCO;
using GeneratorOfReports.ParameterManagement;

namespace GeneratorOfReports.Controllers
{
    [Authorize(Roles = RolesConstants.REPORT_ADMIN)]
    public class ReportController : BaseController
    {
        #region Properties

        private readonly ReportService _reportService;
        private readonly QueryService _queryService;
        private readonly ParameterService _parameterService;
        private readonly AuthorizationService _authorizationService;
        private readonly LocalityService _localityService;

        #endregion

        #region Constructor

        public ReportController()
        {
            _reportService = new ReportService(UnitOfWork);
            _queryService = new QueryService(UnitOfWork);
            _parameterService = new ParameterService(UnitOfWork);
            _localityService = new LocalityService(UnitOfWork);
            _authorizationService = new AuthorizationService(UnitOfWork);
        }

        #endregion

        #region Overview

        public ActionResult Overview() => View();

        #endregion

        #region Details

        [HttpPost]
        public ActionResult LoadReport(int ID, string partialName)
            => PartialView(partialName, CreateDetailsViewModel(ID));

        [NonAction]
        private ReportDetailsViewModel CreateDetailsViewModel(int ID)
        {
            Report report = _reportService.Get(ID);
            int localityID = (User as Principal).CurrentLocalityID;
            bool canEdit = _reportService.CanEdit(report, localityID);

            return new ReportDetailsViewModel
            {
                Report = _reportService.CreateReportViewModel(report),
                CanEdit = canEdit,
                Queries = report.ReportQueries.Select(x =>
                {
                    var q = _queryService.CreateQueryViewModel(x.Query);
                    q.Count = x.Count;
                    return q;
                }),
                Authorization = canEdit ? report.AuthorizationReports.Select(x => _authorizationService.CreateAuthorization(x.Authorization, x.Locality.Name)) : null
            };
        }

        #endregion

        #region Modify 

        #region Remove

        [HttpPost]
        public ActionResult Remove(int ID)
        {
            bool removed = _reportService.Remove(ID, (User as Principal).CurrentLocalityID);
            return Json(removed, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Create 

        public ActionResult Create()
            => View(_reportService.CreateReportCreateViewModel());

        [HttpPost]
        public ActionResult Create(ReportCreateViewModel model)
        {
            if (ModelState.IsValid && CheckIsSmallerOrder(model.Report.Order, 1) &&
                _queryService.ValidCreateQueries(model.Queries))
            {
                var user = (User as Principal);
                var authorizations = _authorizationService.CreateAuthorizationReports(model.Authorization, user.CurrentLocalityID);
                _reportService.AddReport(model, authorizations, user.ID);
                NotificationUtility.Add(new Notification("success", Translation.Report_Create, Translation.Report_Create_SuccessMessage));
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Edit

        [HttpPost]
        public ActionResult Edit(ReportCreateViewModel model)
        {
            if (ModelState.IsValid && CheckIsSmallerOrder(model.Report.Order, 0) &&
                _queryService.ValidCreateQueries(model.Queries))
            {
                var authorizations = _authorizationService.CreateAuthorizationReports(model.Authorization, (User as Principal).CurrentLocalityID);
                _reportService.Update(model, authorizations);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Basics Settings

        [HttpPost]
        public ActionResult CheckBasicsSettingsForm(ReportViewModel model)
        {
            if (ModelState.IsValid && CheckIsSmallerOrder(model.Order, 1))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            FetchErrors();
            return PartialView("Forms/_ReportBasicsSettingsForm", model);
        }

        #endregion

        #endregion

        #region Generate

        public ActionResult Generate(int? modelID)
        {
            if (modelID == null)
            {
                return View(new GenerateReportViewModel { Message = Translation.Report_Generate_Error_SelectReport });
            }

            int ID = (int)modelID;
            GenerateReportViewModel model;
            if (!_reportService.IsVisible(ID, (User as Principal).CurrentLocalityID))
            {
                model = new GenerateReportViewModel { Message = Translation.Report_Generate_Error_SelectReport };
            }
            else if (!_reportService.CanGenerate(ID))
            {
                model = new GenerateReportViewModel { Message = Translation.Report_Generate_Error_Valid };
            }
            else
            {
                model = CreateGenerateReportViewModel(ID);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Generate(GenerateReportViewModel model)
        {
            if (model != null && model.Report != null &&
                model.Settings != null && ValidQueries(model.Queries))
            {
                GeneratedReport queries = CreateGeneratedReport(model);
                Session["Download_Data"] = Generator.Generate(queries);
                Session["Download_Name"] = model.Report.Name + "." + model.Settings.Format;
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Download()
        {
            string name = Session["Download_Name"] as string;
            byte[] data = Session["Download_Data"] as byte[];
            return File(data, "application/octet-stream", name);
        }

        [ChildActionOnly]
        public ActionResult GenerateParameterTypeForm(ParameterViewModel parameter)
        {
            ParameterType t = ParameterTypes.GetValue(parameter.Keyword).Type;
            return PartialView(t.PartialName, t.GetViewModel(parameter.ModelJson, parameter.DefaultValueModel));
        }

        #region Formattors

        [NonAction]
        private GeneratedReport CreateGeneratedReport(GenerateReportViewModel model)
            => new GeneratedReport
            {
                Name = model.Report.Name,
                Description = model.Report.Description,
                UserCreated = (User as Principal).FirstName + " " + (User as Principal).LastName,
                Settings = model.Settings,
                Queries = _queryService.CreateGeneratedQuery(model.Queries)
            };

        [NonAction]
        private GenerateReportViewModel CreateGenerateReportViewModel(int ID)
        {
            Report report = _reportService.Get(ID);
            return new GenerateReportViewModel
            {
                Settings = new Settings { },
                FormatSelectList = FormattingUtility.GetEnumSelectList<Format>(),
                Report = _reportService.CreateReportViewModel(report),
                Queries = _queryService.CreateReportQueryViewModel(report.ReportQueries)
            };
        }

        #endregion

        #region Validators

        [HttpPost]
        public ActionResult ValidateQueryParameter(ParameterViewModel model)
        {
            if (ModelState.IsValid)
            {
                (bool valid, string message) = _parameterService.ValidParametr(model);

                if (valid)
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ModelState.AddModelError("Value", message);
                }
            }

            return PartialView("Forms/_QueryParameterForm", model);
        }

        [NonAction]
        private bool ValidQueries(IEnumerable<ReportQueryViewModel> queries)
         => queries != null &&
            queries.Any() &&
            queries.ToList().TrueForAll(x => _parameterService.ValidParameters(x.Parameters));

        #endregion

        #endregion

        #region Searching 

        [ChildActionOnly]
        public ActionResult GenerateSearchForm()
        {
            ReportSearchViewModel model = new ReportSearchViewModel
            {
                ValidSelectList = FormattingUtility.GetBooleanSelectList()
            };
            return PartialView("Forms/_ReportSearchForm", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult LoadReportsTableData()
        {
            var draw = Request.Form.GetValues("draw")?.FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 10;
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
            var search = Request.Form.GetValues("search[value]")?[0];
            int skip = start != null ? Convert.ToInt32(start) : 0;
            ReportSearchViewModel model = FormattingUtility.FormatJson<ReportSearchViewModel>(search);
            int localityID = (User as Principal).CurrentLocalityID;
            var reports = _reportService.GetVisibleReports(localityID);
            reports = _reportService.Search(reports, model, sortColumn, sortColumnDir);
            int recordsTotal = reports.Count();
            var data = reports
                .Skip(skip)
                .Take(pageSize)
                .Select(x => new
                {
                    x.ID,
                    x.Order,
                    x.Name,
                    x.Description,
                    CanRemove = _reportService.CanRemove(x, localityID),
                    CanEdit = _reportService.CanEdit(x, localityID),
                    CanGenerate = _reportService.CanGenerate(x.ID),
                    HasQueries = x.ReportQueries.Any()
                });
            return Json(new { draw, recordsFiltered = recordsTotal, recordsTotal, data }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Selected queries

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult LoadQueriesTableData(int? ID)
        {
            int localityID = (User as Principal).CurrentLocalityID;

            Dictionary<int, int> q = new Dictionary<int, int>();
            if (ID != null)
            {
                _reportService.Get((int)ID).ReportQueries.ToList().ForEach(x => q.Add(x.QueryID, x.Count));
            }

            var queries = _queryService.GetVisibleQueries(localityID);
            int recordsTotal = queries.Count();
            var data = queries
                .Select(x => new
                {
                    x.ID,
                    x.Name,
                    x.Description,
                    x.Database,
                    Selected = q.ContainsKey(x.ID),
                    Count = q.ContainsKey(x.ID) ? q[x.ID] : 0,
                    Validity = FormattingUtility.GetBooleanWord(x.Valid),
                    TimeCreate = FormattingUtility.GetDay(x.TimeCreate)
                });
            return Json(new { recordsFiltered = recordsTotal, recordsTotal, data }, JsonRequestBehavior.AllowGet);
        }

        [ChildActionOnly]
        public ActionResult GenerateSelectQueriesTable()
        {
            QuerySearchViewModel model = new QuerySearchViewModel
            {
                ValidSelectList = FormattingUtility.CreateSelectList(new List<string> { Translation.Global_Yes, Translation.Global_No })
            };
            return PartialView("Tables/_QueriesSelectTable", model);
        }

        #endregion

        #region Authorization

        [HttpPost]
        public ActionResult LoadReportAuthorizationTableData(int ID)
        {
            // user cannot remove own rights 
            int exceptID = (User as Principal).CurrentLocalityID;
            var localities = _reportService.GetAuthorizations(ID).Where(x => x.LocalityID != exceptID).ToList();
            // add possible missings authorization
            var available = _localityService.GetAll();
            var shoulAdd = available.Where(f1 => localities.All(f2 => f2.LocalityID != f1.ID && f1.ID != exceptID));
            var denyAll = _authorizationService.GetDenyAll();
            localities.AddRange(shoulAdd.Select(x => new AuthorizationReport { LocalityID = x.ID, Locality = x, Authorization = denyAll }));

            int recordsTotal = localities.Count;
            var data = localities.Select(x => new
            {
                x.Locality.ID,
                x.Locality.Name,
                x.Authorization.Visibility,
                x.Authorization.Edit,
                x.Authorization.Remove,
                All = x.Authorization.Visibility && x.Authorization.Edit && x.Authorization.Remove
            });
            return Json(new { recordsFiltered = recordsTotal, recordsTotal, data }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Order of Report

        [HttpPost]
        public ActionResult LoadEditOrderModal(int ID, int order)
        {
            int maximum = _reportService.GetMaximumOrder();
            ReportOrderViewModel model = new ReportOrderViewModel
            {
                ID = ID,
                Order = order,
                Maximum = maximum
            };
            return PartialView("Modals/_ReportEditOrderModal", model);
        }

        [HttpPost]
        public ActionResult EditOrder(ReportOrderViewModel model)
        {
            CheckIsSmallerOrder(model.Order, 0);
            if (ModelState.IsValid)
            {
                _reportService.UpdateOrder(model.ID, model.Order);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return PartialView("Modals/_ReportEditOrderModal", model);
        }

        [HttpPost]
        public ActionResult MoveOrder(int ID, int newOrder)
        {
            if (ID != 0 && newOrder != 0)
            {
                _reportService.UpdateOrder(ID, newOrder);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        private bool CheckIsSmallerOrder(int value, int plus)
        {
            if (value > _reportService.GetMaximumOrder() + plus)
            {
                ModelState.AddModelError("Report.Order", Translation.Report_Order_Maximum_ErrorMessage);
                return false;
            }
            return true;
        }

        #endregion
    }
}