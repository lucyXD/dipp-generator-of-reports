﻿using System.Web.Mvc;

namespace GeneratorOfReports.Controllers
{
    [AllowAnonymous]
    public class ErrorController : Controller
    {
        [HandleError()]
        public ActionResult Index(int code)
        {
            return View(code);
        }
    }
}