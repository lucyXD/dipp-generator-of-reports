﻿using GeneratorOfReports.Models;
using GeneratorOfReports.Resources;
using GeneratorOfReports.Services;
using GeneratorOfReports.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GeneratorOfReports.Controllers
{
    public class LocalityWithRolesController : BaseController
    {
        #region Properties

        private readonly RoleService _roleService;
        private readonly LocalityService _localityService;
        private readonly UserLocalityRoleService _userLocalityRoleService;

        #endregion

        #region Constructor

        public LocalityWithRolesController()
        {
            _roleService = new RoleService(UnitOfWork);
            _localityService = new LocalityService(UnitOfWork);
            _userLocalityRoleService = new UserLocalityRoleService(UnitOfWork);
        }

        #endregion

        #region Generators

        [ChildActionOnly]
        public ActionResult GenerateRolesSelection()
        {
            RoleSelectViewModel model = new RoleSelectViewModel { SelectList = _roleService.GetSelectList() };
            return PartialView("Selections/_RolesSelectPartial", model);
        }

        [ChildActionOnly]
        public ActionResult GenerateLocalitiesSelection()
        {
            LocalitySelectViewModel model = new LocalitySelectViewModel { SelectList = _localityService.GetSelectList() };
            return PartialView("Selections/_LocalitiesSelectPartial", model);
        }

        [HttpPost]
        public ActionResult GenerateLocalityWithRolesTable(List<UserLocalityRole> localitiesWithRoles)
        {
            List<LocalityWithRolesViewModel> model = CreateCompanyWithRolesViewModel(localitiesWithRoles);
            return PartialView("Tables/_LocalitiesWithRolesTable", model);
        }

        [HttpPost]
        public ActionResult GenerateLocalityWithRolesSelect(List<string> localities)
        {
            LocalityWithRolesViewModel model = BlankCompanyWithRolesViewModel(_localityService.GetSelectListExcept(localities));
            return PartialView("Selections/_LocalityWithRolesSelectPartial", model);
        }

        #endregion

        #region Editing Role

        [HttpPost]
        public ActionResult AddRoleToSelection(RoleSelectViewModel model)
        {
            model = AddRole(model);
            return PartialView("Selections/_RolesSelectPartial", model);
        }

        [HttpPost]
        public ActionResult AddRoleToList(LocalityWithRolesViewModel model)
        {
            model.RoleSelectViewModel = AddRole(model.RoleSelectViewModel);
            return PartialView("Selections/_LocalityWithRolesSelectPartial", model);
        }

        private RoleSelectViewModel AddRole(RoleSelectViewModel model)
        {
            if (model.ID != 0)
            {
                Role role = _roleService.Get(model.ID);

                if (model.SelectedList != null)
                {
                    model.SelectedList.Add(new RoleViewModel { ID = model.ID, Name = role.Name });
                }
                else
                {
                    model.SelectedList = new List<RoleViewModel> { new RoleViewModel { ID = model.ID, Name = role.Name } };
                }

                model.SelectList = _roleService.GetSelectListExcept(model.SelectedList);
                model.SelectList = model.SelectList.Any() ? model.SelectList : null;
            }
            return model;
        }

        [HttpPost]
        public ActionResult RemoveRoleFromSelection(RoleSelectViewModel model)
        {
            model = RemoveRole(model);
            return PartialView("Selections/_RolesSelectPartial", model);
        }

        [HttpPost]
        public ActionResult RemoveRoleFromList(LocalityWithRolesViewModel model)
        {
            model.RoleSelectViewModel = RemoveRole(model.RoleSelectViewModel);
            return PartialView("Selections/_LocalityWithRolesSelectPartial", model);
        }

        private RoleSelectViewModel RemoveRole(RoleSelectViewModel model)
        {
            if (model.ID != 0)
            {
                model.SelectedList = model.SelectedList.Where(x => x.ID != model.ID).ToList();
                model.SelectList = _roleService.GetSelectListExcept(model.SelectedList);
            }
            return model;
        }

        #endregion

        #region Editing Locality

        [HttpPost]
        public ActionResult AddLocalityToSelection(LocalitySelectViewModel model)
        {
            if (model.ID != 0)
            {
                Locality locality = _localityService.Get(model.ID);

                if (model.SelectedList != null)
                {
                    model.SelectedList.Add(new Locality { ID = model.ID, Name = locality.Name });
                }
                else
                {
                    model.SelectedList = new List<Locality> { new Locality { ID = model.ID, Name = locality.Name } };
                }

                model.SelectList = _localityService.GetSelectListExcept(model.SelectedList);
            }

            return PartialView("Selections/_LocalitiesSelectPartial", model);
        }

        [HttpPost]
        public ActionResult RemoveLocalityFromSelection(LocalitySelectViewModel model)
        {
            if (model.ID != 0)
            {
                model.SelectedList = model.SelectedList.Where(x => x.ID != model.ID).ToList();
                model.SelectList = _localityService.GetSelectListExcept(model.SelectedList);
            }
            return PartialView("Selections/_LocalitiesSelectPartial", model);
        }

        #endregion

        #region Edit Option

        [HttpPost]
        public ActionResult AddOption(LocalityWithRolesViewModel model)
        {
            if (model.LocalitySelectViewModel.ID == 0)
            {
                ModelState.AddModelError("LocalitySelectViewModel.ID", Translation.User_Company_ErrorMessage_Required);
            }
            else
            {
                model.LocalitySelectViewModel.Selected = model.LocalitySelectViewModel.SelectList?.FirstOrDefault(x => x.Value == model.LocalitySelectViewModel.ID.ToString()).Text;
                model.LocalitySelectViewModel.SelectList = null;
            }
            return PartialView("Selections/_LocalityWithRolesSelectPartial", model);
        }

        [HttpPost]
        public ActionResult RemoveOption(LocalityWithRolesViewModel model, String removedLocalityName, String removedLocalityID)
        {
            if (model.LocalitySelectViewModel.SelectList == null)
            {
                model = BlankCompanyWithRolesViewModel(new List<SelectListItem>() { new SelectListItem { Value = removedLocalityID, Text = removedLocalityName } });
            }
            else
            {
                model.LocalitySelectViewModel.SelectList.Add(new SelectListItem { Value = removedLocalityID, Text = removedLocalityName });
            }
            return PartialView("Selections/_LocalityWithRolesSelectPartial", model);
        }

        #endregion

        #region Utility functions

        private List<LocalityWithRolesViewModel> CreateCompanyWithRolesViewModel(List<UserLocalityRole> localityWithRoles)
        {
            localityWithRoles.ForEach(x => x.Locality = _localityService.Get(x.LocalityID));
            Dictionary<Locality, List<Role>> dict = _userLocalityRoleService.FormatLocalityWithRoles(localityWithRoles);
            List<LocalityWithRolesViewModel> result = new List<LocalityWithRolesViewModel>();

            foreach (KeyValuePair<Locality, List<Role>> duo in dict)
            {
                List<RoleViewModel> roles = _roleService.ConvertToViewModel(duo.Value);
                List<SelectListItem> rolesSelectList = _roleService.GetSelectListExcept(roles);
                result.Add(new LocalityWithRolesViewModel
                {
                    LocalitySelectViewModel = new LocalitySelectViewModel
                    {
                        ID = duo.Key.ID,
                        Selected = duo.Key.Name
                    },
                    RoleSelectViewModel = new RoleSelectViewModel
                    {
                        SelectedList = roles,
                        SelectList = rolesSelectList.Any() ? rolesSelectList : null
                    }
                });
            }

            LocalityWithRolesViewModel blankModel = BlankCompanyWithRolesViewModel(_localityService.GetSelectListExcept(dict.Keys.ToList()));
            if (blankModel != null)
            {
                result.Add(blankModel);
            }

            return result;
        }

        private LocalityWithRolesViewModel BlankCompanyWithRolesViewModel(List<SelectListItem> localities)
            => localities.Any()
                ? new LocalityWithRolesViewModel
                {
                    LocalitySelectViewModel = new LocalitySelectViewModel
                    {
                        Selected = " ",
                        SelectList = localities
                    },
                    RoleSelectViewModel = new RoleSelectViewModel
                    {
                        SelectList = _roleService.GetSelectList()
                    }
                }
                : null;

        #endregion
    }
}