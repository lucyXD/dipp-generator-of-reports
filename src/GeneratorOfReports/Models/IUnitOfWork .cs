﻿using System;

namespace GeneratorOfReports.Models
{
    public interface IUnitOfWork : IDisposable
    {
        int Commit();
        void Dispose();
    }
}
