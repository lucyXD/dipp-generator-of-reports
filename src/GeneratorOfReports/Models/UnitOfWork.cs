﻿using GeneratorOfReports.Repositories;

namespace GeneratorOfReports.Models
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Properties

        public GORContext Context { get; private set; }

        public UserRepository UserRepository { get; private set; }
        public AuditRepository AuditRepository { get; private set; }
        public LocalityRepository CompanyRepository { get; private set; }
        public RoleRepository RoleRepository { get; private set; }
        public UserLocalityRoleRepository UserLocalityRoleRepository { get; private set; }
        public ReportRepository ReportRepository { get; private set; }
        public QueryRepository QueryRepository { get; internal set; }
        public ParameterRepository ParameterRepository { get; private set; }
        public AuthorizationRepository AuthorizationRepository { get; internal set; }

        #endregion

        #region Constructors

        public UnitOfWork()
        {
            Context = new GORContext();
            UserRepository = new UserRepository(Context);
            AuditRepository = new AuditRepository(Context);
            RoleRepository = new RoleRepository(Context);
            CompanyRepository = new LocalityRepository(Context);
            UserLocalityRoleRepository = new UserLocalityRoleRepository(Context);
            ReportRepository = new ReportRepository(Context);
            QueryRepository = new QueryRepository(Context);
            ParameterRepository = new ParameterRepository(Context);
            AuthorizationRepository = new AuthorizationRepository(Context);
        }

        #endregion

        #region Methods

        public int Commit() => Context.SaveChanges();

        public void Dispose() => Context.Dispose();

        #endregion
    }
}