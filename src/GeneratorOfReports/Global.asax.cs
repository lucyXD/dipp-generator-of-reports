﻿using GeneratorOfReports.Authentication;
using Newtonsoft.Json;
using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace GeneratorOfReports
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            SqlServerTypes.Utilities.LoadNativeAssemblies(Server.MapPath("~/bin"));
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            SetLanguage();
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            if (exception != null || Response.StatusCode == 401)
            {
                var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
                Response.Redirect(urlHelper.Action("Index", "Error", new { code = Response.StatusCode }));
            }
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies["GORAuthentiacation_Cookie"];
            if (authCookie != null)
            {
                try
                {
                    FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                    MemberSerializeModel serializeModel = JsonConvert.DeserializeObject<MemberSerializeModel>(authTicket.UserData);
                    HttpContext.Current.User = new Principal(authTicket.Name)
                    {
                        ID = serializeModel.ID,
                        FirstName = serializeModel.FirstName,
                        LastName = serializeModel.LastName,
                        CurrentRole = serializeModel.CurrentRole,
                        CurrentLocality = serializeModel.CurrentLocality,
                        CurrentLocalityID = serializeModel.CurrentLocalityID,
                        Roles = serializeModel.Roles,
                        Localities = serializeModel.Localities
                    };
                }
                catch (CryptographicException)
                {
                    FormsAuthentication.SignOut();
                }
            }
        }

        #region Utility functions

        private static void SetLanguage()
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["OG_GOR_Language"];
            if (cookie != null && cookie.Value != null)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(cookie.Value);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(cookie.Value);
            }
            else
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("En");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("En");
            }
            CultureInfo info = new CultureInfo(Thread.CurrentThread.CurrentCulture.ToString());
            info.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy";
            Thread.CurrentThread.CurrentCulture = info;
        }

        #endregion
    }
}
