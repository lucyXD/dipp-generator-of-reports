﻿using GeneratorOfReports.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace GeneratorOfReports.ViewModels
{
    public class LocalitySelectViewModel
    {
        public List<SelectListItem> SelectList { get; set; }
        public List<Locality> SelectedList { get; set; }
        public string Selected { get; set; }
        public int ID { get; set; }
    }
}