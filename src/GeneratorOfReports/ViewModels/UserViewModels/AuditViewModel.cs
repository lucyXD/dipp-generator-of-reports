﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace GeneratorOfReports.ViewModels
{
    public class SearchAuditsViewModel
    {
        public AuditsViewModel AuditsViewModel { get; set; }

        [Display(Name = "Global_DateFrom", ResourceType = typeof(Resources.Translation))]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? ActionDateFrom { get; set; }

        [Display(Name = "Global_DateTo", ResourceType = typeof(Resources.Translation))]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? ActionDateTo { get; set; }

        [Display(Name = "Action_Type", ResourceType = typeof(Resources.Translation))]
        public int? Action { get; set; }

        public IEnumerable<SelectListItem> Actions { get; set; }
    }

    public class AuditsViewModel
    {
        public Dictionary<string, List<string>> HistoryOfActions { get; set; }
    }
}