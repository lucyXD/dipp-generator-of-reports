﻿namespace GeneratorOfReports.ViewModels
{
    public class LocalityWithRolesViewModel
    {
        // Locality details
        public LocalitySelectViewModel LocalitySelectViewModel { get; set; }

        // Roles details
        public RoleSelectViewModel RoleSelectViewModel { get; set; }
    }
}