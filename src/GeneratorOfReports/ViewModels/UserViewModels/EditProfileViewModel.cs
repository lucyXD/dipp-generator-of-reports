﻿namespace GeneratorOfReports.ViewModels
{
    public class EditProfileViewModel
    {
        public UserViewModel User { get; set; }
        public ChangePasswordViewModel ChangePasswordViewModel { get; set; }
    }
}