﻿namespace GeneratorOfReports.ViewModels
{
    public class UserDetailsViewModel
    {
        public UserViewModel User { get; set; }
        public EditProfileViewModel EditUser { get; set; }
        public SearchAuditsViewModel SearchAuditsViewModel { get; set; }
    }
}