﻿using GeneratorOfReports.Attributes;
using GeneratorOfReports.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GeneratorOfReports.ViewModels
{
    public class UserViewModel
    {
        public int ID { get; set; }

        [Display(Name = "User_PersonalNumber", ResourceType = typeof(Resources.Translation))]
        [Required(ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "User_PersonalNumber_ErrorMessage_Required", AllowEmptyStrings = false)]
        [MaxLength(int.MaxValue, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_ExceededMax")]
        public string PersonalNumber { get; set; }

        [Display(Name = "User_Username", ResourceType = typeof(Resources.Translation))]
        [MaxLength(int.MaxValue, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_ExceededMax")]
        [Required(ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "User_Login_ErrorMessage_Required")]
        [MinLength(5, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_Min")]
        public string Login { get; set; }

        [Display(Name = "User_FirstName", ResourceType = typeof(Resources.Translation))]
        [MaxLength(900, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_ExceededMax")]
        [Required(ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "User_FirstName_ErrorMessage_Required")]
        [AlphaCharacters]
        public string FirstName { get; set; }

        [Display(Name = "User_Surname", ResourceType = typeof(Resources.Translation))]
        [MaxLength(900, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_ExceededMax")]
        [Required(ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "User_Surname_ErrorMessage_Required")]
        [AlphaCharacters]
        public string Surname { get; set; }

        [Display(Name = "User_Title", ResourceType = typeof(Resources.Translation))]
        [MaxLength(20, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_ExceededMax")]
        public string Title { get; set; }

        [Display(Name = "User_Email", ResourceType = typeof(Resources.Translation))]
        [Required(ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "User_Email_ErrorMessage_Required", AllowEmptyStrings = false)]
        [MaxLength(254, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_ExceededMax")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "User_Email_ErrorMessage_Correct")]
        public string Email { get; set; }

        [Display(Name = "User_PhoneNumber", ResourceType = typeof(Resources.Translation))]
        [MaxLength(15, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_ExceededMax")]
        [MinLength(4, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_Min")]
        [RegularExpression(@"^\((\+[0-9]{1,3})\)[ 0-9]{1,14}$", ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "User_PhoneNumber_ErrorMessage_Correct")]
        public string PhoneNumber { get; set; }

        [Display(Name = "User_DateOfBirth", ResourceType = typeof(Resources.Translation))]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? DateOfBirth { get; set; }

        [Display(Name = "User_IsActive", ResourceType = typeof(Resources.Translation))]
        public bool IsActive { get; set; }

        public List<UserLocalityRole> LocalityWithRoles { get; set; }
        public Dictionary<Locality, List<Role>> LocalityWithRolesDict { get; set; }
    }
}