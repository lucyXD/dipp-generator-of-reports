﻿namespace GeneratorOfReports.ViewModels
{
    public class UserSearchViewModel
    {
        public UserViewModel UserViewModel { get; set; }
        public RoleSelectViewModel RoleSelectViewModel { get; set; }
        public LocalitySelectViewModel LocalitySelectViewModel { get; set; }
    }
}