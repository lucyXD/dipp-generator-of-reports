﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace GeneratorOfReports.ViewModels
{
    public class RoleSelectViewModel
    {
        public List<SelectListItem> SelectList { get; set; }
        public List<RoleViewModel> SelectedList { get; set; }
        public int ID { get; set; }
    }

    public class RoleViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}