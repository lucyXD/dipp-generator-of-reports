﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace GeneratorOfReports.ViewModels
{
    public class SelectDBViewModel
    {
        public IEnumerable<SelectListItem> Databases { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Query_Database_ErrorMessage_Required", AllowEmptyStrings = false)]
        public string Database { get; set; }
    }
}