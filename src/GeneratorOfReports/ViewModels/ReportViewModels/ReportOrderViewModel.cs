﻿using System.ComponentModel.DataAnnotations;

namespace GeneratorOfReports.ViewModels
{
    public class ReportOrderViewModel
    {
        public int ID { get; set; }

        [Display(Name = "Report_Order_Maximum", ResourceType = typeof(Resources.Translation))]
        public int Maximum { get; set; }

        [Display(Name = "Report_Order", ResourceType = typeof(Resources.Translation))]
        [Range(1, int.MaxValue, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Report_Order_ErrorMessage_Positive")]
        public int Order { get; set; }
    }
}