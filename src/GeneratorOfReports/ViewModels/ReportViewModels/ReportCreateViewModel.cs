﻿using System.Collections.Generic;

namespace GeneratorOfReports.ViewModels
{
    public class ReportCreateViewModel
    {
        public ReportViewModel Report { get; set; }
        public List<AuthorizationViewModel> Authorization { get; set; }
        public List<QueryReportViewModel> Queries { get; set; }
    }
}