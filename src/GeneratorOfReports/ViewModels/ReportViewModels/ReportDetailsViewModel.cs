﻿using System.Collections.Generic;

namespace GeneratorOfReports.ViewModels
{
    public class ReportDetailsViewModel
    {
        public ReportViewModel Report { get; set; }
        public IEnumerable<QueryViewModel> Queries { get; set; }
        public bool CanEdit { get; set; }
        public IEnumerable<AuthorizationViewModel> Authorization { get; set; }
    }
}