﻿using GeneratorOfReports.ReportGenerator;
using System.Collections.Generic;
using System.Web.Mvc;

namespace GeneratorOfReports.ViewModels
{
    public class GenerateReportViewModel
    {
        public string Message { get; set; }

        public Settings Settings { get; set; }
        public IEnumerable<SelectListItem> FormatSelectList { get; set; }

        public ReportViewModel Report { get; set; }
        public IEnumerable<ReportQueryViewModel> Queries { get; set; }
    }
}