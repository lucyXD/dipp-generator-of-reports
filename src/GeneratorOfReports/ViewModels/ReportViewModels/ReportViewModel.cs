﻿using System.ComponentModel.DataAnnotations;

namespace GeneratorOfReports.ViewModels
{
    public class ReportViewModel
    {
        public int? ID { get; set; }

        [Display(Name = "Global_Name", ResourceType = typeof(Resources.Translation))]
        [Required(ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Report_Name_ErrorMessage_Required", AllowEmptyStrings = false)]
        [MaxLength(int.MaxValue, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_ExceededMax")]
        public string Name { get; set; }

        [Display(Name = "Global_Description", ResourceType = typeof(Resources.Translation))]
        [MaxLength(int.MaxValue, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_ExceededMax")]
        public string Description { get; set; }

        [Display(Name = "Global_Valid", ResourceType = typeof(Resources.Translation))]
        public bool Valid { get; set; }
        public string ValidWord { get; set; }

        [Display(Name = "Report_Order", ResourceType = typeof(Resources.Translation))]
        [Range(1, int.MaxValue, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Report_Order_ErrorMessage_Positive")]
        public int Order { get; set; }
        public int Maximum { get; set; }

        [Display(Name = "Global_TimeCreate", ResourceType = typeof(Resources.Translation))]
        public string TimeCreate { get; set; }
    }
}