﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace GeneratorOfReports.ViewModels
{
    public class ReportSearchViewModel
    {
        [Display(Name = "Global_Name", ResourceType = typeof(Resources.Translation))]
        public string Name { get; set; }

        [Display(Name = "Global_Description", ResourceType = typeof(Resources.Translation))]
        public string Description { get; set; }

        [Display(Name = "Query_Valid", ResourceType = typeof(Resources.Translation))]
        public bool? Valid { get; set; }
        public IEnumerable<SelectListItem> ValidSelectList { get; set; }
    }
}