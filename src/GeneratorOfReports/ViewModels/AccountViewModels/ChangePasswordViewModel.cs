﻿using System.ComponentModel.DataAnnotations;

namespace GeneratorOfReports.ViewModels
{
    public class ChangePasswordViewModel
    {
        public bool ChangePassword { get; set; }

        [Display(Name = "User_NewPassword", ResourceType = typeof(Resources.Translation))]
        [Required(ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "User_Password_ErrorMessage_Required", AllowEmptyStrings = false)]
        [MinLength(5, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_Min")]
        [MaxLength(int.MaxValue, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_ExceededMax")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "User_ConfirmPassword", ResourceType = typeof(Resources.Translation))]
        [Required(ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "User_Password_ErrorMessage_Required", AllowEmptyStrings = false)]
        [Compare("Password", ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "User_ConfirmPassword_ErrorMessage")]
        [MaxLength(int.MaxValue, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_ExceededMax")]
        [MinLength(5, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_Min")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}