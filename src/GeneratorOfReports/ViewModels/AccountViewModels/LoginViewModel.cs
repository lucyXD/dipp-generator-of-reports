﻿using System.ComponentModel.DataAnnotations;

namespace GeneratorOfReports.ViewModels
{
    public class LoginViewModel
    {
        [Display(Name = "User_Username", ResourceType = typeof(Resources.Translation))]
        [Required(ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "User_Login_ErrorMessage_Required", AllowEmptyStrings = false)]
        [MaxLength(int.MaxValue, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_ExceededMax")]
        public string Login { get; set; }

        [Display(Name = "User_Password", ResourceType = typeof(Resources.Translation))]
        [Required(ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "User_Password_ErrorMessage_Required", AllowEmptyStrings = false)]
        [MaxLength(int.MaxValue, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_ExceededMax")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}