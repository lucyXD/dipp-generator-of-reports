﻿using System.ComponentModel.DataAnnotations;

namespace GeneratorOfReports.ViewModels
{
    public class ForgottenPasswordViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "User_Email_ErrorMessage_Required", AllowEmptyStrings = false)]
        [Display(Name = "User_Email", ResourceType = typeof(Resources.Translation))]
        [MaxLength(254, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_ExceededMax")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "User_Email_ErrorMessage_Correct")]
        public string Email { get; set; }
    }
}