﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace GeneratorOfReports.ViewModels
{
    public class QuerySearchViewModel
    {
        [Display(Name = "Global_Name", ResourceType = typeof(Resources.Translation))]
        public string Name { get; set; }

        [Display(Name = "Query_Database", ResourceType = typeof(Resources.Translation))]
        public string Database { get; set; }

        [Display(Name = "Query_Valid", ResourceType = typeof(Resources.Translation))]
        public bool? Valid { get; set; }
        public IEnumerable<SelectListItem> ValidSelectList { get; set; }

        public List<int> SelectedQueries { get; set; }
    }
}