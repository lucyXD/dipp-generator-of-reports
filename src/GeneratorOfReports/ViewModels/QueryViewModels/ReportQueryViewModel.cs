﻿using System.Collections.Generic;

namespace GeneratorOfReports.ViewModels
{
    public class ReportQueryViewModel
    {
        public QueryViewModel Query { get; set; }
        public IEnumerable<ParameterViewModel> Parameters { get; set; }
    }
}