﻿namespace GeneratorOfReports.ViewModels
{
    public class QueryReportViewModel
    {
        public int ID { get; set; }
        public int Count { get; set; }
    }
}