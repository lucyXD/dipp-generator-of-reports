﻿using System.Collections.Generic;

namespace GeneratorOfReports.ViewModels
{
    public class QueryDetailsViewModel
    {
        public QueryViewModel Query { get; set; }
        public IEnumerable<ParameterViewModel> Parameters { get; set; }
        public bool CanEdit { get; set; }
        public IList<AuthorizationViewModel> Authorization { get; set; }
    }
}