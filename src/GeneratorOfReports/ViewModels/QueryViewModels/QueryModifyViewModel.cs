﻿using GeneratorOfReports.ContextManagement;
using System.Collections.Generic;

namespace GeneratorOfReports.ViewModels
{
    public class QueryModifyViewModel
    {
        public QueryViewModel Query { get; set; }
        public Database Database { get; set; }
        public IEnumerable<AuthorizationViewModel> Authorization { get; set; }
        public IEnumerable<ParameterViewModel> Parameters { get; set; }
        public bool CreateReport { get; set; }
    }
}