﻿namespace GeneratorOfReports.ViewModels
{
    public class AuthorizationViewModel
    {
        public int ID { get; set; }
        public string Locality { get; set; }
        public int LocalityID { get; set; }
        public bool All { get; set; }
        public bool Edit { get; set; }
        public bool Visibility { get; set; }
        public bool Remove { get; set; }
    }
}