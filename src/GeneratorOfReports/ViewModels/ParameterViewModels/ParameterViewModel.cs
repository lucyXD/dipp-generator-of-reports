﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace GeneratorOfReports.ViewModels
{
    public class ParameterViewModel
    {
        public int ID { get; set; }

        [Display(Name = "Global_Name", ResourceType = typeof(Resources.Translation))]
        [Required(ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Query_Name_ErrorMessage_Required", AllowEmptyStrings = false)]
        [MaxLength(int.MaxValue, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_ExceededMax")]
        public string Name { get; set; }

        [Display(Name = "Global_Description", ResourceType = typeof(Resources.Translation))]
        [MaxLength(int.MaxValue, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_ExceededMax")]
        public string Description { get; set; }

        [Display(Name = "Query_Parameter_DefaultValue", ResourceType = typeof(Resources.Translation))]
        [MaxLength(int.MaxValue, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_ExceededMax")]
        public string DefaultValue { get; set; }
        public string DefaultValueModel { get; set; }
        public bool UseDefaultVaue { get; set; }

        [Display(Name = "Query_Parameter_Required", ResourceType = typeof(Resources.Translation))]
        public bool Required { get; set; }
        public string RequiredWord { get; set; }

        [Display(Name = "Query_Parameter_Type", ResourceType = typeof(Resources.Translation))]
        [Required(ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Query_Parameter_Type_ErrorMessage_Required", AllowEmptyStrings = false)]
        [MaxLength(int.MaxValue, ErrorMessageResourceType = typeof(Resources.Translation), ErrorMessageResourceName = "Global_ErrorMessage_ExceededMax")]
        public string Keyword { get; set; }
        public string Type { get; set; }
        public IEnumerable<SelectListItem> SQLTypeSelectList { get; set; }

        [Display(Name = "Query_Parameter_IsParameter", ResourceType = typeof(Resources.Translation))]
        public bool IsParameter { get; set; }

        public string ModelJson { get; set; }
        public string Hash { get; set; }
    }
}