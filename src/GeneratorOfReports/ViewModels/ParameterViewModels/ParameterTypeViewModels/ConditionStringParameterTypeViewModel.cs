﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace GeneratorOfReports.ViewModels
{
    public class ConditionStringParameterTypeViewModel : ParameterTypeViewModel
    {
        public string Condition { get; set; }
        public IEnumerable<SelectListItem> ComparisonsSelectList { get; set; }
    }
}