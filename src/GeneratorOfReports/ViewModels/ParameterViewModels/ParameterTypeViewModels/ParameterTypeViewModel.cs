﻿namespace GeneratorOfReports.ViewModels
{
    public class ParameterTypeViewModel
    {
        public int ID { get; set; }
        public string Value { get; set; }
        public string Hash { get; set; }
    }
}