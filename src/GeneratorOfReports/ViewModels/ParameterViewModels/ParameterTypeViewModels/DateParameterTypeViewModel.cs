﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GeneratorOfReports.ViewModels
{
    public class DateParameterTypeViewModel : ParameterTypeViewModel
    {
        [Display(Name = "Global_DateFrom", ResourceType = typeof(Resources.Translation))]
        public DateTime? ActionDateFrom { get; set; }

        [Display(Name = "Global_DateTo", ResourceType = typeof(Resources.Translation))]
        public DateTime? ActionDateTo { get; set; }
    }
}