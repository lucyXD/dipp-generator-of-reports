﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace GeneratorOfReports.ViewModels
{
    public class SelectParameterTypeViewModel : ParameterTypeViewModel
    {
        public IEnumerable<SelectListItem> Options { get; set; }
    }
}