﻿namespace GeneratorOfReports.ViewModels
{
    public class PositiveIntParameterTypeViewModel : ParameterTypeViewModel
    {
        public new int Value { get; set; }
    }
}