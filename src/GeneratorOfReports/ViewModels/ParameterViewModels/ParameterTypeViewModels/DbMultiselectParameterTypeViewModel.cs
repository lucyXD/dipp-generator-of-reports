﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace GeneratorOfReports.ViewModels
{
    public class DbMultiselectParameterTypeViewModel : ParameterTypeViewModel
    {
        public MultiSelectList Options { get; set; }
        public List<string> Selected { get; set; }
        public string Database { get; set; }
        public string ValueIdentifier { get; set; }
        public string TextIdentifier { get; set; }
        public string TableIdentifier { get; set; }
    }
}