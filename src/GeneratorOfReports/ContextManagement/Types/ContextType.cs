﻿namespace GeneratorOfReports.ContextManagement
{
    /// <summary>
    /// Contains contexts the user can work with when creating queries.
    /// </summary>
    public class ContextType : ContextWrapper
    {
        #region Contexes

        public static readonly ContextType RestaurantModel = new ContextType(
            "Restaurant",
            "data source=LUCIAHARCEK430C;initial catalog=Restaurant;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework");

        public static readonly ContextType TrainTimetableModel = new ContextType(
            "TrainTimetable",
            "data source=LUCIAHARCEK430C;initial catalog=TrainTimetable;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework");

        public static readonly ContextType LWPKartotekaVyvojModel = new ContextType(
            "LWP_Kartoteka_vyvoj",
            "data source=LUCIAHARCEK430C;initial catalog=LWP_Kartoteka_vyvoj;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework");

        #endregion

        #region Constructors

        public ContextType(string name, string connectionString) : base(name, connectionString) { }

        #endregion
    }
}