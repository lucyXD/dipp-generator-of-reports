﻿using GeneratorOfReports.Resources;
using GeneratorOfReports.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace GeneratorOfReports.ContextManagement
{
    /// <summary>
    /// The wrapper and security layer determines functions over contexes.
    /// </summary>
    public abstract class ContextWrapper : TypeUtility
    {
        #region Properties

        // Specific contex
        public string _connectionString { get; private set; }

        #region Banned 

        // Banned expresions in query
        private readonly Regex _regexBanned = new Regex("\\b(DROP DATABASE|CREATE DATABASE|CREATE TABLE|DELETE TABLE|ALTER TABLE|TRUNCATE TABLE|DROP TABLE)\\b", RegexOptions.IgnoreCase);

        #endregion

        #endregion

        #region Constructor

        protected ContextWrapper(string name, string connectionString) : base(name)
        {
            _connectionString = connectionString;
        }

        #endregion

        #region Getters

        public Database GetDB()
            => new Database
            {
                Name = Name,
                Tables = GetTables()
            };

        private List<string> GetTables()
        {
            List<string> tables = new List<string>();

            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();

                DataTable schema = connection.GetSchema("Tables");
                foreach (DataRow row in schema.Rows)
                {
                    tables.Add(row["TABLE_SCHEMA"] + "." + row["TABLE_NAME"].ToString());
                }
            }

            return tables;
        }

        public List<object> GetTableData(string tableName)
        {
            var columns = new List<object>();
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();
                tableName = tableName.Substring(tableName.IndexOf(".") + 1);
                DataTable dt = connection.GetSchema("Columns", new[] { connection.Database, null, tableName });
                foreach (DataRow row in dt.Rows)
                {
                    columns.Add(new
                    {
                        Name = row["COLUMN_NAME"].ToString(),
                        Type = row["DATA_TYPE"].ToString()
                    });
                }

            }
            return columns;
        }

        internal MultiSelectList CreateMultiselect(string table, string columnValue, string columnText)
        {
            string form = "SELECT DISTINCT TOP 999 [{0}].{2}, [{0}].{1} FROM [{0}] ORDER BY [{0}].{1} ASC";
            string query = string.Format(form, table, columnValue, columnText);
            DataTable dt = GetDataTable(query);
            var multiselect = new MultiSelectList(dt.AsDataView(), columnValue, columnText);
            return multiselect;
        }

        #endregion

        #region Execute Query

        internal DataTable GetDataTable(string query)
        {
            DataTable table = new DataTable();
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = _connectionString;
                try
                {
                    con.Open();
                    using (var command = con.CreateCommand())
                    {
                        command.CommandText = query;
                        command.Connection = con;
                        command.CommandType = CommandType.Text;
                        using (var reader = command.ExecuteReader())
                        {
                            table.Load(reader);
                        }
                    }
                }
                catch (Exception ex)
                {
                    return table;
                }
            }
            return table;
        }

        #endregion

        #region Validation

        internal (bool, Error) IsCorrect(string query)
        {
            if (query == null)
            {
                return (false, new Error(Translation.Query_Error_Empty_Type, Translation.Query_Error_Empty_Description, Translation.Report_Query));
            }

            // remove redundant parts from query
            query = RemoveComments(query);

            // check if query can affect context
            if (TryAffect(query))
            {
                return (false, new Error(Translation.Query_Error_AffectDB_Type, Translation.Query_Error_AffectDB_Description, Translation.Report_Query));
            }

            // corect from of the query
            (bool correctForm, Error error) = CanEvaluate(query);
            if (!correctForm)
            {
                return (false, error);
            }

            return (true, null);
        }

        internal (bool, Error) CanEvaluate(string query)
        {
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = _connectionString;
                try
                {
                    con.Open();
                    using (var transaction = con.BeginTransaction(IsolationLevel.RepeatableRead))
                    using (var command = con.CreateCommand())
                    {
                        command.CommandType = CommandType.Text;
                        command.Connection = con;
                        command.CommandText = query;
                        command.Transaction = transaction;

                        command.CommandTimeout = 300;  // seconds

                        if (command.ExecuteNonQuery() > 0) // query would change db
                        {
                            return (false, new Error(Translation.Query_Error_Empty_Type, Translation.Query_Error_Empty_Description, Translation.Report_Query));
                        }
                        transaction.Rollback();
                    }
                }
                catch (Exception e) // query isn't in correct shape
                {
                    return (false, new Error(Translation.Query_Error_InvalidSyntax_Type, e.Message, Translation.Report_Query));
                }
            }

            return (true, null);
        }

        private bool TryAffect(string query)
        {
            var matches = _regexBanned.Matches(query).Cast<Match>();
            return matches.Any(x => !IsInName(query, x.Index, x.Length));
        }

        #region Utilities

        /// <summary>
        /// Source: https://drizin.io/Removing-comments-from-SQL-scripts/
        /// </summary>
        public string RemoveComments(string input)
        {
            Regex everythingExceptNewLines = new Regex("[^\r\n]");
            //based on http://stackoverflow.com/questions/3524317/regex-to-strip-line-comments-from-c-sharp/3524689#3524689
            var lineComments = @"--(.*?)\r?\n";
            var lineCommentsOnLastLine = @"--(.*?)$";
            var literals = @"('(('')|[^'])*')";
            var bracketedIdentifiers = @"\[((\]\])|[^\]])* \]";
            var quotedIdentifiers = @"(\""((\""\"")|[^""])*\"")";
            //so we should use balancing groups http://weblogs.asp.net/whaggard/377025
            var nestedBlockComments = @"/\*
                                 (?>
                                 /\*  (?<LEVEL>)      # On opening push level
                                 | 
                                 \*/ (?<-LEVEL>)     # On closing pop level
                                 |
                                 (?! /\* | \*/ ) . # Match any char unless the opening and closing strings   
                                 )+                         # /* or */ in the lookahead string
                                 (?(LEVEL)(?!))             # If level exists then fail
                                 \*/";

            string noComments = Regex.Replace(input,
                nestedBlockComments + "|" + lineComments + "|" + lineCommentsOnLastLine + "|" + literals + "|" + bracketedIdentifiers + "|" + quotedIdentifiers,
                me =>
                {
                    if (me.Value.StartsWith("/*"))
                        return everythingExceptNewLines.Replace(me.Value, " ");
                    else if (me.Value.StartsWith("--"))
                        return everythingExceptNewLines.Replace(me.Value, " ");
                    else
                        return me.Value;
                },
                RegexOptions.Singleline | RegexOptions.IgnorePatternWhitespace);
            return noComments;
        }

        private bool IsInName(string query, int pos, int length)
        {
            var left = query.Substring(0, pos);
            var right = query.Substring(pos + length);

            int leftCount_en = left.Split('\'').Length - 1;
            int leftCount = left.Split('"').Length - 1;

            int rightCount_en = right.Split('\'').Length - 1;
            int rightCount = right.Split('"').Length - 1;

            if ((IsOdd(leftCount) && IsOdd(rightCount)) ||
                (IsOdd(leftCount_en) && IsOdd(rightCount_en)))
                return true;

            return false;
        }

        private bool IsOdd(int n) => n % 2 != 0;

        #endregion

        #endregion
    }
}