﻿using System.Collections.Generic;

namespace GeneratorOfReports.ContextManagement
{
    public class Database
    {
        public string Name { get; set; }
        public List<string> Tables { get; set; }
    }
}