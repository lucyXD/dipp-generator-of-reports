﻿namespace GeneratorOfReports.ContextManagement
{
    public class Error
    {
        public string Type { get; }
        public string Description { get; }
        public string Location { get; }

        public Error(string Type, string Description, string Location)
        {
            this.Type = Type;
            this.Description = Description;
            this.Location = Location;
        }
    }
}