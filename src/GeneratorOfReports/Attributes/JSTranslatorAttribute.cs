﻿using System.Text.RegularExpressions;
using System.Web.Optimization;

namespace GeneratorOfReports.Attributes
{
    /// <summary>
    /// Source: 
    /// https://www.c-sharpcorner.com/UploadFile/2ed7ae/multilingual-mvc-application-with-customized-js-bundles-base/
    /// </summary>
    public class JSTranslatorAttribute : IBundleTransform
    {
        private static readonly Regex Regex = new Regex(@"GetResourceValue\(([^\))]*)\)", RegexOptions.Singleline | RegexOptions.Compiled);

        public void Process(BundleContext context, BundleResponse response)
        {
            response.Content = Translate(response.Content);
        }

        private string Translate(string text)
        {
            MatchCollection matches = Regex.Matches(text);
            foreach (Match match in matches)
            {
                object obj = Resources.Translation.ResourceManager.GetString(match.Groups[1].Value);
                if (obj != null)
                {
                    string tranlatedText = obj.ToString().Replace("'", "");
                    text = text.Replace(match.Value, tranlatedText);
                }
            }
            return text;
        }
    }
}