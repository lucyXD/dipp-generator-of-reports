﻿using System.ComponentModel.DataAnnotations;

namespace GeneratorOfReports.Attributes
{
    public class AlphaCharactersAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            bool isValid = value == null || System.Text.RegularExpressions.Regex.IsMatch(value.ToString(), @"^(?=\S)[\p{L} \p{N}]+(?<=[^.\s])$"); 
            if (isValid)
            {
                return null; 
            }
            return new ValidationResult(Resources.Translation.Global__AlphaCharacters_ErrorMessage);
        }
    }
}