﻿using GeneratorOfReports.Authentication;
using GeneratorOfReports.Resources;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GeneratorOfReports.Utilities
{
    public static class FormattingUtility
    {
        #region Word formatting

        // Universal formating of Date for whole application
        public static string GetDay(DateTime? date)
            => date == null ? "-" : date.Value.ToString("MM/dd/yyyy");

        public static string GetDayTime(DateTime? date)
            => date == null ? "-" : date.Value.ToString("MM/dd/yyyy HH:mm");

        internal static string GetBooleanWord(bool valid)
            => valid ? Translation.Global_Yes : Translation.Global_No;

        public static string GetRole(string role)
        {
            switch (role)
            {
                case RolesConstants.REPORT_ADMIN: return Translation.Role_ReportAdministrator;
                case RolesConstants.QUERY_ADMIN: return Translation.Role_QueryAdministrator;
                case RolesConstants.USER_ADMIN: return Translation.Role_UserAdministrator;
                default: return Translation.Global_Error;
            }
        }

        #endregion

        #region Select list formatting

        internal static IEnumerable<SelectListItem> GetEnumSelectList<T>()
            => Enum.GetValues(typeof(T)).Cast<int>().Select(e => new SelectListItem()
            {
                Text = Enum.GetName(typeof(T), e),
                Value = e.ToString()
            }).ToList();

        internal static IEnumerable<SelectListItem> CreateSelectList(List<string> items)
            => items.Select(x => new SelectListItem { Text = x, Value = x }).ToList();

        internal static List<SelectListItem> GetBooleanSelectList()
            => new List<SelectListItem> {
                new SelectListItem { Text = Translation.Global_Yes, Value = "True" },
                new SelectListItem { Text = Translation.Global_No, Value = "False" } };

        #endregion

        #region JSON formatting

        internal static T FormatJson<T>(string modelJson)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(modelJson);
            }
            catch (Exception)
            {
                return default;
            }
        }

        #endregion
    }
}