﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace GeneratorOfReports.Utilities
{
    /// <summary>
    /// Enumerated class used when working with types instead of enumerated types.
    /// This improves code redundancy and replaces flow control commands that control enum values.
    /// </summary>
    public abstract class TypeUtility : IComparable
    {
        #region Properties

        public string Name { get; private set; }

        #endregion

        #region Constructor

        protected TypeUtility(string name)
        {
            Name = name;
        }

        #endregion

        #region Getters

        public static List<T> GetAllList<T>() where T : TypeUtility
            => GetAll<T>().ToList();

        public static IEnumerable<T> GetAll<T>() where T : TypeUtility
            => typeof(T)
                .GetFields(BindingFlags.Static | BindingFlags.Public)
                .Select(x => (T)x.GetValue(null));

        public static IEnumerable<SelectListItem> GetSelectList<T>() where T : TypeUtility
            => new SelectList(GetAllList<T>()
                        .OrderBy(x => x.Name)
                        .Select(x => new SelectListItem { Value = x.Name, Text = x.Name })
                        .ToList(), "Value", "Text");

        public static T GetValue<T>(string name) where T : TypeUtility
            => GetAll<T>().FirstOrDefault(x => x.Name.Equals(name));

        #endregion

        #region Utility

        internal static bool Know<T>(string name) where T : TypeUtility
            => GetAll<T>().Any(x => x.Name.Equals(name));

        public override bool Equals(object obj)
            => obj != null && GetType().Equals(obj.GetType()) && Name.Equals(((TypeUtility)obj).Name);

        public int CompareTo(object obj)
            => Name.CompareTo(((TypeUtility)obj).Name);

        public override int GetHashCode()
            => 539060726 + EqualityComparer<string>.Default.GetHashCode(Name);

        #endregion
    }
}