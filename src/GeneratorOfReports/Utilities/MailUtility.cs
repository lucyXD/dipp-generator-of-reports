﻿using System;
using System.Net;
using System.Net.Mail;

namespace GeneratorOfReports.Utilities
{
    public class MailUtility
    {
        #region Credentials

        public static readonly string EMAIL = "OG.GeneratorOfReports@gmail.com";
        private static readonly string PASSWORD = "GeneratorOfReports";

        #endregion

        #region Sending email

        public static bool SendEmail(string toEmail, string subject, string body)
        {
            try
            {
                SmtpClient smtpClient = CreateGmailClient();
                MailMessage message = new MailMessage { From = new MailAddress(EMAIL), IsBodyHtml = true, Subject = subject, Body = body };
                message.To.Add(new MailAddress(toEmail));
                smtpClient.Send(message);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private static SmtpClient CreateGmailClient()
            => new SmtpClient
            {
                Port = 587,
                Host = "smtp.gmail.com",
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(EMAIL, PASSWORD),
                DeliveryMethod = SmtpDeliveryMethod.Network
            };

        #endregion
    }
}