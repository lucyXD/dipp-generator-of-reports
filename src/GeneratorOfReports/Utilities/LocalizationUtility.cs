﻿using System.Web;

namespace GeneratorOfReports.Utilities
{
    /// <summary>
    /// Source: 
    /// https://www.c-sharpcorner.com/UploadFile/2ed7ae/multilingual-mvc-application-with-customized-js-bundles-base/
    /// </summary>
    public static class LocalizationUtility
    {
        public static HtmlString LocalizeBundle(string fileName)
        {
            var cookie = HttpContext.Current.Request.Cookies["OG_GOR_Language"];
            string culture = cookie != null && cookie.Value != null && cookie.Value.Trim() != string.Empty
                                ? cookie.Value
                                : "en";
            fileName = string.Concat(fileName, "-", culture);
            var output = (HtmlString)System.Web.Optimization.Scripts.Render(fileName);
            return output;
        }
    }
}