﻿using System.Collections.Generic;

namespace GeneratorOfReports.Utilities
{
    #region Notification Utility

    public static class NotificationUtility
    {
        private static List<Notification> Notifications = new List<Notification>();

        public static List<Notification> GetAll() 
            => Notifications;

        public static void Add(Notification notification) 
            => Notifications.Add(notification);

        public static void Remove(Notification notification) 
            => Notifications.Remove(notification);

        public static void Clear() 
            => Notifications = new List<Notification>();
    }

    #endregion

    #region Notification POCO

    public class Notification
    {
        public string Type { get; set; }
        public string Message { get; set; }
        public string Title { get; set; }

        public Notification(string type, string title, string message)
        {
            Type = type;
            Message = message;
            Title = title;
        }
    }

    #endregion
}