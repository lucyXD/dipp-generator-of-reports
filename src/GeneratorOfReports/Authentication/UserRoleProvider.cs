﻿using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using GeneratorOfReports.Models;

namespace GeneratorOfReports.Authentication
{
    public class UserRoleProvider : RoleProvider
    {

        public override string ApplicationName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            var userRoles = new string[] { };

            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                return userRoles;

            using (GORContext _dbContext = new GORContext())
            {
                var dbUser = _dbContext.Users.FirstOrDefault(x => x.Login.Equals(username));
                if (dbUser != null) userRoles = dbUser.UserLocalityRoles.Select(x => x.Role.Name).ToArray();
                return userRoles;
            }
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            var userRoles = GetRolesForUser(username);
            return userRoles.Contains(roleName);
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}