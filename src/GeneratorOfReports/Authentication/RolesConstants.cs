﻿namespace GeneratorOfReports.Authentication
{
    public static class RolesConstants
    {
        /// <summary>
        ///  The query administrator has access to editing and creating queries.
        /// </summary>
        public const string QUERY_ADMIN = "QueryAdministrator";

        /// <summary>
        /// He can  check all users in the system, see and edit information about
        /// them and list the history of their actions in the user management section.
        /// </summary>
        public const string USER_ADMIN = "UserAdministrator";

        /// <summary>
        /// He uses queries created by the query administrator for generating reports.
        /// </summary>
        public const string REPORT_ADMIN = "ReportAdministrator";
    }
}
