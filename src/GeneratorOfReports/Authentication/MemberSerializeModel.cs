﻿namespace GeneratorOfReports.Authentication
{
    public class MemberSerializeModel
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CurrentRole { get; set; }
        public string CurrentLocality { get; set; }
        public int CurrentLocalityID { get; set; }
        public string[] Roles { get; set; }
        public string[] Localities { get; set; }

        public MemberSerializeModel()  { }

        public MemberSerializeModel(Principal user)
        {
            ID = user.ID;
            FirstName = user.FirstName;
            LastName = user.LastName;
            CurrentRole = user.CurrentRole;
            CurrentLocality = user.CurrentLocality;
            CurrentLocalityID = user.CurrentLocalityID;
            Roles = user.Roles;
            Localities = user.Localities;
        }
    }
}