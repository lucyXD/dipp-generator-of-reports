﻿using GeneratorOfReports.Models;
using System.Collections.Generic;
using System.Web.Security;
using System.Linq;

namespace GeneratorOfReports.Authentication
{
    public class Member : MembershipUser
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string CurrentRole { get; set; }
        public string CurrentLocality { get; set; }
        public int CurrentLocalityID { get; set; }

        public string[] Roles { get; set; }
        public string[] Localities { get; set; }

        public Member(User user)
        {
            ID = user.ID;
            FirstName = user.FirstName;
            LastName = user.Surname;

            IEnumerable<UserLocalityRole> RoleUserCompanies = user.UserLocalityRoles.AsEnumerable();
            if (RoleUserCompanies != null && RoleUserCompanies.Any())
            {
                CurrentRole = RoleUserCompanies.FirstOrDefault().Role.Name;
                CurrentLocality = RoleUserCompanies.FirstOrDefault().Locality.Name;
                CurrentLocalityID = RoleUserCompanies.FirstOrDefault().Locality.ID;
                Roles = RoleUserCompanies.Where(x => x.Locality.Name.Equals(CurrentLocality)).Select(x => x.Role.Name).ToArray();
                Localities = RoleUserCompanies.Select(x => x.Locality.Name).Distinct().ToArray();
            }
        }
    }
}