﻿using System.Security.Principal;

namespace GeneratorOfReports.Authentication
{
    public class Principal : IPrincipal
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string CurrentRole { get; set; }
        public string CurrentLocality { get; set; }
        public int CurrentLocalityID { get; set; }

        public string[] Roles { get; set; }
        public string[] Localities { get; set; }

        public IIdentity Identity { get; private set; }
        public bool IsInRole(string role) { return (CurrentRole != null) && CurrentRole.Equals(role); }
        public Principal(string username) { Identity = new GenericIdentity(username); }
    }
}