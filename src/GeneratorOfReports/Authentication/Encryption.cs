﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace GeneratorOfReports.Authentication
{
    public static class Encryption
    {
        public static string GeneratetPasswordToken => Guid.NewGuid().ToString();

        public static string GetSHA256Hash(string password, string salt)
        {
            using (var sha256 = SHA256.Create())
            {
                byte[] combined = AddSalt(Encoding.UTF8.GetBytes(password),
                                          Encoding.UTF8.GetBytes(salt));
                return Convert.ToBase64String(sha256.ComputeHash(combined));
            }
        }

        private static byte[] AddSalt(byte[] hash, byte[] salt)
        {
            byte[] combined = new byte[hash.Length + salt.Length];
            Buffer.BlockCopy(hash, 0, combined, 0, hash.Length);
            Buffer.BlockCopy(salt, 0, combined, hash.Length, salt.Length);
            return combined;
        }
    }
}