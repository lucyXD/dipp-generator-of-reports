﻿(function ($) {

    /* ---------------------------------
     * DATATABLE 
     * --------------------------------- */

    $.extend(true, $.fn.dataTable.defaults, {
        processing: true,
        serverSide: true,
        autoWidth: true,
        language: {
            "lengthMenu": 'GetResourceValue(DataTable_LengthMenu)',
            "processing": "<i class='fa fa-spinner fa-spin'></i> GetResourceValue(Global_Loading)",
            "search": "",
            "zeroRecords": 'GetResourceValue(DataTable_ZeroRecords)',
            "info": 'GetResourceValue(DataTable_Info)',
            "infoEmpty": 'GetResourceValue(DataTable_InfoEmpty)',
            "infoFiltered": "",
            "paginate": {
                "first": 'GetResourceValue(DataTable_First)',
                "last": 'GetResourceValue(DataTable_Last)',
                "next": 'GetResourceValue(DataTable_Next)',
                "previous": 'GetResourceValue(DataTable_Previous)'
            }
        }
    });

})(jQuery);