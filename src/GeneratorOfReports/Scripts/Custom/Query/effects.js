﻿(function ($) {

    /* -----------------------------------------------
     * PARAMETERS LOADING
     * ----------------------------------------------- */

    const parameterRegex = /(@\w+)/g;

    /* "PARAMETERS" INSIDE A QUERY TEXTAREA */

    var queryMain = $('.query-main');
    var queryBack = $('.query-back');

    /* ADD/EDIT PARAMETER */

    // when create query: empty, edit query: orignal values
    var oldParameters = queryMain.val().match(parameterRegex);

    function checkEditing() {

        var text = queryMain.val();
        var parametersTab = $("#parametersSettingsTab");
        var newParameters = text.match(parameterRegex);

        oldParameters = laodActual();

        if (newParameters != undefined) {
            newParameters = newParameters.filter((v, i, a) => a.indexOf(v) === i);
        }

        if (newParameters == null) {
            newParameters = [];
        }

        if (!areEqual(oldParameters, newParameters)) {

            var newLength = newParameters == null ? 0 : newParameters.length;
            var oldLength = oldParameters == null ? 0 : oldParameters.length;

            if (newLength == oldLength) { // changed value
                var index = findIndexDifference(oldParameters, newParameters);
                let newValue = newParameters[index];
                var parameters = parametersTab.find("form");
                var p = parameters[index];
                if (p != undefined) { $(p.getElementsByClassName("parameterName")[0]).val(newValue); checkEditing(); }
            } else {
                oldParameters = laodActual();
                if (newLength < oldLength) { // remove parameter
                    var index = findIndexDifference(oldParameters, newParameters);
                    var parameters = parametersTab.find("form");
                    $(parameters[index]).remove();
                    checkEditing();
                } else { //add parameter
                    var index = findIndexDifference(newParameters, oldParameters);
                    var addedValue = newParameters[index];
                    $.ajax({
                        url: 'AddParameter',
                        type: 'POST',
                        data: JSON.stringify({ "name": addedValue }),
                        contentType: 'application/json; charset=utf-8',
                        success: function (response) {
                            var parameters = parametersTab.find("form");
                            if (parameters[index] == undefined) {
                                parametersTab.append(response);
                            } else {
                                $(parameters[index]).before(response);
                            }
                            CheckInputFields();
                            checkEditing();
                        }
                    });
                }
            }
        }
    }

    function areEqual(a1, a2) {
        var t1 = JSON.stringify(a1);
        var t2 = JSON.stringify(a2);
        return t1 == t2;
    }

    function findIndexDifference(a1, a2) {
        var index = 0;

        if (a1 == null || a2 == null) return index;

        while (a1.length >= index) {
            if (!a2.includes(a1[index])) break;
            index++;
        }

        return index;
    }

    function laodActual() {
        var p = Array();
        $("#parametersSettingsTab").find("form").each(function () {
            p.push($(this.getElementsByClassName("parameterName")[0]).val());
        });
        return p;
    }

    /* ---------------------------------
     * PARAMETERS ISPARAMETER
     * --------------------------------- */

    // hide all except name and isParameter option
    $('#parametersSettingsTab').on('change', '.isParameterCheckbox', function (e) {
        e.preventDefault();
        var form = this.closest("form");
        $(form).find(".row:not(.isParameter)")
            .each(function () { $(this).toggleClass("hide") });
    });

    /* ---------------------------------
     * DEFAULT PARAMETER VALUE  
     * --------------------------------- */

    $('#parametersSettingsTab').on('change', '.parameterType', function (e) {
        e.preventDefault();
        var form = this.closest("form");
        var elem = form.getElementsByClassName("defaultValue");
        var type = this.value;
        LoadParameterDefaultForm(form, elem, type)
    });

    function LoadParameterDefaultForm(parent, elem, type) {
        $.ajax({
            url: 'CreateParameterDefaultForm',
            type: 'POST',
            data: JSON.stringify({ type: type }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                $(elem).html(response);
                CheckInputFields();
                $(parent).find(".validation-summary-errors ul li").html("");
            }
        });
    }

    /* -------------------------------------------------------------------------------------------------
     * QUERY INPUT HIGHLIGTING
     * (HIGHLIGHT "PARAMETERS)
     * 
     * INPSO : 
     * https://stackoverflow.com/questions/1619167/textarea-that-can-do-syntax-highlighting-on-the-fly
     * ------------------------------------------------------------------------------------------------- */

    var queryHighlights = $('.query-highlights');

    var keywords = [" DISTINCT", "SELECT", "FROM", "WHERE ", " LIKE", "SET ",
        "JOIN", "LEFT JOIN", "RIGHT JOIN", "CROSS JOIN", "FULL JOIN", "SELF JOIN", "INNER JOIN", "DELETE JOIN",
        "LEFT OUTER", "LEFT INNER", " ON ",
        " CASE", " WHILE", " AS ", "ORDER BY", " ASC", "DESC", "GROUP BY ",
        " HAVING", "SUM", " AVG", " MAX", " MIN", " COUNT",
        " AND ", " OR ", " NOT", " IS",
        " BETWEEN", "FALSE", "NULL", "TRUE", " IN ", " TOP", " LIMIT ", "DECLARE "];
    const keywordsRegex = RegExp(`\\b(?:\\w+-)*${keywords.join('|')}(?:-\\w+)*\\b`, 'gi');

    queryMain.on({
        'input': handleInput,
        'scroll': handleScroll
    });

    function handleInput() {
        actualizeHighlights();
        actualizeHistory();
        actualizeLineNo();
        checkEditing();
    }

    function handleScroll() {
        actualizeLineNo();
        queryBack.scrollTop(queryMain.scrollTop());
        queryBack.scrollLeft(queryMain.scrollLeft());
    }

    function actualizeHighlights() {
        var text = queryMain.val();

        text = text
            .replace(/\n$/g, '\n\n')
            .replace(/>/g, "&gt;")
            .replace(/</g, "&lt;")
            .replace(parameterRegex, '<mark>$&</mark>')     // PARAMETER HIGHLIGHT
            .replace(keywordsRegex, '<span class="text-important">$&</span>');        //SQL KEYWORDS HIGHLIGHT

        queryHighlights.html(text);
    }

    handleInput();

    /* ---------------------------------
     * UNDO & REDO BUTTONS
     * --------------------------------- */

    var undoButton = $('#undoQueryButton');
    var redoButton = $('#redoQueryButton');
    var history;
    var historyIndex;

    function actualizeHistory() {

        if (history == undefined) {
            history = [];
            historyIndex = -1;
        }

        if (historyIndex != -1 || queryMain.val() != "") {

            if ($(undoButton).hasClass("disabled")) {
                $(undoButton).removeClass('disabled');
            }

            if (!$(redoButton).hasClass("disabled")) {
                $(redoButton).addClass('disabled');
            }

            if (history.length - 1 != historyIndex && historyIndex >= 0) {
                history.length = historyIndex;
            }

            historyIndex++;
            history.push(queryMain.val());
        }
    }

    $(undoButton).click(function (e) {
        if (historyIndex >= 0) {
            historyIndex--;
            queryMain.val(history[historyIndex]);
            if (historyIndex == -1 && !$(undoButton).hasClass("disabled")) {
                $(undoButton).addClass('disabled');
            }
            $(redoButton).removeClass('disabled');
            actualizeHighlights();
            actualizeLineNo();
        }
    });

    $(redoButton).click(function (e) {
        if (historyIndex < history.length) {
            historyIndex++;
            queryMain.val(history[historyIndex]);
            if (historyIndex == history.length - 1 && !$(redoButton).hasClass("disabled")) {
                $(redoButton).addClass('disabled');
            }
            $(undoButton).removeClass('disabled');
            actualizeHighlights();
            actualizeLineNo();
        }
    });

    /* ---------------------------------
     * RESIZING DUO
     * --------------------------------- */

    var handler = document.querySelector('.handler');
    var wrapper = handler.closest('.resize-duo');
    var member1 = wrapper.querySelector('.member-1');
    var member2 = wrapper.querySelector('.member-2');
    var resizecontent = wrapper.querySelector('.resize-content');
    var isHandlerDragging = false;

    document.addEventListener('mouseup', function (e) {
        isHandlerDragging = false;
    });

    document.addEventListener('mousedown', function (e) {
        if (e.target === handler) {
            isHandlerDragging = true;
        }
    });

    document.addEventListener('mousemove', function (e) {

        if (!isHandlerDragging) {
            return false;
        }

        var offsetBottom = 40;  // (bottom of tabs)
        var minWidth = 30;
        var containerOffsetTop = wrapper.offsetTop;
        var pointerRelativeXpos = e.clientY - containerOffsetTop - 30 - minWidth - containerOffsetTop - 4;

        // also in css min-height
        member1.style.height = (Math.max(minWidth, pointerRelativeXpos)) + 'px';

        // -- value is bcs of handler
        member2.style.height = (wrapper.offsetHeight - member1.offsetHeight - 7) + 'px';
        resizecontent.style.height = (member2.offsetHeight - offsetBottom - 2) + 'px';
    });

    /* ---------------------------------
     * LINES  NUMBER
     * --------------------------------- */

    function actualizeLineNo() {

        var lines = document.getElementById('queryLines');
        var linesWrapper = document.getElementsByClassName('lines-wrapper')[0];
        var l = Array.from($(lines).find('.line'));
        var n = queryMain.val().split("\n").length;

        if (l.length < n) {
            for (let step = l.length; step < n; step++) {
                addLineNumber(step + 1);
            }
        } else {
            for (let step = n; step < l.length; step++) {
                removeLineNumber();
            }
        }

        // Actualize showed lines No.
        lines.style.marginTop = (-1 * queryMain.scrollTop() + 3) + "px";
        linesWrapper.style.marginLeft = (-1 * queryMain.scrollLeft()) + "px";
    }

    function addLineNumber(lineNo) {
        $('#queryLines').append("<div class='line'>" + lineNo + "</div>");
    }

    function removeLineNumber() {
        var last = $('#queryLines').find('.line');
        last[last.length - 1].remove()
    }

})(jQuery);