﻿/* ---------------------------------
 * QUERIES TABLE
 * --------------------------------- */

var queriesTable = $("#queriesTable").DataTable({
    pageLength: 12,
    lengthMenu: [12, 20, 30],
    columns: [
        {
            data: "ID",
            className: "ID",
            render: $.fn.dataTable.render.text()
        },
        {
            name: "Name",
            data: "Name",
            render: $.fn.dataTable.render.text()
        },
        {
            name: "Database",
            data: "Database",
            render: $.fn.dataTable.render.text()
        },
        {
            data: "CanDelete",
            orderable: false,
            render: function (data) {
                if (data) {
                    return "<a href='#' class='removeQuery' title='Remove query'><span class='fa fa-trash'></span></a>"
                }
            }
        },
        {
            data: 'CanEdit',
            orderable: false,
            render: function (data, type, row, meta) {
                if (data) {
                    return "<a href='/Query/Edit?ID=" + row.ID + "' title='Edit query'><span class='fa fa-edit'></span></a>"
                }
            }
        },
        {
            data: 'ID',
            orderable: false,
            render: function (data) {
                return "<a href='/Query/Copy?ID=" + data + "' title='Copy query'><span class='fa fa-copy'></span></a>"
            }
        }
    ],
    columnDefs: [{ "defaultContent": "-", "targets": "_all", }],
    ajax: { url: "LoadQueriesTableData", type: 'POST', datatype: "json" }
});

/* ---------------------------------
 * SEARCHING
 * --------------------------------- */

var querySearch = $("#queriesTable thead");

function searchQueries() {
    var data = JSON.stringify({
        Database: $('#Database').val(),
        Name: $('#Name').val()
    });
    queriesTable.search(data).draw();
}

$(querySearch).find('select').on('change', function () {
    searchQueries();
});

$(querySearch).find('input, textarea').on('change, keyup', function () {
    searchQueries();
});

/* ---------------------------------
 * DETAILS
 * --------------------------------- */

queriesTable.on('click', 'tbody tr', function () {

    var tr = $(this);
    var ID = tr.find(".ID").text();

    if (ID != undefined && ID != "") {
        HighlightRow(tr);
        $.ajax({
            url: 'LoadDetails',
            type: 'POST',
            data: JSON.stringify({ ID: ID }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                $("#queryDetails").html(response);
                loadDetailsTable(ID);
            }
        });
    }
});

function HighlightRow(tr) {
    $("#queriesTable tbody tr").removeClass('selected-row');
    $(tr).addClass('selected-row');
}

/* ---------------------------------
 * REMOVE QUERY
 * --------------------------------- */

queriesTable.on('click', 'a.removeQuery', function (e) {

    e.stopPropagation();

    var ID = $(this).parents("tr").find(".ID").text();
    var removeReportFunction = function () {
        $.ajax({
            url: 'Remove',
            type: 'POST',
            data: JSON.stringify({ ID: ID }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response == true) {
                    $("#queryDetails").html("<p>GetResourceValue(Query_Remove_SuccessMessage)</p>");
                    searchQueries();
                } else {
                    toastr["error"]("GetResourceValue(Query_Remove)", "GetResourceValue(Query_Remove_ErrorMessage)");
                }
            }
        });
    }

    loadConfirmationModal("GetResourceValue(Query_Remove_Question)", removeReportFunction);
});

/* ---------------------------------
 * CONFIRMATION MODAL
 * --------------------------------- */

var confirmationFunction;

function loadConfirmationModal(message, successFunction) {
    var modal = document.getElementById("confirmationModal");
    confirmationFunction = successFunction;
    $(modal).modal('show');
    $("#confirmationMessage").text(message);
}

function passConfirmationAnswer(answer) {
    if (answer && confirmationFunction != null) {
        confirmationFunction();
    }
}