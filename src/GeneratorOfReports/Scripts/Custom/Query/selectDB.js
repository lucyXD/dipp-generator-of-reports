﻿(function ($) {

    loadTreeView();

    /* ---------------------------------
     * SELECT DB
     * --------------------------------- */

    $("#selectDBButton").click(function (e) {
        e.preventDefault();
        if ($("#selectDBForm").valid()) {
            var text = $("#selectDBButton").text();

            $("#selectDBButton").addClass("disabled");
            $("#selectDBButton").html("<div class='row center'><i class='fa fa-spinner fa-spin'></i></div>")

            $.ajax({
                url: 'SelectDatabase',
                type: 'POST',
                data: JSON.stringify({
                    "model": $('#selectDBForm').serializeObject()
                }),
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    $("#selectDBButton").removeClass("disabled");
                    $("#selectDBButton").html(text);
                    $("#DBStructure").html(response);
                    $('#selectDBModal').modal('hide');
                    loadTreeView();
                },
                error: function () {
                    $("#selectDBButton").removeClass("disabled");
                    $("#selectDBButton").html(text);
                }
            });
        }
    });

    /* ---------------------------------
     * TREEVIEW
     * --------------------------------- */

    function loadTreeView() {
        var toggler = document.getElementsByClassName("treeview");
        for (var i = 0; i < toggler.length; i++) {
            toggler[i].addEventListener("click", function () {
                var selector = this.querySelector(".nested");
                if (selector == undefined) {
                    if (!this.classList.contains("treeview-down")) {
                        this.classList.toggle("treeview-down");
                        pinTableData(this, $("#DBName").text());
                    }
                } else {
                    this.querySelector(".nested").classList.toggle("active");
                    this.classList.toggle("treeview-down");
                }
            });
        }
    }

    /* ---------------------------------
     * LAZY SHOW
     * --------------------------------- */

    function pinTableData(table, db) {
        $.ajax({
            url: 'PinTableData',
            type: 'POST',
            data: JSON.stringify({ db: db, name: table.innerHTML.trim() }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                var tableData = "<ul class='nested active'>";
                if (response.status) {
                    var data = response.data;
                    $.each(data, function (_, value) {
                        tableData += "<li>" + value.Name + "[" + value.Type + "] </li>";
                    });
                }
                tableData += "</ul>";
                $(table).append(tableData);
            }
        });
    }

})(jQuery);