﻿(function ($) {

    /* ---------------------------------
     * EDIT BUTTON
     * --------------------------------- */

    $('#editQueryButton').click(function (e) {
        e.preventDefault();

        // Button loading
        $("#editQueryButton").addClass("disabled");
        $("#editQueryButton").html("<div class='row center'><div>GetResourceValue(Global_Loading) <i class='fa fa-spinner fa-spin'></i></div>")

        // check correction of data
        checkAll().then(response => {
            if (response) {
                $.ajax({
                    url: 'Edit',
                    type: 'POST',
                    data: JSON.stringify({ model: CreateModifyModel() }),
                    contentType: 'application/json; charset=utf-8',
                    success: function (response) {
                        if (response == false) {
                            checkAll();
                            toastr["warning"]("GetResourceValue(Query_Edit_ErrorMessage)", "GetResourceValue(Query_Edit)");
                        } else {
                            window.location.reload(true);
                        }
                        $("#editQueryButton").removeClass("disabled");
                        $("#editQueryButton").html("GetResourceValue(Query_Edit)");
                    },
                    error: function () {
                        $("#editQueryButton").removeClass("disabled");
                        $("#editQueryButton").html("GetResourceValue(Query_Edit)");
                    }
                });
            } else {
                $("#editQueryButton").removeClass("disabled");
                $("#editQueryButton").html("GetResourceValue(Query_Edit)");
            }
        });
    });

})(jQuery);