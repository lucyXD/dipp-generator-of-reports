﻿(function ($) {

    /* ---------------------------------
     * CREATE BUTTON 
     * --------------------------------- */

    $('#createQueryButton').click(function (e) {
        e.preventDefault();

        // Button loading
        $("#createQueryButton").addClass("disabled");
        $("#createQueryButton").html("<div class='row center'><div>GetResourceValue(Global_Loading) <i class='fa fa-spinner fa-spin'></i></div>")

        // check correction of data
        checkAll().then(response => {
            if (response) {
                $.ajax({
                    url: 'Create',
                    type: 'POST',
                    data: JSON.stringify({ model: CreateModifyModel() }),
                    contentType: 'application/json; charset=utf-8',
                    success: function (response) {
                        if (response == false) {
                            checkAll();
                            $("#createQueryButton").removeClass("disabled");
                            $("#createQueryButton").html("GetResourceValue(Query_Create)");
                            var count = $('#errorsTable tr').length - 1;
                            toastr["warning"]("GetResourceValue(Query_Create_ErrorMessage)" + " " + count, "GetResourceValue(Query_Create)",);
                        } else {
                            window.location.reload(true);
                        }
                    },
                    error: function () {
                        $("#createQueryButton").removeClass("disabled");
                        $("#createQueryButton").html("GetResourceValue(Query_Create)");
                    }
                });
            } else {
                $("#createQueryButton").removeClass("disabled");
                $("#createQueryButton").html("GetResourceValue(Query_Create)");
            }
        });
    });

})(jQuery);