﻿/* ---------------------------------
 * VALIDATION
 * --------------------------------- */

let validator = arr => arr.every(v => v === true);

async function checkAll() {
    var validQuery = await checkQuery();
    var validBasicsSettings = await checkBasicsSettings();
    var result = validBasicsSettings && validQuery;
    return result;
}

/* DATABASE */
function checkDB() {
    var result = false;
    if ($("#DBName").text() == "") {
        $("#DBValidationMessage").text("GetResourceValue(Query_Database_ErrorMessage_Required)");
        addError("GetResourceValue(Query_Database_Select)", "GetResourceValue(Query_Database_ErrorMessage_Required)", "GetResourceValue(Query_Database)");
    } else {
        result = true;
    }
    return result;
}

/* BASICS SETTINGS FORM */
function checkBasicsSettings() {
    return new Promise(function (resolve, reject) {
        if ($("#basicsSettingsForm").valid()) {
            $.ajax({
                url: 'CheckBasicsSettingsForm',
                type: 'POST',
                data: JSON.stringify({ "model": $('#basicsSettingsForm').serializeObject() }),
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response != true) {
                        $("#basicsSettings").html(response);
                        addError("GetResourceValue(Global_BasicsSettings)", "GetResourceValue(Global_BasicsSettings_ErrorMessage)", "GetResourceValue(Global_BasicsSettings)");
                        CheckInputFields();
                        resolve(false);
                    } else {
                        $(".field-validation-error").remove();
                        resolve(true);
                    }
                }
            });
        } else {
            addError("GetResourceValue(Global_BasicsSettings)", "GetResourceValue(Global_BasicsSettings_ErrorMessage)", "GetResourceValue(Global_BasicsSettings)");
            resolve(false);
        }
    });
}

/* CHECK PARAMETERS VALIDATION */
function checkParameters() {
    var promises = []
    $("#parametersSettingsTab").find("form").each(function (index, item) {
        promises.push(new Promise((resolve, reject) => {
            var form = this;
            var data = getParameter(form);
            $.ajax({
                url: 'CheckParameter',
                type: 'POST',
                data: JSON.stringify({ model: data }),
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response == true) {
                        $(form).find(".validation-summary-errors ul li").remove();
                        resolve(true);
                    } else {
                        $(form).html(response);
                        addError("GetResourceValue(Query_Parameter)", $(form).find(".validation-summary-errors ul li").text(), (index + 1) + ".GetResourceValue(Query_Parameter)");
                        resolve(false);
                    }
                },
                error: function () {
                    resolve(false);
                }
            });
        }));
    });

    return Promise.all(promises).then(values => {
        CheckInputFields();
        if (validator(values)) {
            return true;
        } else {
            return false;
        }
    });
}

/* QUERY */
async function checkQuery() {

    // clear previuous errors displayed
    clearErrors();

    // validate DB and parameters for evaluating query
    var validDB = await checkDB();
    var validParameters = await checkParameters();

    return new Promise(function (resolve, reject) {
        if ($('#query').val().trim() == "") {
            addError("GetResourceValue(Query_Error_Empty_Type)", "GetResourceValue(Report_Query_ErrorMessage_Required)", "GetResourceValue(Report_Query)");
            resolve(false);
        } else if (validDB && validParameters) {
            $.ajax({
                url: 'CheckQuery',
                type: 'POST',
                data: JSON.stringify({
                    parameters: getParameters(),
                    query: $('#query').val(),
                    database: $("#DBName").text()
                }),
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response == true) {
                        resolve(true);
                    } else {
                        $("#errorsWarningsTab").html(response);
                        actualizeErrorsNumber();
                        resolve(false);
                    }
                }
            });
        } else {
            addError("GetResourceValue(Report_Query)", "GetResourceValue(Query_ErrorMessage_CreateTest)", "GetResourceValue(Report_Query)");
            resolve(false);
        }
    });
}

/* ---------------------------------
 * ERRORS
 * --------------------------------- */

/* NUMBER OF ERRORS */
function actualizeErrorsNumber() {
    var count = $('#errorsTable tr').length - 1;
    $("#numberOfErrors").text(count);
}

/* ADD ERROR */
function addError(name, descritpion, location) {
    // add error
    $('#errorsTable tbody').append(
        "<tr>" +
        "<td>" + name + "</td>" +
        "<td>" + descritpion + "</td>" +
        "<td>" + location + "</td>" +
        "</tr>"
    );
    // update error value
    $("#numberOfErrors").text(parseInt($("#numberOfErrors").text()) + 1);
}

/* CLEAR ERRORS TABLE */
function clearErrors() {
    $('#errorsTable tbody tr').remove();
    $("#numberOfErrors").text(0);
}

/* ---------------------------------
 * Fetching DATA
 * --------------------------------- */

function CreateModifyModel() {
    model = new Object();
    // Query
    model.Query = $('#basicsSettingsForm').serializeObject();
    model.Query.Database = $("#DBName").text();
    model.Query.Text = $('#query').val();
    // CreateReport
    model.CreateReport = $('#CreateReport').prop("checked");
    // Authorization
    model.Authorization = getAuthorization();
    // Parameters
    model.Parameters = getParameters();
    return model;
}

function getAuthorization() {
    var authorization = [];
    var nodes = $($('#authorizationTable').DataTable().rows().nodes());
    $.each(nodes, function (idx, value) {
        var cells = $(Array.from($(this).find('td')));
        var checkBoxes = $(Array.from($(this).find('input[type="checkbox"]')));
        authorization.push({
            LocalityID: cells[0].innerText,
            All: $(checkBoxes[0]).prop("checked"),
            Visibility: $(checkBoxes[1]).prop("checked"),
            Edit: $(checkBoxes[2]).prop("checked"),
            Remove: $(checkBoxes[3]).prop("checked")
        });
    });
    return authorization;
}

function getParameters() {
    var parameters = [];
    $("#parametersSettingsTab").find("form").each(function (item, _) {
        parameters.push(getParameter(this));
    });
    return parameters
}

function getParameter(p) {
    var fields = $(p).find('[disabled]');
    fields.prop('disabled', false);
    var data = $(p).serializeObject();
    var defaultValue = Array.from($(p).find(".defaultValue"))[0]
    data.ModelJson = JSON.stringify($(defaultValue).serializeParameter());
    fields.prop('disabled', true);
    return data;
}

/* ---------------------------------
 * DEBUG / RUN -> QUERY
 * --------------------------------- */

const queriesOutputTable = $("#outputQueriesTable");
var runQueryButton = $('#runQueryButton');

runQueryButton.click(function (e) {
    e.preventDefault();
    // Button loading
    runQueryButton.addClass("disabled");
    runQueryButton.html("<div class='row center'><i class='fa fa-spinner fa-spin'></i></div>")
    // check correction of data
    runQuery();
});

function runQuery() {
    checkQuery().then(response => {
        if (response) {
            $.ajax({
                url: "EvaluateQuery",
                type: 'POST',
                data: JSON.stringify({
                    parameters: getParameters(),
                    query: $('#query').val(),
                    database: $("#DBName").text()
                }),
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response.status != true) {
                        queriesOutputTable.html("<table><tbody><tr><td class='text-important'>GetResourceValue(Query_ErrorMessage_CreateTest)</td></tr></tbody></table>");
                    } else {
                        loadQueryDataTable(response.result);
                    }
                    // Button reset
                    $('#runQueryButton').removeClass("disabled");
                    $('#runQueryButton').html("<span class='fa fa-play'></span>");
                },
                error: function () {
                    // Button reset
                    $('#runQueryButton').removeClass("disabled");
                    $('#runQueryButton').html("<span class='fa fa-play'></span>");
                }
            });
        } else {
            $('#runQueryButton').removeClass("disabled");
            $('#runQueryButton').html("<span class='fa fa-play'></span>");
            queriesOutputTable.html("<table><tbody><tr><td class='text-important'>GetResourceValue(Query_ErrorMessage_CreateTest)</td></tr></tbody></table>");
        }
    });
}

function loadQueryDataTable(data) {
    // clear previous
    queriesOutputTable.html("<table></table>");
    // remove previous table data
    if ($.fn.DataTable.isDataTable('#outputQueriesTable')) {
        queriesOutputTable.DataTable().destroy();
        queriesOutputTable.empty();
    }
    // add dynamic header (No of columns)
    var columns = [];
    var TableHeader = "<thead><tr>";
    $.each(data[0], function (key, value) {
        columns.push({ "data": key });
        TableHeader += "<th>" + key + "</th>";
    });
    TableHeader += "</thead></tr>";
    queriesOutputTable.append(TableHeader);
    $('#outputQueriesTable thead th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" placeholder="' + title + '" />');
    });
    // load datatable
    if (data == "") {
        queriesOutputTable.html("<table><tbody><tr><td class='center'>GetResourceValue(DataTable_InfoEmpty)</td></tr></tbody></table>");
    } else {
        queriesOutputTable.dataTable({
            pageLength: 5,
            lengthMenu: [5, 10, 20, 50],
            processing: true,
            serverSide: false,
            dom: 'Blfrtip',
            data: data,
            columns: columns,
            initComplete: function () {
                // add searching for columns
                this.api().columns().every(function () {
                    var column = this;
                    $('input', this.header()).on('keyup change clear', function () {
                        if (column.search() !== this.value) {
                            column.search(this.value).draw();
                        }
                    });
                });
            }
        });
    }
}
