﻿/* ---------------------------------
 * DB MULTISELECT PARAMETER
 * --------------------------------- */

$('#parametersSettingsTab').on('change', '.multiselectInput', function (e) {
    var form = this.closest("form");
    var defaultValue = form.getElementsByClassName("defaultValue");
    var data = ($(defaultValue).serializeParameter());
    data.Database = $("#DBName").text();
    var modelJson = JSON.stringify(data);
    LoadDbMultiselectList(defaultValue, modelJson)
});

function LoadDbMultiselectList(defaultValue, data) {
    $.ajax({
        url: '/Query/LoadMultiselect',
        type: 'POST',
        data: JSON.stringify({ modelJson: data }),
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            $(defaultValue).html(response);
            CheckInputFields();
        }
    });
}