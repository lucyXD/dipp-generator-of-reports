﻿/* ---------------------------------
 * AUTHORIYATION TABKE DETAILS
 * --------------------------------- */

var headerChecked;

function loadDetailsTable(ID) {
    headerChecked = [true, true, true, true];

    $("#authorizationDetailsTable").DataTable({
        pageLength: 5,
        lengthMenu: [5, 10, 20],
        processing: false,
        serverSide: false,
        columns: [
            {
                name: "Name",
                data: "Name",
                render: $.fn.dataTable.render.text()
            },
            {
                name: "All",
                data: "All",
                className: "selectAll"
            },
            {
                name: "Visibility",
                data: "Visibility"
            },
            {
                name: "Remove",
                data: "Remove"

            },
            {
                name: "Edit",
                data: "Edit"
            }
        ],
        columnDefs: [{
            targets: [1, 2, 3, 4],
            orderable: false,
            checkboxes: {}
        }],
        ajax: {
            url: getDataUrl(),
            data: { ID: ID },
            type: 'POST',
            datatype: "json"
        },
        initComplete: function () {
            // set header
            $.each($("#authorizationDetailsTable").find("th:gt(1)"), function (index, value) {
                value.append(" " + value.getAttribute("aria-label"))
            });

            var table = $('#authorizationDetailsTable').DataTable();
            var nodes = $(table.rows().nodes());

            $.each(nodes, function (index, value) {
                var checkBoxes = $(Array.from($(this).find('input[type="checkbox"]')));
                var data = table.row(index).data()

                // update table values from data
                if (data.All) {
                    $(checkBoxes[0]).prop("checked", true);
                } else {
                    headerChecked[0] = false;
                }
                if (data.Visibility) {
                    $(checkBoxes[1]).prop("checked", true);
                } else {
                    headerChecked[1] = false;
                }
                if (data.Edit) {
                    $(checkBoxes[2]).prop("checked", true);
                } else {
                    headerChecked[2] = false;
                }
                if (data.Remove) {
                    $(checkBoxes[3]).prop("checked", true);
                } else {
                    headerChecked[3] = false;
                }

                // only for reading
                checkBoxes.each(function () {
                    $(this).prop('disabled', true);
                })
            });

            // set header
            checkHeader();
        }
    });
}

function getDataUrl() {
    var defaultUrl = "/Authorization/LoadAuthorizationTableData";
    var t = $("#authorizationTableSection");
    var addUrl = t.attr("data-url");
    if (addUrl != undefined) return addUrl;
    return defaultUrl;
}

function checkHeader() {
    $('#authorizationDetailsTable thead tr').find('input[type="checkbox"]').each(function (index, data) {
        $(data).prop("checked", headerChecked[index]);
    });
}

/*
 * AFTER DRAW CALLL (BUG IN CHECKBOXES.JS)
 */
$(document).on('draw.dt', function () {
    $('#authorizationDetailsTable thead input[type="checkbox"]').prop('disabled', true);
    checkHeader();
});