﻿(function ($) {

    /* ---------------------------------
     * SPINNER
     * --------------------------------- */

    $(window).on('load', function () {
        function hidePreloader() {
            var preloader = $('.spinner-wrapper');
            setTimeout(function () { preloader.fadeOut(500); }, 500);
        }
        hidePreloader();
    });

    /* ---------------------------------
     * SCROLLER 
     * --------------------------------- */

    $(document).on('click', 'a.page-scroll', function (event) {
        $('html, body').stop().animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 550);
        event.preventDefault();
    });

    /* ---------------------------------
     * BACK TO TOP 
     * --------------------------------- */

    $('body').prepend('<a href="body" class="back-to-top page-scroll">Back to Top</a>');

    var scrolled = 650;
    $(window).scroll(function () {
        if ($(window).scrollTop() > scrolled) {
            $('a.back-to-top').fadeIn('500');
        } else {
            $('a.back-to-top').fadeOut('500');
        }
    });

    /* ---------------------------------
     * HIDE AND SHOW PASSWORD    
     * --------------------------------- */

    $(".icon-password").click(function () {

        var parentofSelected = $(this).parent();
        var password = parentofSelected.find('input')

        if ($(this).hasClass("fa-eye")) {
            $(password).attr('type', 'text');
            $(this).removeClass('fa-eye').addClass('fa-eye-slash');
        } else {
            $(password).attr('type', 'password');
            $(this).addClass('fa-eye').removeClass('fa-eye-slash');
        }
    })

    /* ---------------------------------
     * NOTIFICATIONS
     * --------------------------------- */

    toastr.options = {
        "debug": false,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "fadeIn": 300,
        "fadeOut": 100,
        "timeOut": 3000,
        "extendedTimeOut": 1000
    };

    /* ---------------------------------
     * Toggle Hide and Show 
     * --------------------------------- */

    $(".hideShowToggler").click(function (e) {
        e.preventDefault();
        toggleHideAndShow();
        var isTrueSet = ($('.hideShowToggler').val() === 'true');
        $('.hideShowToggler').attr('value', !isTrueSet);
    })

    function toggleHideAndShow() {
        var x = document.getElementsByClassName("hideAndShowElement")[0];
        if (x.classList.contains("hide")) {
            x.classList.remove("hide");
            $('.icon-show').hide();
            $('.icon-hide').show();
        } else {
            x.classList.add("hide");
            $('.icon-hide').hide();
            $('.icon-show').show();
        }
    }

})(jQuery);

/* -------------------------------------------------------------------------------------------------------------
 * PREVENT XSS
 * 
 * https://gomakethings.com/preventing-cross-site-scripting-attacks-when-using-innerhtml-in-vanilla-javascript/
 * ------------------------------------------------------------------------------------------------------------- */

function encodeHTML(str) {
    var temp = document.createElement('div');
    temp.textContent = str;
    return temp.innerHTML;
};

/* ---------------------------------
 * DATEPICKER
 * --------------------------------- */

function bindDatePicker() {
    $(".datepicker").datepicker({
        dateFormat: "mm/dd/yy",
        buttonText: "<i class='fa fa-calendar'></i>",
        showOn: "both",
        changemonth: true,
        changeyear: true,
        onSelect: function () {
            CheckInputFields(this);
        }
    });

    $(".fromDatepicker").datepicker({
        changeMonth: true,
        changeyear: true,
        numberOfMonths: 2,
        buttonText: "<i class='fa fa-calendar'></i>",
        showOn: "both",
        dateFormat: "mm/dd/yy",
        onSelect: function (selectedDate) {
            $(".toDatepicker").datepicker("option", "minDate", selectedDate);
            CheckInputFields(this);
            $(this).trigger('dateupdated');
        }
    });

    $(".toDatepicker").datepicker({
        changeMonth: true,
        changeyear: true,
        numberOfMonths: 2,
        dateFormat: "mm/dd/yy",
        showOn: "both",
        buttonText: "<i class='fa fa-calendar'></i>",
        onSelect: function (selectedDate) {
            $(".fromDatepicker").datepicker("option", "maxDate", selectedDate);
            CheckInputFields(this);
            $(this).trigger('dateupdated');
        }
    });
}

/* ---------------------------------
 * DATETIMEPICKER
 * --------------------------------- */

function bindDateTimePicker() {
    $(".fromDateTimePicker").datetimepicker({
        format: 'm/d/Y H:m',
        step: 1,
        buttonText: "<i class='fa fa-calendar'></i>",
        onSelectDate: function (selectedDate) {
            $(".toDateTimePicker").datetimepicker({ "minDate": selectedDate });
            CheckInputFields();
        },
        onSelectTime: function (selectedDate) {
            $(".toDateTimePicker").datetimepicker({ "minTime": selectedDate });
            CheckInputFields();
        },
    });

    $(".toDateTimePicker").datetimepicker({
        format: 'm/d/Y H:m',
        step: 1,
        buttonText: "<i class='fa fa-calendar'></i>",
        onSelectDate: function (selectedDate) {
            $(".fromDateTimePicker").datetimepicker({ "maxDate": selectedDate });
            CheckInputFields();
        },
        onSelectTime: function (selectedDate) {
            $(".fromDateTimePicker").datetimepicker({ "maxTime": selectedDate });
            CheckInputFields();
        },
    });
}

/* ---------------------------------
 * MULTISELECT
 * --------------------------------- */

function bindMultiselect() {
    $(".multiselect").SumoSelect({ search: true });
}

/* ---------------------------------
 * SERIALIZE FORM MODEL OBJECT
 * --------------------------------- */

$.fn.serializeObject = function () {
    var object = {};
    $.each(this.serializeArray(), function () {
        if (object[this.name]) {
            if (!object[this.name].push) {
                object[this.name] = object[this.name];
            }
        } else {
            object[this.name] = this.value || '';
        }
    });

    $('input[type=checkbox]:not(:checked)', this).each(function () {
        object[this.name] = false;

    });

    $('input[type=checkbox]:checked', this).each(function () {
        object[this.name] = true;
    });

    return object;
};

$.fn.serializeParameter = function () {
    var object = {};
    $.each(this.find('input, select, textarea'), function () {
        if (object[this.name]) {
            if (!object[this.name].push) {
                object[this.name] = object[this.name];
            }
        } else {
            object[this.name] = this.value || '';
        }
    });

    var multiselect = $(this).find(".multiselect");
    if (multiselect != undefined) {
        var result = [];
        multiselect.find("option").each(function (j, item) {
            if ($(item).prop("selected")) {
                result.push(item.value);
            }
        });

        var name = Array.from($(this).find(".multiselect"));
        if (name != undefined && name[0] != undefined) {
            object[name[0].getAttribute("name")] = result;
        }
    }

    return object;
};

/* ---------------------------------
 * INPUT AREAS TEXT - TOP
 * --------------------------------- */

$.fn.CheckInputFields = function () {
    if ($(this).val() != '') {
        $(this).addClass('notEmpty');
    } else {
        $(this).removeClass('notEmpty');
    }
};

CheckInputFields();

function CheckInputFields() {

    bindDatePicker();
    bindDateTimePicker();
    bindMultiselect();

    $("input, textarea").keyup(function () {
        if (this != undefined) {
            $(this).CheckInputFields();
        }
    });

    var x = document.querySelectorAll("fieldset input, fieldset textarea");
    Array.prototype.forEach.call(x, function (obj) {
        $(obj).CheckInputFields();
    });
}