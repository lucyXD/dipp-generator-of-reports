﻿(function ($) {

    var usersTable = $("#usersTable");

    /* ---------------------------------
     * USERS TABLE    
     * --------------------------------- */

    usersTable.DataTable({
        columns: [
            { "name": "ID", "data": "ID", className: "ID", render: $.fn.dataTable.render.text() },
            { "name": "PersonalNumber", "data": "PersonalNumber", render: $.fn.dataTable.render.text() },
            { "name": "Login", "data": "Login", render: $.fn.dataTable.render.text() },
            { "name": "FirstName", "data": "FirstName", render: $.fn.dataTable.render.text() },
            { "name": "Surname", "data": "Surname", render: $.fn.dataTable.render.text() },
            { "name": "Email", "data": "Email", render: $.fn.dataTable.render.text() },
            {
                data: "ID",
                orderable: false,
                render: function (data) {
                    return "<a href='#' class='openEditingModal' title='Edit user'><span class='fa fa-edit'></span></a>";
                }
            },
            {
                data: "ID",
                orderable: false,
                render: function (data) {
                    return "<a href='#' class='openDetailsModal' title='Details user'><span class='fa fa-eye'></span></a>";
                }
            }
        ],
        columnDefs: [{ "defaultContent": "-", "targets": "_all", }],
        ajax: { url: "/User/LoadUsersTableData", type: 'POST', datatype: "json" }
    });

    /* ---------------------------------
     * EDIT 
     * --------------------------------- */

    usersTable.on('click', 'a.openEditingModal', function () {
        var ID = $(this).parents("tr").find(".ID").text();
        $.ajax({
            url: 'LoadUser',
            type: 'POST',
            data: JSON.stringify({
                ID: ID,
                partialName: "Modals/_UserEditModal"
            }),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                loadEditModal(data);
            }
        });
    });

    function loadEditModal(data) {

        $("#userEditSection").html(data);
        $('#userEditModal').modal('show');
        CheckInputFields();

        // EDIT BUTTON
        $('#editUserButton').click(function (e) {
            e.preventDefault();
            $.ajax({
                url: 'Edit',
                type: 'POST',
                data: JSON.stringify({
                    model: $('#editUserForm').serializeObject(),
                    localityRoles: getLocalitiesWithRoles($('#userEditModal'))
                }),
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    if (response == true) {
                        $('#userEditModal').remove();
                        $('.modal-backdrop').remove();
                        searchUsers();
                        toastr["success"]("GetResourceValue(User_Edit_SuccessMessage)", "GetResourceValue(User_EditUser)");
                    } else if (response == false) {
                        toastr["success"]("GetResourceValue(User_Edit_ErrorMessage)", "GetResourceValue(User_EditUser)");
                    } else {
                        $('.modal-backdrop').remove();
                        loadEditModal(response);
                    }
                }
            });
        });
    }

    /* ---------------------------------
     * DETAILS
     * --------------------------------- */

    usersTable.on('click', 'a.openDetailsModal', function () {
        var ID = $(this).parents("tr").find(".ID").text();
        $.ajax({
            url: '/User/LoadUser',
            type: 'POST',
            data: JSON.stringify({
                ID: ID,
                partialName: "Modals/_UserDetailsModal"
            }),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $("#userDetailsSection").html(data);
                $('#userDetailsModal').modal('show');
                LoadAudits(ID);
                CheckInputFields();
            }
        });
    });

    // Audits

    function LoadAudits(ID) {
        $.ajax({
            url: "/User/LoadAudits",
            type: 'POST',
            data: JSON.stringify({ ID: ID }),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $("#historyOfActions").html(data);
                $("#loadingAudits").hide();
                LoadAuditsBase(ID);
            }
        })
    }

    function LoadAuditsBase(ID) {

        $('body').on('dateupdated', function (e) { loadAudits(0); });
        $("#dateFrom").change(function () { loadAudits(0); });
        $("#dateTo").change(function () { loadAudits(0); });
        $("#actionId").change(function () { loadAudits(0); });

        var pageNumber = 0;

        if ($("#timeline")[0] != undefined) {
            $("#timeline")[0].onscroll = function () {
                if ($("#timeline")[0].scrollHeight - $("#timeline").scrollTop() - $("#timeline").innerHeight() < 1 &&
                    pageNumber > -1) {
                    loadAudits(++pageNumber);
                }
            }
        }

        function loadAudits(page) {
            pageNumber = page;
            $("#loadingAudits").show();
            $.ajax({
                url: '/User/SearchAudits',
                type: 'POST',
                data: JSON.stringify({
                    "ActionDateFrom": new Date($('#dateFrom').val()),
                    "ActionDateTo": new Date($('#dateTo').val()),
                    "Action": $('#actionId').val(),
                    "PageNumber": page,
                    "ID": ID
                }),
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    if (pageNumber == 0) {
                        $("#timeline").html(data);
                    } else {
                        if (data == '') {
                            pageNumber = -1;
                        } else {
                            $("#timeline").append(data);
                        }
                    }
                    $("#loadingAudits").hide();
                }
            });
        }
    }

})(jQuery);

/* ---------------------------------
 * LOCALITY WITH ROLES TABLE
 * --------------------------------- */

function getLocalitiesWithRoles(modal) {

    var localitiesWithRoles = [];

    $(modal).find("table tr:not(:first)").each(function (index, item) {
        var selectedLocality = this.getElementsByClassName('selectedLocality')[0];
        if (typeof selectedLocality != 'undefined') {
            $.each($(Array.from(this.getElementsByClassName("removeRoleFromList"))), function () {
                localitiesWithRoles.push({
                    Locality: {
                        Name: $.trim(selectedLocality.textContent),
                        ID: selectedLocality.getAttribute("id")
                    },
                    LocalityID: selectedLocality.getAttribute("id"),
                    Role: {
                        Name: $.trim(this.textContent),
                        ID: this.getAttribute("id")
                    },
                    RoleID: this.getAttribute("id")
                });
            });
        }
    });

    return localitiesWithRoles;
}

/* ---------------------------------
 * CREATE
 * --------------------------------- */

function openCreateUserModal() {
    $.ajax({
        url: '/User/LoadCreateUserModal',
        type: 'POST',
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (data) { loadCreateModal(data) }
    });
}

function loadCreateModal(data) {

    $("#userCreateSection").html(data);
    $('#userCreateModal').modal('show');
    CheckInputFields();

    // CREATE BUTTON
    $('#createUserButton').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: 'Create',
            type: 'POST',
            data: JSON.stringify({
                model: $('#createUserForm').serializeObject(),
                localityRoles: getLocalitiesWithRoles($('#userCreateModal'))
            }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                if (response == true) {
                    $('#userCreateModal').remove();
                    $('.modal-backdrop').remove();
                    searchUsers();
                    toastr["success"]("GetResourceValue(User_Create_SuccessMessage)", "GetResourceValue(User_Create)");
                } else if (response == false) {
                    toastr["success"]("GetResourceValue(User_Create_ErrorMessage)", "GetResourceValue(User_Create)");
                } else {
                    $('.modal-backdrop').remove();
                    loadCreateModal(response);
                }
            }
        });
    });
}