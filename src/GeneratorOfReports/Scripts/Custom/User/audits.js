﻿(function ($) {

 /* -----------------------------------------------
 * Scripts for lazy loading of audits and filter
 * them according to input parameters.
 * ----------------------------------------------- */

    $("div#loadingAudits").hide();
    $('body').on('dateupdated', function (e) { loadAudits(0); });
    $("#dateFrom").change(function () { loadAudits(0); });
    $("#dateTo").change(function () { loadAudits(0); });
    $("#actionId").change(function () { loadAudits(0); });

    var pageNumber = 0;

    if ($("#timeline")[0] != undefined) {
        $("#timeline")[0].onscroll = function () {
            if ($("#timeline")[0].scrollHeight - $("#timeline").scrollTop() - $("#timeline").innerHeight() < 1 &&
                pageNumber > -1) {
                loadAudits(++pageNumber);
            }
        }
    }

    function loadAudits(page) {
        pageNumber = page;
        $("#loadingAudits").show();
        $.ajax({
            url: '/User/Audits',
            type: 'POST',
            data: JSON.stringify({
                "ActionDateFrom": new Date($('#dateFrom').val()),
                "ActionDateTo": new Date($('#dateTo').val()),
                "Action": $('#actionId').val(),
                "PageNumber": page
            }),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (pageNumber == 0) {
                    $("#timeline").html(data);
                } else {
                    if (data == '') {
                        pageNumber = -1;
                    } else {
                        $("#timeline").append(data);
                    }
                }
                $("#loadingAudits").hide();
            }
        });
    }
})(jQuery);