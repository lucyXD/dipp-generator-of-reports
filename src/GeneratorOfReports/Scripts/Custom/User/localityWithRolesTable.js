﻿(function ($) {

    /* ---------------------------------
     * ON CLICK EVENTS
     * --------------------------------- */

    $('#localityWithRolesTable tbody').on('change', '.addRoleToList', function (e) {
        addRoleToList(this.closest('tr'), $(this).val());
    })

    $('#localityWithRolesTable tbody').on('click', '.removeRoleFromList', function (e) {
        removeRoleFromList(this.closest('tr'), this.getAttribute("id"));
    })

    $('#localityWithRolesTable tbody').on('click', '.addOption', function (e) {
        addOption(this.closest('tr'));
    })

    $('#localityWithRolesTable tbody').on('click', '.removeOption', function (e) {
        removeOption(this.closest('tr'));
    })

    /* ---------------------------------
    * FUNCTIONS
    * --------------------------------- */

    function loadElements(tr) {

        var selectedLocality = tr.getElementsByClassName('selectedLocality')[0];
        var localities;
        var localityID;
        
        if (selectedLocality == null) {
            // localities
            localities = Array();
            var localityOptions = Array.from($(tr).find('.localityOption > option'));
            $(localityOptions).each(function () {
                if ($(this).val() != "") {
                    localities.push({
                        Value: $(this).val(),
                        Text: $(this).text()
                    });
                }
            });
            // localityID
            localityID = $(tr).find('.localityOption option:selected').val();
            // Selected locality
            selectedLocality = $(tr).find('.localityOption option:selected').text()
        } else {
            // localities
            localities = Array();
            // localityID
            localityID = selectedLocality.getAttribute("id");
            // Selected locality
            selectedLocality = selectedLocality.textContent;
        }

        // Selected roles
        var selectedRoles = Array.from(tr.getElementsByClassName("removeRoleFromList"));
        selectedRoles = selectedRoles.map(element =>
            ({
                ID: element.getAttribute("id"),
                Name: element.textContent
            })
        );

        // Roles
        var roles = Array();
        var rolesOptions = Array.from($(tr).find('.roleOption > option'));
        $(rolesOptions).each(function () {
            if ($(this).val() != "") {
                roles.push({
                    Value: $(this).val(),
                    Text: $(this).text()
                });
            }
        });

        var model = {
            LocalitySelectViewModel: {
                "ID": localityID,
                "SelectList": localities,
                "Selected": selectedLocality
            },
            RoleSelectViewModel: {
                "SelectedList": selectedRoles,
                "SelectList": roles,
                "ID": null
            }
        };

        return model;
    }

    function addRoleToList(tr, roleID) {
        var model = loadElements(tr);
        model.RoleSelectViewModel.ID = roleID;
        $.ajax({
            url: '/LocalityWithRoles/AddRoleToList',
            type: 'POST',
            data: JSON.stringify({ "model": model }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) { $(tr).html(response); }
        });
    }

    function removeRoleFromList(tr, roleID) {
        var model = loadElements(tr);
        model.RoleSelectViewModel.ID = roleID;
        $.ajax({
            url: '/LocalityWithRoles/RemoveRoleFromList',
            type: 'POST',
            data: JSON.stringify({ "model": model }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) { $(tr).html(response); }
        });
    }

    function addOption(tr) {
        var model = loadElements(tr);
        $.ajax({
            url: '/LocalityWithRoles/AddOption',
            type: 'POST',
            data: JSON.stringify({ "model": model }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (model.LocalitySelectViewModel.ID == "") {
                    $(tr).html(response);
                } else {
                    $(tr).html(response);
                    GenerateLocalityWithRolesSelect(tr);
                }
            }
        });
    }

    function GenerateLocalityWithRolesSelect(tr) {
        var tbody = $(tr).closest(".localityWithRolesTableBody");
        localities = Array.from($(tbody).find('.selectedLocality'));
        localities = localities.map(element => element.textContent);

        $.ajax({
            url: '/LocalityWithRoles/GenerateLocalityWithRolesSelect',
            type: 'POST',
            data: JSON.stringify({ "localities": localities }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                $(tbody).append("<tr>" + response + "</tr>");
            }
        });
    }

    function removeOption(tr) {

        var tbody = $(tr).closest(".localityWithRolesTableBody");
        var lastTr = $(tbody).find("tr")[$(tbody).find("tr").length - 1];
        var model = loadElements(lastTr);

        // Removed locality
        removedlocality = tr.getElementsByClassName('selectedLocality')[0];
        // localityID
        removedlocalityID = removedlocality.getAttribute("id");
        // Selected locality
        removedlocalityName = removedlocality.textContent;

        $.ajax({
            url: '/LocalityWithRoles/RemoveOption',
            type: 'POST',
            data: JSON.stringify({
                "removedLocalityID": removedlocalityID,
                "removedLocalityName": removedlocalityName,
                "model": model
            }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                $(tr).remove();
                if (model.LocalitySelectViewModel.SelectList == undefined) {
                    $(tbody).append("<tr>" + response + "</tr>");
                } else {
                    $(lastTr).html(response);
                }
            }
        });
    }

})(jQuery);