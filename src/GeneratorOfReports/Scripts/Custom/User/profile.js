﻿(function ($) {

    /* ---------------------------------
     * EDIT PROFILE
     * --------------------------------- */

    $('#editProfileSection').on('click', ' #editProfileButton', function (e) {
        e.preventDefault();
        $("#editProfileButton").addClass("disabled");
        if ($("#editProfileForm").valid()) {
            $.ajax({
                url: 'UpdateProfile',
                type: 'POST',
                data: JSON.stringify({ model: $("#editProfileForm").serializeObject() }),
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response == true) {
                        window.location.reload(true);
                    } else {
                        $('.modal-backdrop').remove();
                        $("#editProfileSection").html(response);
                        $('#editProfileModal').modal('show');
                        CheckInputFields();
                    }
                }
            });
        }
        $("#editProfileButton").removeClass("disabled");
    });

})(jQuery);