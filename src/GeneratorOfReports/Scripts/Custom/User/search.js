﻿/* ---------------------------------
 * SEARCHING
 * --------------------------------- */

var form = $("#userSearchForm");

form.map(function () {
    $(form).find('input').on('keyup', function () {
        searchUsers();
    });
    $(form).find('input[type="checkbox"]').on('change', function () {
        searchUsers();
    });
    $(form).find('select').on('change', function () {
        searchUsers();
    });
});

$(".datepickersearch").datepicker({
    dateFormat: "mm/dd/yy",
    buttonText: "<i class='fa fa-calendar'></i>",
    showOn: "both",
    changemonth: true,
    changeyear: true,
    onSelect: function () {
        searchUsers();
        CheckInputFields(this);
    }
});

function searchUsers() {
    searchUsers(null, null);
}

function searchUsers(selectedList, view) {

    if (view == null) {
        var rolesSelect = getSelection($("#rolesSelectPartial"));
        var localitiesSelect = getSelection($("#localitiesSelectPartial"));
    } else if (view == "roleSelection") {
        var rolesSelect = selectedList;
        var localitiesSelect = getSelection($("#localitiesSelectPartial"));
    } else {
        var rolesSelect = getSelection($("#rolesSelectPartial"));
        var localitiesSelect = selectedList;
    }

    var data = JSON.stringify({
        "UserViewModel": $(form).serializeObject(),
        "RoleSelectViewModel": rolesSelect,
        "LocalitySelectViewModel": localitiesSelect
    });

    $("#usersTable").DataTable().search(data).draw();
}

function getSelection(partialView) {
    var selectedList = Array.from(partialView.find(".removeSelection"));
    selectedList = selectedList.map(element => ({ ID: element.getAttribute("removedID") }));
    return { SelectedList: selectedList };
}

(function ($) {

    /* ---------------------------------
     * ON CLICK EVENTS
     * --------------------------------- */

    $('.selection').on('change', ".addSelection", function (e) {
        var selectedList = addToList(this.closest('div'), $(this).val());
        searchUsers({ SelectedList: selectedList }, this.closest('div').getAttribute("id"));
    })

    $('.selection').on('click', ".removeSelection", function (e) {
        var selectedList = removeFromList(this.closest('div'), this.getAttribute("removedID"));
        searchUsers({ SelectedList: selectedList }, this.closest('div').getAttribute("id"));
    })

    /* ---------------------------------
     * ADD / REMOVE SELECTIONS
     * --------------------------------- */

    function loadData(selection) {

        // Select List
        var selectList = Array();
        var options = Array.from($(selection).find('.addSelection > option'));
        $(options).each(function () {
            if ($(this).val() != "") {
                selectList.push({
                    Value: $(this).val(),
                    Text: $.trim($(this).text())
                });
            }
        });

        // Selected options
        var selectedList = Array.from(selection.getElementsByClassName("removeSelection"));
        selectedList = selectedList.map(element =>
            ({
                ID: element.getAttribute("removedID"),
                Name: $.trim(element.textContent)
            })
        );

        var model = {
            "SelectList": selectList,
            "SelectedList": selectedList
        };

        return model;
    }

    function addToList(selection, ID) {

        var model = loadData(selection);
        var addUrl = selection.getAttribute("add-url");
        model.ID = ID;

        $.ajax({
            url: '/LocalityWithRoles/' + addUrl,
            type: 'POST',
            data: JSON.stringify({ "model": model }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) { $(selection).replaceWith(response); }
        });

        model.SelectedList.push({ ID: ID });
        return model.SelectedList;
    }

    function removeFromList(selection, ID) {

        var model = loadData(selection);
        var removeUrl = selection.getAttribute("remove-url");
        model.ID = ID;

        $.ajax({
            url: '/LocalityWithRoles/' + removeUrl,
            type: 'POST',
            data: JSON.stringify({ "model": model }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) { $(selection).replaceWith(response); }
        });

        var index = model.SelectedList.map(x => { return x.ID; }).indexOf(ID);
        model.SelectedList.splice(index, 1);
        return model.SelectedList;
    }

})(jQuery);