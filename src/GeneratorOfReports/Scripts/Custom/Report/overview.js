﻿/* ---------------------------------
 * REPORTS TABLE
 * --------------------------------- */

var reportsTable = $("#reportsTable");

reportsTable.DataTable({
    columns: [
        {
            name: "ID", data: "ID",
            className: "ID",
            render: $.fn.dataTable.render.text()
        },
        {
            name: "Order", data: "Order",
            className: "order",
            render: $.fn.dataTable.render.text()
        },
        {
            name: "Name",
            data: "Name",
            render: $.fn.dataTable.render.text()
        },
        {
            name: "Description",
            data: "Description",
            render: function (data, type, row) {
                if (data != null && data.length > 50) {
                    return encodeHTML(data.substr(0, 50) + "...");
                } else {
                    return encodeHTML(data);
                }
            }
        },
        {
            name: "CanRemove",
            data: "CanRemove",
            orderable: false,
            render: function (data) {
                if (data) {
                    return "<a href='#' class='removeReport' title='Remove report'><span class='fa fa-trash'></span></a>"
                }
            }
        },
        {
            data: 'CanEdit',
            orderable: false,
            render: function (data, type, row, meta) {
                if (data) {
                    return "<a href='#' class='openEditingModal' title='Edit report'><span class='fa fa-edit'></span></a>"
                }

            }
        },
        {
            data: 'CanEdit',
            orderable: false,
            render: function (data, type, row, meta) {
                if (data) {
                    return "<a href='#' class='openEditOrderModal' title='Open modal for editing order of report'><span class='fa fa-sort'></span></a>"
                }

            }
        },
        {
            data: 'CanGenerate',
            orderable: false,
            render: function (data, type, row, meta) {
                if (data && row.HasQueries) {
                    return "<a href='/Report/Generate?modelID=" + row.ID + "' title='Generate report'><span class='fa fa-download'></span></a>"
                }

            }
        },
        {
            data: "ID",
            orderable: false,
            render: function (data) {
                return "<a href='#' class='openDetailsModal' title='Details report'><span class='fa fa-eye'></span></a>"
            }
        },
        {
            data: "HasQueries",
            orderable: false,
            render: function (data) {
                if (!data) {
                    return "<span class='text-important fa fa-exclamation-triangle' title='No queries for report'></span>"
                }
            }
        },
    ],
    order: [[1, "asc"]],
    columnDefs: [{ "defaultContent": "", "targets": "_all", }],
    ajax: { url: "LoadReportsTableData", type: 'POST', datatype: "json" }
});

/* ---------------------------------
 * SEARCHING
 * --------------------------------- */

var reportSearchForm = $("#reportSearchForm");

function searchReports() {
    data = JSON.stringify(reportSearchForm.serializeObject());
    reportsTable.DataTable().search(data).draw();
}

reportSearchForm.map(function () {
    $(reportSearchForm).find('select').on('change', function () {
        searchReports();
    });

    $(reportSearchForm).find('input, textarea').on('change, keyup', function () {
        searchReports();
    });
});

/* ---------------------------------
 * CHANGE ORDER FUNCTIONS
 * --------------------------------- */

var actualOrder;
var newOrder;
var ID;

reportsTable.sortable({
    items: 'tr:not(:first)',
    cursor: 'pointer',
    axis: 'y',
    dropOnEmpty: false,
    stop: function (e, tr) {
        actualOrder = $(this).find("td")[1].textContent;
        ID = $(this).find("td")[0].textContent;
        computeNewOrder(tr.item.index());
        var editOrderFunction = function () {
            $.ajax({
                url: 'MoveOrder',
                type: 'POST',
                data: JSON.stringify({
                    ID: ID,
                    newOrder: newOrder
                }),
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    if (response == true) {
                        searchReports();
                        toastr["success"]("GetResourceValue(Report_Order_Edit_SuccessMessage)", "GetResourceValue(Report_Order_Edit)");
                    } else {
                        toastr["error"]("GetResourceValue(Report_Order_Edit_ErrorMessage)", "GetResourceValue(Report_Order_Edit)");
                    }
                }
            });
        }
        loadConfirmationModal("GetResourceValue(Report_Order_Edit_Question)" + " " + newOrder, editOrderFunction);
    },
    receive: function (e, tr) {
        $(this).find("tbody").append(tr.item);
    }
});

reportsTable.on('click', 'a.openEditOrderModal', function () {

    var ID = $(this).parents("tr").find(".ID").text();
    var pos = $(this).position();
    var order = $(this).parents("tr").find("td")[1].textContent;

    $.ajax({
        url: 'LoadEditOrderModal',
        type: 'POST',
        data: JSON.stringify({
            ID: ID,
            order: order
        }),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            loadEditOrderModal(data, pos);
        }
    });
});

function loadEditOrderModal(data, pos) {

    // LOAD DATA
    $("#editOrderSection").html(data);
    $('#reportEditOrderModal').modal('show');

    // EDIT POSITION FOR MODAL
    var k = document.getElementById("reportEditOrderDialog");
    k.style.right = pos.left + 'rem';
    k.style.top = pos.top + 8 + 'rem';
    k.style.position = 'absolute';

    // HANDLE EDIT ORDER SUBMIT FORM
    $('#editOrderButton').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: 'EditOrder',
            type: 'POST',
            data: JSON.stringify({ model: $('#editOrderForm').serializeObject() }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                if (response == true) {
                    $('#reportEditOrderModal').remove();
                    $('.modal-backdrop').remove();
                    searchReports();
                    toastr["success"]("GetResourceValue(Report_Order_Edit_SuccessMessage)", "GetResourceValue(Report_Order_Edit)");
                } else {
                    $('.modal-backdrop').remove();
                    loadEditOrderModal(response, pos);
                }
            }
        });
    });
}

function computeNewOrder(position) {
    newOrder = reportsTable.DataTable().page.len() * reportsTable.DataTable().page() + (position + 1);
}

/* ---------------------------------
 * DETAILS
 * --------------------------------- */

var openedDetailsModalNo = 0;

reportsTable.on('click', 'a.openDetailsModal', function () {

    var ID = $(this).parents("tr").find(".ID").text();
    openedDetailsModalNo += 1
    if (openedDetailsModalNo == 1) {
        openedDetailsModalNo = 0;
        $.ajax({
            url: 'LoadReport',
            type: 'POST',
            data: JSON.stringify({
                ID: ID,
                partialName: "Modals/_ReportDetailsModal"
            }),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $("#reportModal").html(data);
                $('#reportDetailsModal').modal('show');
                loadDetailsTable(ID);
                $("#reportDetailsModal .openEditingModal").on('click', function () {
                    $('.modal-backdrop').remove();
                    loadEditModalData(ID);
                })
                openedDetailsModalNo = 0;
            }
        });
    }
});

/* ---------------------------------
 * REMOVE 
 * --------------------------------- */

reportsTable.on('click', 'a.removeReport', function () {

    var ID = $(this).parents("tr").find(".ID").text();
    var removeReportFunction = function () {
        $.ajax({
            url: 'Remove',
            type: 'POST',
            data: JSON.stringify({ ID: ID }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response) {
                    searchReports();
                    toastr["success"]("GetResourceValue(Report_Remove_SuccessMessage)", "GetResourceValue(Report_Remove)");
                } else {
                    toastr["error"]("GetResourceValue(Report_Remove_ErrorMessage)", "GetResourceValue(Report_Remove)");
                }
            }
        });
    }

    loadConfirmationModal("GetResourceValue(Report_Remove_Question)", removeReportFunction);
});

/* ---------------------------------
 * EDIT 
 * --------------------------------- */

var openedEditModalNo = 0;

reportsTable.on('click', 'a.openEditingModal', function () {
    openedEditModalNo += 1
    if (openedEditModalNo == 1) {
        var ID = $(this).parents("tr").find(".ID").text();
        loadEditModalData(ID);
    }
});

function loadEditModalData(ID) {
    $.ajax({
        url: 'LoadReport',
        type: 'POST',
        data: JSON.stringify({
            ID: ID,
            partialName: "Modals/_ReportEditModal"
        }),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            // SHOW MODAL
            $("#reportModal").html(data);
            $('#reportEditModal').modal('show');
            // LOAD ELEMENTS
            CheckInputFields();
            loadAuthorizationTable(ID);
            loadSelectQueriesTable(ID);

            // EDIT BUTTON
            $('#editReportButton').click(function (e) {
                e.preventDefault();
                editReport();
            });

            openedEditModalNo = 0;
        }
    });
}

/* ---------------------------------
 * CONFIRMATION MODAL
 * --------------------------------- */

var confirmationFunction;

function loadConfirmationModal(message, successFunction) {
    var modal = document.getElementById("confirmationModal");
    confirmationFunction = successFunction;
    $(modal).modal('show');
    $("#confirmationMessage").text(message);
}

function passConfirmationAnswer(answer) {
    if (answer && confirmationFunction != null) {
        confirmationFunction();
    }
}