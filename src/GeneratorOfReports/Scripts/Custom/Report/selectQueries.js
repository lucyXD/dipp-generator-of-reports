﻿/* ---------------------------------
 * QUERIES TABLE
 * --------------------------------- */

var queriesTable;
var selectedQueries = [];

loadSelectQueriesTable();

function loadSelectQueriesTable(ID) {
    // TABLE
    queriesTable = $("#selectQueriesTable").DataTable({
        processing: false,
        serverSide: false,
        columns: [
            {
                name: "ID",
                data: "ID",
                className: "ID",
                render: $.fn.dataTable.render.text()
            },
            {
                name: "Selected",
                data: "Selected",
                orderable: false,
                render: function (data) {
                    return "<input type='checkbox' autocomplete='off'>"
                }
            },
            {
                name: "Name",
                data: "Name",
                render: $.fn.dataTable.render.text()
            },
            {
                name: "Database",
                data: "Database",
                render: $.fn.dataTable.render.text()
            },
            {
                name: "Validity",
                data: "Validity",
                render: $.fn.dataTable.render.text()
            },
            {
                data: "Count",
                orderable: false,
                render: function (data) {
                    return '<input type="number" class="small" min="0" value="' + data + '">';
                }
            },
            {
                data: "null",
                orderable: false,
                render: function (data) {
                    return "<a href='#' class='showDetails' title='Show details'><span class='fa fa-eye'></span></a>"
                }
            }
        ],
        columnDefs: [
            {
                defaultContent: "-",
                targets: "_all",
            }
        ],
        stateSave: false,
        select: {
            style: 'multi'
        },
        initComplete: function () {
            loadSelectedQueries();
            this.api().columns().every(function () {
                var column = this;
                $('input, select', this.header()).on('keyup change clear', function () {
                    if (column.search() !== this.value) {
                        column.search(this.value).draw();
                    }
                });
            });
        },
        ajax: {
            url: "LoadQueriesTableData",
            type: 'POST',
            data: { ID: ID },
            datatype: "json"
        }
    });

    // DETAILS
    $('#selectQueriesTable tbody').on('click', '.showDetails', function () {
        var tr = $(this).closest('tr');
        var row = queriesTable.row(tr);
        if (row.child.isShown()) {
            row.child.hide();
            tr.removeClass('shown');
        } else {
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });

    $('#selectQueriesTable tbody').on('click', 'input[type="checkbox"]', function () {
        var count = $(this).parents("tr").find(".small");
        if (this.checked) {
            count.val(1);
        } else {
            count.val(0);
        }
    });   
}

function format(data) {
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
        '<tr>' +
        "GetResourceValue(Global_Description): " + formatNull(data.Description) +
        '</br>' +
        "GetResourceValue(Global_TimeCreate): " + formatNull(data.TimeCreate) +
        '</tr>' +
        '</table>';
}

function formatNull(data) {
    if (data == null) return "";
    return encodeHTML(data);
}

function loadSelectedQueries() {
    var table = $('#selectQueriesTable').DataTable();
    var nodes = $(table.rows().nodes());
    $.each(nodes, function (index, value) {
        var checkBoxes = $(Array.from($(this).find('input[type="checkbox"]')));
        var data = table.row(index).data();
        if (data.Selected) $(checkBoxes[0]).prop("checked", true);
    });
}

function getSelectedQueries() {
    var queries = [];
    var table = $('#selectQueriesTable').DataTable();
    var nodes = $(table.rows().nodes());
    $.each(nodes, function (index, value) {
        var checkBoxes = $(Array.from($(this).find('input[type="checkbox"]')));
        var count = $(this).find(".small").val();
        var data = table.row(index).data();
        if ($(checkBoxes[0]).prop("checked")) {
            queries.push({
                ID: data.ID,
                Count: count
            });
        }
    });
    return queries;
}