﻿/* ---------------------------------
 * EDIT BUTTON  
 * --------------------------------- */

function editReport() {

    // check correction of data
    if (checkAll()) {
        createReport = function () {
            // Button loading
            $("#editReportButton").addClass("disabled");
            $("#editReportButton").html("<div class='row center'><div>GetResourceValue(Global_Loading) <i class='fa fa-spinner fa-spin'></i></div>")

            $.ajax({
                url: 'Edit',
                type: 'POST',
                data: JSON.stringify({ model: getReportModel() }),
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response) {
                        $('#reportEditModal').remove();
                        $('.modal-backdrop').remove();
                        toastr["success"]("GetResourceValue(Report_Edit_SuccessMessage)", "GetResourceValue(Global_Edit)");
                        searchReports();
                    } else {
                        toastr["warning"]("GetResourceValue(Report_Edit_ErrorMessage)", "GetResourceValue(Global_Edit)");
                        checkAll();
                    }
                    // Button reset
                    $("#editReportButton").removeClass("disabled");
                    $("#editReportButton").html("GetResourceValue(Report_Create)");
                }
            });
        };
        if (queriesExceeded) {
            loadConfirmationModal("GetResourceValue(Report_Queries_Question_TooMany)", createReport);
        } else {
            createReport();
        }
    }
}

/* ---------------------------------
 * CREATE BUTTON
 * --------------------------------- */

$('#createReportButton').click(function (e) {
    e.preventDefault();

    // check correction of data
    if (checkAll()) {
        createReport = function () {
            $("#createReportButton").addClass("disabled");
            $("#createReportButton").html("<div class='row center'><div>GetResourceValue(Global_Loading) <i class='fa fa-spinner fa-spin'></i></div>");
            $.ajax({
                url: 'Create',
                type: 'POST',
                data: JSON.stringify({ model: getReportModel() }),
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    if (response) {
                        window.location.reload(true);
                    } else {
                        toastr["warning"]("GetResourceValue(Report_Create_ErrorMessage)", "GetResourceValue(Query_Create)");
                        checkAll();
                    }

                    // Button reset
                    $("#createReportButton").removeClass("disabled");
                    $("#createReportButton").html("GetResourceValue(Report_Create)");
                }
            });
        };
        if (queriesExceeded) {
            loadConfirmationModal("GetResourceValue(Report_Queries_Question_TooMany)", createReport);
        } else {
            createReport();
        }
    }
})

/* ---------------------------------
 * CONFIRMATION MODAL
 * --------------------------------- */

var confirmationFunction;

function loadConfirmationModal(message, successFunction) {
    var modal = document.getElementById("confirmationModal");
    confirmationFunction = successFunction;
    $(modal).modal('show');
    $("#confirmationMessage").text(message);
}

function passConfirmationAnswer(answer) {
    if (answer && confirmationFunction != null) {
        confirmationFunction();
    }
}

/* ---------------------------------
 * FETCH DATA
 * --------------------------------- */

function getReportModel() {
    var model = new Object();
    model.Report = $('#basicsSettingsForm').serializeObject();
    model.Queries = getSelectedQueries();
    model.Authorization = getAuthorization();
    return model;
}

function getAuthorization() {

    var authorization = [];
    var nodes = $($('#authorizationTable').DataTable().rows().nodes());

    $.each(nodes, function (idx, value) {
        var cells = $(Array.from($(this).find('td')));
        var checkBoxes = $(Array.from($(this).find('input[type="checkbox"]')));
        authorization.push({
            LocalityID: cells[0].innerText,
            All: $(checkBoxes[0]).prop("checked"),
            Visibility: $(checkBoxes[1]).prop("checked"),
            Edit: $(checkBoxes[2]).prop("checked"),
            Remove: $(checkBoxes[3]).prop("checked")
        });
    });

    return authorization;
}

/* ---------------------------------
 * VALIDATION
 * --------------------------------- */

function checkAll() {
    var validQueries = checkQueries();
    var validBasicsSettings = checkBasicsSettings();
    return (validQueries && validBasicsSettings);
}

var queriesExceeded;

function checkQueries() {
    var queries = getSelectedQueries();
    if (queries.length > 0) {
        var valid = true;
        $.each(queries, function (index, value) {
            if (value.Count <= 0) {
                valid = false;
            } else if (value.Count > 5) {
                queriesExceeded = true;
            }
        });
        if (valid) {
            $("#queriesValidationMessage").text("");
            return true;
        } else {
            $("#queriesValidationMessage").text("GetResourceValue(Report_Queries_ErrorMessage_PositiveCount)");
            return false;
        }
    }
    $("#queriesValidationMessage").text("GetResourceValue(Report_Queries_ErrorMessage_NoneSelected)");
    return false;
}

function checkBasicsSettings() {
    var result = $("#basicsSettingsForm").valid();
    if (result) {
        $.ajax({
            url: 'CheckBasicsSettingsForm',
            type: 'POST',
            data: JSON.stringify({ "model": $('#basicsSettingsForm').serializeObject() }),
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                if (response != true) {
                    $("#basicsSettingsForm").replaceWith(response);
                    result = false;
                    CheckInputFields();
                }
            }
        });
    }
    return result;
}