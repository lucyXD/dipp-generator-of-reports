﻿(function ($) {

    // VALIDATION

    let validator = arr => arr.every(v => v === true);

    function checkParameters() {

        var promises = [];
        $(".parameterSettings").each(function (index, item) {
            promises.push(new Promise((resolve, reject) => {

                var form = item.getElementsByTagName("form");
                var data = getParameter(form);

                $.ajax({
                    url: 'ValidateQueryParameter',
                    type: 'POST',
                    data: JSON.stringify({ model: data}),
                    contentType: 'application/json; charset=utf-8',
                    success: function (response) {
                        if (response == true) {
                            $(form).find(".validation-summary-errors").html("");
                            resolve(true);
                        } else {
                            $(form).parent().html(response);
                            CheckInputFields();
                            resolve(false);
                        }
                    },
                    error: function () {
                        resolve(false);
                    }
                });
            }));
        });

        return Promise.all(promises).then(values => { return validator(values); });
    }

    // GENERATE
    function generateReport() {
        checkParameters().then(response => {
            if (response) {
                $.ajax({
                    url: 'Generate',
                    type: 'POST',
                    data: createGenerateViewModel(),
                    contentType: 'application/json; charset=utf-8',
                    success: function (response) {
                        if (response) {
                            window.open('/Report/Download', '_blank', '');
                            toastr["success"]("GetResourceValue(Report_Generate_SuccessMessage)", "GetResourceValue(Report_Generate)");
                        } else {
                            toastr["warning"]("GetResourceValue(Report_Generate_ErrorMessage)", "GetResourceValue(Report_Generate)");
                            GoNextSTep("step0", 0);
                        }
                        $("#generateReportButton").removeClass("disabled");
                        $("#generateReportButton").html("GetResourceValue(Report_Generate)");
                    },
                    error: function (response) {
                        toastr["warning"]("GetResourceValue(Report_Generate_ErrorMessage)", "GetResourceValue(Report_Generate)");
                        window.location.reload(true);
                    }
                });
            } else {
                $("#generateReportButton").removeClass("disabled");
                $("#generateReportButton").html("GetResourceValue(Report_Generate)");
            }
        });
    };

    // MODEL
    function createGenerateViewModel() {
        model = JSON.stringify({
            Report: $("#reportBasicsDetails").serializeObject(),
            Settings: $("#reportBasicsSettings").serializeObject(),
            Queries: getQueries()
        });
        return model;
    }

    function getQueries() {
        var queries = [];
        $(".querySettings").each(function (index, item) {
            var form = item.getElementsByTagName("form");
            var data = $(form).serializeObject();
            queries.push({
                Query: data,
                Parameters: getParameters(this)
            });
        });
        return queries;
    }

    function getParameters(query) {
        var parameters = [];
        $(query).find(".parameterSettings").each(function (_, item) {
            var form = item.getElementsByTagName("form");
            parameters.push(getParameter(form));
        });
        return parameters;
    }

    function getParameter(form) {
        var parameter = $(form).serializeObject();
        var defaultValue = Array.from($(form).find(".parameterInsertedValue"))[0]
        parameter.ModelJson = JSON.stringify($(defaultValue).serializeParameter());
        return parameter;
    }

    /* ---------------------------------
     * STEPS
     * --------------------------------- */

    $.fn.multipleStepsForm = function () {
        var element = this;
        var steps = $(element).find("section");
        var count = steps.length;
        var submmitButtonName = "generateReportButton";

        // BUILDING
        var multipleStepsNav = "<ul id='multipleStepsNav' class='multipleStepsNav'></ul>";
        $(element).before(multipleStepsNav);
        steps.each(function (i) {
            $(this).wrap("<div id='step" + i + "'></div>");
            $(this).append("<div id='step" + i + "commands' class='row border-top'></div>");

            // Add to menu information header
            var name = $(this).find("h4").html();
            $("#multipleStepsNav").append("<li id='StepsNavStep" + i + "'>GetResourceValue(Global_Step) " + (i + 1) + "<span>" + name + "</span></li>");

            // Buttons
            if (i == 0) {
                createNextButton(i);
                selectStep(i);
            } else if (i == count - 1) {
                $("#step" + i).hide();
                createPreviousButton(i);
                createSubmitButton(i);
            } else {
                $("#step" + i).hide();
                createPreviousButton(i);
                createNextButton(i);
            }
        });

        function selectStep(i) {
            $("#multipleStepsNav li").removeClass("text-important");
            $("#StepsNavStep" + i).addClass("text-important");
        };

        // CREATE PREVIOUS BUTTON
        function createPreviousButton(i) {
            var stepName = "step" + i;
            $("#" + stepName + "commands").append("<a href='#' id='" + stepName + "Prev' class='btn btn-white-sm'>< GetResourceValue(DataTable_Previous)</a>");
            $("#" + stepName + "Prev").bind("click", function (e) {
                $("#" + stepName).hide();
                $("#step" + (i - 1)).show();
                selectStep(i - 1);
            });
        };

        // CREATE NEXT BUTTON
        function createNextButton(i) {
            var stepName = "step" + i;
            $("#" + stepName + "commands").append("<a href='#' id='" + stepName + "Next' class='btn btn-orange-sm items-right'>GetResourceValue(DataTable_Next) ></a>");
            $("#" + stepName + "Next").bind("click", function (e) {
                CheckStepAndMove(stepName, i);
            });
        };

        // CREATE SUBMIT BUTTON
        function createSubmitButton(i) {
            var stepName = "step" + i;
            $("#" + stepName + "commands").append("<a href='#' id='" + submmitButtonName + "' class='btn btn-green-sm items-right'>GetResourceValue(Report_Generate) <span class='fa fa-check'></span></a>");
            $("#" + submmitButtonName).bind("click", function (e) {
                $("#" + submmitButtonName).addClass("disabled");
                $("#" + submmitButtonName).html("<div class='row center'><div>GetResourceValue(Report_Generate) <i class='fa fa-spinner fa-spin'></i></div>")
                generateReport();
            });
        }

        // MOVING
        function CheckStepAndMove(stepName, step) {
            $("#" + stepName).hide();
            $("#step" + (step + 1)).show();
            selectStep(step + 1);
        }
    };

    $("#multipleStepsForm").multipleStepsForm();

})(jQuery); 