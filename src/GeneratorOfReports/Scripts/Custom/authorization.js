﻿/* ---------------------------------
 * AUTHORIZATION TABLE
 * --------------------------------- */

var authorizationTable;
loadAuthorizationTable($("#ID").val());

function loadAuthorizationTable(ID) {
    authorizationTable = $("#authorizationTable").DataTable({
        pageLength: 5,
        lengthMenu: [5, 10, 20],
        processing: false,
        serverSide: false,
        columns: [
            {
                name: "ID",
                data: "ID",
                className: "ID",
                render: $.fn.dataTable.render.text()
            },
            {
                name: "Name",
                data: "Name",
                render: $.fn.dataTable.render.text()
            },
            {
                name: "All",
                data: "All",
                className: "selectAll"
            },
            {
                name: "Visibility",
                data: "Visibility"
            },
            {
                name: "Remove",
                data: "Remove"

            },
            {
                name: "Edit",
                data: "Edit"
            }
        ],
        columnDefs: [{
            targets: [2, 3, 4, 5],
            orderable: false,
            checkboxes: {
                selectRow: true
            },
            stateSave: true,
            select: {
                style: 'multi'
            },
        }],
        ajax: {
            url: getDataUrl(),
            data: { ID: ID },
            type: 'POST',
            datatype: "json"
        },
        initComplete: function () {
            // set header
            $.each($("#authorizationTable").find("th:gt(1)"), function (index, value) {
                value.append(" " + value.getAttribute("aria-label"))
            });

            // load data from controller
            var table = $('#authorizationTable').DataTable();
            var nodes = $(table.rows().nodes());
            $.each(nodes, function (index, value) {
                var checkBoxes = $(Array.from($(this).find('input[type="checkbox"]')));
                var data = table.row(index).data()
                // update table values from data
                if (data.All) $(checkBoxes[0]).prop("checked", true);
                if (data.Visibility) $(checkBoxes[1]).prop("checked", true);
                if (data.Edit) $(checkBoxes[2]).prop("checked", true);
                if (data.Remove) $(checkBoxes[3]).prop("checked", true);
            });

            // check all header columns
            checkColumns();
        }
    });

    authorizationTable = $("#authorizationTable").DataTable();

    /* CLICK EVENTS */

    $('#authorizationTable tbody').on('click', ' input[type="checkbox"] ', function () {

        var td = $(this).closest('td');
        if ($(td).hasClass("selectAll")) return;

        // column checkall
        var n = 0;
        $('#authorizationTable tbody tr').each(function (ju, data) {
            $(data).find('input[type="checkbox"]').each(function (j, d) {
                if (j == $(td).index() - 2 && $(d).prop("checked")) {
                    n++;
                }
            });
        });
        checked = (n == $('#authorizationTable tbody tr').length);
        $('#authorizationTable thead tr').find('input[type="checkbox"]').each(function (j, d) {
            if (j == $(td).index() - 2) {
                changeCheck($(d), checked);
            }
        });

        // rowcheck 
        var tr = $(this).closest('tr');
        var checkBoxes = $(Array.from($(tr).find('input[type="checkbox"]')));

        // if visibility was unchecked => unchecked remove/edit option
        if ($(td).index() == 3) {
            if (!$(this).prop("checked")) {
                $(tr).find('input[type="checkbox"]').each(function (index, cell) {
                    changeCheck($(cell), false);
                });
            }
        } else {
            // if edit/remove was checked automatic check visibility
            if ($(this).prop("checked")) {
                $(checkBoxes[1]).prop("checked", true);
            }
        }

        // all
        n = getNoChecked($(tr).find('input[type="checkbox"]:gt(0)'));
        checked = n == 3;
        $(checkBoxes[0]).prop("checked", n == 3);

        // check all header columns
        checkColumns();
    })

    /* CLICK HEAD */

    $('#authorizationTable thead').on('click', ' input[type="checkbox"] ', function () {

        var th = $(this).closest('th');
        if ($(th).hasClass("selectAll")) return;

        if (this.checked) {

            // check all
            var n = getNoChecked($('#authorizationTable thead tr input[type = "checkbox"]'));

            if (n == 3) {
                $('#authorizationTable thead .selectAll  input[type="checkbox"], #authorizationTable tbody .selectAll  input[type="checkbox"]').prop("checked", true);
            } else {
                //check each line
                $('#authorizationTable tbody tr').each(function (indexRow, row) {
                    n = 0;
                    $(row).find('input[type = "checkbox"]').each(function (indexCell, cell) {
                        if (indexCell != $(th).index() - 2 && $(cell).prop("checked")) {
                            n++;
                        }
                    });
                    if (n == 2) {
                        $(row).find('.selectAll input[type = "checkbox"]').prop("checked", true);
                    }
                });
            }
        } else {
            $('#authorizationTable thead .selectAll  input[type="checkbox"], #authorizationTable tbody .selectAll  input[type="checkbox"]').prop("checked", false);
        }
    })

    /* Select All column: select whole line */

    $('#authorizationTable tbody').on('click', '.selectAll input[type="checkbox"]', function () {

        var checked = this.checked;

        // check & uncheck row
        var tr = $(this).closest('tr');
        $(tr).find('input[type="checkbox"]').each(function () {
            $(this).prop('checked', checked);
        });

        // column check
        var n = getNoChecked($('#authorizationTable tbody tr .selectAll'));
        if (n == $('#authorizationTable tbody tr').length) {
            $('#authorizationTable input[type="checkbox"]').each(function () {
                changeCheck($(this), true);
            });
        } else {
            checkColumns();
        }
    })

    $('#authorizationTable thead').on('click', '.selectAll input[type="checkbox"]', function () {
        // set on other pages
        setAll(this.checked);
        // check header
        checkColumns();
    })

    /*
     * AFTER DRAW CALLL (BUG IN CHECKBOXES.JS)
     */
    $("#authorizationTable").on('draw.dt', function () {
        checkColumns();
    });
}

function getDataUrl() {
    var defaultUrl = "/Authorization/LoadAuthorizationTableData";
    var t = $("#authorizationTableSection");
    var addUrl = t.attr("data-url");
    if (addUrl != undefined) return addUrl;
    return defaultUrl;
}

function setAll(checked) {
    var nodes = $(authorizationTable.rows().nodes());
    $.each(nodes, function (idx, value) {
        $(this).find('input[type="checkbox"]').each(function () {
            changeCheck($(this), checked);
        });
    });
}

function getNoChecked(checkboxes) {
    var n = 0;
    checkboxes.each(function (index, data) {
        if ($(data).prop('checked')) {
            n++;
        }
    });
    return n;
}

function checkColumns() {
    // fetch data if selected all
    var headerChecked = [true, true, true, true]
    var nodes = $(authorizationTable.rows().nodes());
    $.each(nodes, function (idx, value) {
        $(this).find('input[type="checkbox"]').each(function (index, data) {
            headerChecked[index] = headerChecked[index] && $(data).prop("checked");
        });
    });

    // set header
    $('#authorizationTable thead tr').find('input[type="checkbox"]').each(function (index, data) {
        changeCheck($(data), headerChecked[index]);
    });
}

function changeCheck(checkbox, checked) {
    checkbox.prop('indeterminate', false);
    checkbox.prop('checked', checked);
}