﻿using GeneratorOfReports.Models;
using System.Collections.Generic;
using System.Linq;

namespace GeneratorOfReports.Repositories
{
    public class AuditRepository : Repository<Audit>
    {
        public AuditRepository(GORContext _context) : base(_context) { }

        #region Getters

        public virtual List<Audit> GetByUserID(int ID)
            => _context.Audits.Where(u => u.UserID == ID).ToList();

        #endregion
    }
}