﻿using GeneratorOfReports.Models;

namespace GeneratorOfReports.Repositories
{
    public class ReportRepository : Repository<Report>
    {
        public ReportRepository(GORContext _context) : base(_context) { }
    }
}