﻿using GeneratorOfReports.Models;

namespace GeneratorOfReports.Repositories
{
    public class RoleRepository : Repository<Role>
    {
        public RoleRepository(GORContext _context) : base(_context) { }
    }
}