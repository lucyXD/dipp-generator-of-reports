﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;

namespace GeneratorOfReports.Repositories
{
    public interface IRepository<T> where T : class
    {
        T Get(int id);
        IEnumerable<T> GetAll();
        void Add(T entity);
        void AddRange(IEnumerable<T> entities);
        void Remove(T entity);
        void RemoveRange(IEnumerable<T> entities);
        void Update(T entity);
        DbEntityEntry GetEntry(T entity);
    }
}