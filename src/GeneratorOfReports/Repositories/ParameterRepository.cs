﻿using GeneratorOfReports.Models;
using System.Collections.Generic;
using System.Linq;

namespace GeneratorOfReports.Repositories
{
    public class ParameterRepository : Repository<Parameter>
    {
        public ParameterRepository(GORContext _context) : base(_context) { }

        public override Parameter Get(int id)
             => _context.Parameters.FirstOrDefault(x => x.ID == id);

        public IEnumerable<Parameter> GetAllByQueryID(int queryID)
            => _context.Parameters.Where(x => x.QueryID == queryID);
    }
}