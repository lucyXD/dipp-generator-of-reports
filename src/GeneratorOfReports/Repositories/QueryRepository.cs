﻿using GeneratorOfReports.Models;

namespace GeneratorOfReports.Repositories
{
    public class QueryRepository : Repository<Query>
    {
        public QueryRepository(GORContext _context) : base(_context) { }
    }
}