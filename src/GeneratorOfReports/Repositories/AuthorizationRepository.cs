﻿using GeneratorOfReports.Models;
using System.Linq;

namespace GeneratorOfReports.Repositories
{
    public class AuthorizationRepository : Repository<Authorization>
    {
        public AuthorizationRepository(GORContext _context) : base(_context) { }

        internal Authorization Get(bool visibility, bool edit, bool remove)
            => _context.Authorizations.FirstOrDefault(x => x.Visibility == visibility && x.Edit == edit && x.Remove == remove);
    }
}