﻿using GeneratorOfReports.Models;

namespace GeneratorOfReports.Repositories
{
    public class LocalityRepository : Repository<Locality>
    {
        public LocalityRepository(GORContext _context) : base(_context) { }
    }
}