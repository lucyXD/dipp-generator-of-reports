﻿using GeneratorOfReports.Models;
using System.Linq;

namespace GeneratorOfReports.Repositories
{
    public class UserRepository : Repository<User>
    {
        public UserRepository(GORContext _context) : base(_context) { }

        public User GetByEmail(string email)
            => _context.Users.FirstOrDefault(u => u.Email.Equals(email));

        public User GetByPasswordToken(string guid)
            => _context.Users.FirstOrDefault(u => u.PasswordToken.Equals(guid));

        public User ValidateUser(string username, string password)
        {
            User user = _context.Users.FirstOrDefault(u => u.Login.Equals(username) && u.Password.Equals(password) && u.IsActive);
            return user;
        }
    }
}