﻿using GeneratorOfReports.Models;
using System.Collections.Generic;
using System.Linq;

namespace GeneratorOfReports.Repositories
{
    public class UserLocalityRoleRepository : Repository<UserLocalityRole>
    {

        public UserLocalityRoleRepository(GORContext _context) : base(_context) { }

        #region Getters 

        public List<UserLocalityRole> GetByUserID(int userID)
            => GetAll().Where(x => x.UserID == userID).ToList();

        #endregion

    }
}