﻿using GeneratorOfReports.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace GeneratorOfReports.Repositories
{
    public abstract class Repository<T> : IRepository<T> where T : class
    {
        protected GORContext _context;

        protected Repository(GORContext context)
        {
            _context = context;
        }

        public virtual T Get(int id) => _context.Set<T>().Find(id);

        public virtual IEnumerable<T> GetAll() => _context.Set<T>().ToList();

        public virtual void Add(T entity) => _context.Set<T>().Add(entity);

        public virtual void AddRange(IEnumerable<T> entities) => _context.Set<T>().AddRange(entities);

        public virtual void Remove(T entity) => _context.Set<T>().Remove(entity);

        public virtual void RemoveRange(IEnumerable<T> entities) => _context.Set<T>().RemoveRange(entities);

        public virtual void Update(T entity) => _context.Entry(entity).State = EntityState.Modified;

        public virtual DbEntityEntry GetEntry(T entity) => _context.Entry(entity);
    }
}