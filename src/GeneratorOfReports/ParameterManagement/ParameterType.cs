﻿using GeneratorOfReports.Utilities;
using GeneratorOfReports.ViewModels;

namespace GeneratorOfReports.ParameterManagement
{
    public abstract class ParameterType : IParameterType
    {
        #region Properties

        // Keyword which is safe in DB as unique identifier
        public string Keyword { get; set; }

        // for user representation view
        public string PartialName { get; set; }

        public ParameterTypeViewModel ViewModel { get; set; }

        #endregion

        #region Constructor

        protected ParameterType()
        {
            ViewModel = new ParameterTypeViewModel();
            PartialName = "Forms/ParameterTypes/_ParameterType";
        }

        #endregion

        #region Validation

        public virtual bool IsEmpty(string modelJson)
        {
            var model = FormatModel(modelJson);
            return model == null || string.IsNullOrEmpty(model.Value);
        }

        public virtual bool IsValid(string modelJson)
        {
            var model = FormatModel(modelJson);
            return (model != null);
        }

        #endregion

        #region Process Parameter

        public virtual ParameterTypeViewModel GetModel()
            => ViewModel;

        /// <summary>
        /// Get ViewModel used for user to input data for generating report.
        /// </summary>
        /// <param name="modelJson">model from user input</param>
        /// <param name="defaultModel">default model from database</param>
        /// <returns></returns>
        public virtual ParameterTypeViewModel GetViewModel(string modelJson, string defaultModel)
        {
            var model = string.IsNullOrEmpty(modelJson)
                           ? ViewModel
                           : FormatModel(modelJson);
            model.Hash = System.Guid.NewGuid().ToString();
            return ViewModel;
        }

        public virtual string ProcessParameter(string text, string name, string modelJson)
        {
            var model = FormatModel(modelJson);
            text = text.Replace(name, model.Value);
            return text;
        }

        public virtual string DisappearParameter(string text, string name)
            => text.Replace(name, "");

        #endregion

        #region Default value

        public virtual ParameterTypeViewModel GetDefaultModel(string modelJson)
        {
            var model = FormatModel(modelJson);
            model.Hash = System.Guid.NewGuid().ToString();
            return model;
        }

        public virtual string GetDefaultView()
            => PartialName;

        public virtual bool IsEmptyDefaultValue(string modelJson)
            => IsEmpty(modelJson);

        public virtual string DefaultParameter(string text, string name, string modelJson)
            => ProcessParameter(text, name, modelJson);

        public virtual bool IsValidDefault(string modelJson)
            => IsValid(modelJson);

        public virtual string DisplayDefault(string modelJson)
            => FormatModel(modelJson).Value;

        #endregion

        #region Formatting

        public virtual ParameterTypeViewModel FormatModel(string modelJson)
            => FormattingUtility.FormatJson<ParameterTypeViewModel>(modelJson);

        #endregion
    }
}