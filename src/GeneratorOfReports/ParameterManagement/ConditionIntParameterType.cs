﻿using GeneratorOfReports.Utilities;
using GeneratorOfReports.ViewModels;
using System.Collections.Generic;

namespace GeneratorOfReports.ParameterManagement
{
    public class ConditionIntParameterType : ParameterType
    {
        #region Properties

        private readonly List<string> _options = new List<string>() { "=", "<>", "<=", ">=", "<", ">" };

        #endregion

        #region Constructor

        public ConditionIntParameterType()
        {
            Keyword = "CONDITION_INT";
            ViewModel = new ConditionIntParameterTypeViewModel
            {
                ComparisonsSelectList = FormattingUtility.CreateSelectList(_options)
            };
            PartialName = "Forms/ParameterTypes/_ConditionIntParameterType";
        }

        #endregion

        #region Validation

        public override bool IsEmpty(string modelJson)
        {
            var model = FormatModel(modelJson);
            return model == null || string.IsNullOrEmpty(model.Condition);
        }

        public override bool IsValid(string modelJson)
        {
            var model = FormatModel(modelJson);
            return model != null && (string.IsNullOrEmpty(model.Condition) || _options.Contains(model.Condition));
        }

        #endregion

        #region Process parameter

        public override ParameterTypeViewModel GetViewModel(string modelJson, string defaultModel)
        {
            var model = string.IsNullOrEmpty(modelJson)
                ? new ConditionIntParameterTypeViewModel()
                : FormatModel(modelJson);
            model.ComparisonsSelectList = FormattingUtility.CreateSelectList(_options);
            model.Hash = System.Guid.NewGuid().ToString();
            return model;
        }

        public override string ProcessParameter(string text, string name, string modelJson)
        {
            var model = FormatModel(modelJson);
            string declare = model.Condition + "  " + model.Value;
            text = text.Replace(name, declare);
            return text;
        }

        public override string DisappearParameter(string text, string name)
            => text.Replace(name, "< 0 or 1 = 1 ");

        #endregion

        #region Default Value

        public override string DisplayDefault(string modelJson)
        {
            var model = FormatModel(modelJson);
            return model.Condition + " " + model.Value;
        }

        public override ParameterTypeViewModel GetDefaultModel(string modelJson)
            => GetViewModel(modelJson, null);

        #endregion

        #region Formatting

        private ConditionIntParameterTypeViewModel FormatModel(string modelJson)
        => FormattingUtility.FormatJson<ConditionIntParameterTypeViewModel>(modelJson);

        #endregion
    }
}