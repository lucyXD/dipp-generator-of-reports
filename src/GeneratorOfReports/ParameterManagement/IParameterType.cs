﻿using GeneratorOfReports.ViewModels;

namespace GeneratorOfReports.ParameterManagement
{
    interface IParameterType
    {
       // Base value
        bool IsValid(string modelJson);
        bool IsEmpty(string modelJson);
        ParameterTypeViewModel GetModel();
        string ProcessParameter(string text, string name, string modelJson);
        string DisappearParameter(string text, string name);
        // Default value
        bool IsEmptyDefaultValue(string modelJson);
        bool IsValidDefault(string modelJson);
        ParameterTypeViewModel GetDefaultModel(string modelJson);
        string GetDefaultView();
        string DefaultParameter(string text, string name, string modelJson);
        string DisplayDefault(string modelJson);
    }
}
