﻿using GeneratorOfReports.Utilities;
using GeneratorOfReports.ViewModels;

namespace GeneratorOfReports.ParameterManagement
{
    public class PositiveIntParameterType : ParameterType
    {
        #region Constructor

        public PositiveIntParameterType()
        {
            Keyword = "POSITIVE_INT";
            ViewModel = new PositiveIntParameterTypeViewModel();
            PartialName = "Forms/ParameterTypes/_PositiveIntParameterType";
        }

        #endregion

        #region Validation

        public override bool IsEmpty(string modelJson)
        {
            var model = FormatModel(modelJson);
            return model == null || model.Value == 0;
        }

        public override bool IsValid(string modelJson)
        {
            var model = FormatModel(modelJson);
            return !(model == null || model.Value < 0);
        }

        #endregion

        #region Process parameter

        public override ParameterTypeViewModel GetViewModel(string modelJson, string defaultModel)
        {
            var model = string.IsNullOrEmpty(modelJson)
                ? new PositiveIntParameterTypeViewModel()
                : FormatModel(modelJson);
            model.Hash = System.Guid.NewGuid().ToString();
            return model;
        }

        public override string DisappearParameter(string text, string name)
                => text.Replace(name, "9999");

        #endregion

        #region Default value

        public override ParameterTypeViewModel GetDefaultModel(string modelJson)
            => GetViewModel(modelJson, null);

        #endregion

        #region Formatting

        private PositiveIntParameterTypeViewModel FormatModel(string modelJson)
            => FormattingUtility.FormatJson<PositiveIntParameterTypeViewModel>(modelJson);

        #endregion
    }
}