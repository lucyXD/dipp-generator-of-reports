﻿using GeneratorOfReports.Resources;
using GeneratorOfReports.Utilities;
using GeneratorOfReports.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace GeneratorOfReports.ParameterManagement
{
    public class TrueFalseParameterType : ParameterType
    {
        #region Constructor

        public TrueFalseParameterType()
        {
            Keyword = "TRUE_FALSE";
            ViewModel = new SelectParameterTypeViewModel { Options = CreateSelectList() };
            PartialName = "Forms/ParameterTypes/_SelectParameterType";
        }

        #endregion

        #region Validation

        public override bool IsEmpty(string modelJson)
        {
            var model = FormatModel(modelJson);
            return model == null || string.IsNullOrEmpty(model.Value);
        }

        public override bool IsValid(string modelJson)
            => FormatModel(modelJson) != null;

        #endregion

        #region Process parameter

        public override ParameterTypeViewModel GetViewModel(string modelJson, string defaultModel)
        {
            var model = string.IsNullOrEmpty(modelJson)
                 ? new SelectParameterTypeViewModel()
                 : FormatModel(modelJson);
            model.Options = CreateSelectList();
            model.Hash = System.Guid.NewGuid().ToString();
            return model;
        }

        public override string DisappearParameter(string text, string name)
            => text.Replace(name, " 1>2 ");

        #endregion

        #region Default Value

        public override ParameterTypeViewModel GetDefaultModel(string modelJson)
           => GetViewModel(modelJson, null);

        private SelectParameterTypeViewModel FormatModel(string modelJson)
            => FormattingUtility.FormatJson<SelectParameterTypeViewModel>(modelJson);

        private List<SelectListItem> CreateSelectList()
            => new List<SelectListItem> {
                new SelectListItem { Text = Translation.Global_Yes, Value = " 1<2 " },
                new SelectListItem { Text = Translation.Global_No, Value = " 1>2 " } };

        #endregion
    }
}