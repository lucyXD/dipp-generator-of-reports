﻿using GeneratorOfReports.Utilities;
using GeneratorOfReports.ViewModels;

namespace GeneratorOfReports.ParameterManagement
{
    public class EnumerationParameterType : ParameterType
    {
        #region Constructor

        public EnumerationParameterType()
        {
            Keyword = "ENUMERATION";
        }

        #endregion

        #region Process parameter

        public override string DisappearParameter(string text, string name)
            => text.Replace(name, " ('') OR 1=1 ");

        public override string ProcessParameter(string text, string name, string modelJson)
        {
            var model = FormatModel(modelJson);
            text = text.Replace(name, Declare(model.Value));
            return text;
        }

        #endregion

        #region Default Value

        public override ParameterTypeViewModel GetDefaultModel(string modelJson)
        {
            var model = FormatModel(modelJson);
            model.Hash = System.Guid.NewGuid().ToString();
            return model;
        }

        #endregion

        #region Formatting

        private ParameterTypeViewModel FormatModel(string modelJson)
            => FormattingUtility.FormatJson<ParameterTypeViewModel>(modelJson);

        private string Declare(string value)
          => "( " + System.Text.RegularExpressions.Regex.Replace(value, @"\w+", "\'$0\'") + " ) ";

        #endregion
    }
}