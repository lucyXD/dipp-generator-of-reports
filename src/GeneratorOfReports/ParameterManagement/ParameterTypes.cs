﻿using GeneratorOfReports.Resources;
using GeneratorOfReports.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GeneratorOfReports.ParameterManagement
{
    public class ParameterTypes : TypeUtility
    {
        #region PARAMETER TYPES

        // Positive INT
        public static readonly ParameterTypes POSITIVE_INT = new ParameterTypes(Translation.Parameter_Type_Positive_Int, new PositiveIntParameterType());

        // Logical parameter True / False
        public static readonly ParameterTypes TRUE_FALSE = new ParameterTypes(Translation.Parameter_Type_YesNo, new TrueFalseParameterType());

        //Period (from - to) for the date
        public static readonly ParameterTypes DATE = new ParameterTypes(Translation.Parameter_Type_Date, new DateParameterType());

        // Period (from - to) for date and time
        public static readonly ParameterTypes DATE_TIME = new ParameterTypes(Translation.Parameter_Type_DateTime, new DateTimeParameterType());

        // text for int conditions (=, <>, <=, >=, <, >)
        public static readonly ParameterTypes CONDITION_INT = new ParameterTypes(Translation.Parameter_Type_Condition_Int, new ConditionIntParameterType());

        // text for strings conditions (=, <>, <=, >=, <, >, contains, do not contain)
        public static readonly ParameterTypes CONDITION_STRING = new ParameterTypes(Translation.Parameter_Type_Condition_String, new ConditionStringParameterType());

        // General choice (ex1, ex2, ex3)
        public static readonly ParameterTypes ENUMERATION = new ParameterTypes(Translation.Parameter_Type_Enumeration, new EnumerationParameterType());

        // DB MultiSelect
        public static readonly ParameterTypes DB_MULTISELECT = new ParameterTypes(Translation.Parameter_Type_DbMultiselect, new DbMultiselectParameterType());

        #endregion

        #region Properties

        public ParameterType Type { get; private set; }

        #endregion

        #region Constructor

        protected ParameterTypes(string name, ParameterType type) : base(name)
        {
            Type = type;
        }

        #endregion

        #region Methods

        public static IEnumerable<SelectListItem> GetSelectList()
            => new SelectList(GetAllList<ParameterTypes>()
                .OrderBy(x => x.Name)
                .Select(x => new SelectListItem { Value = x.Type.Keyword, Text = x.Name })
                .ToList(), "Value", "Text");

        public static ParameterTypes GetValue(string keyword)
            => GetAll<ParameterTypes>().FirstOrDefault(x => x.Type.Keyword.Equals(keyword));

        #endregion
    }
}