﻿using GeneratorOfReports.Utilities;
using GeneratorOfReports.ViewModels;
using System;

namespace GeneratorOfReports.ParameterManagement
{
    internal class DateTimeParameterType : ParameterType
    {
        #region Constructor

        public DateTimeParameterType()
        {
            Keyword = "DATE_TIME";
            ViewModel = new DateParameterTypeViewModel();
            PartialName = "Forms/ParameterTypes/_DateTimeParameterType";
        }

        #endregion

        #region Validation

        public override bool IsEmpty(string modelJson)
        {
            var model = FormatModel(modelJson);
            return model == null || (model.ActionDateFrom == null && model.ActionDateTo == null);
        }

        public override bool IsValid(string modelJson)
        {
            var model = FormatModel(modelJson);
            return model != null && ((model.ActionDateFrom == null && model.ActionDateTo == null) ||
                (model.ActionDateFrom != null && model.ActionDateTo != null));
        }

        #endregion

        #region Process parameter

        public override ParameterTypeViewModel GetViewModel(string modelJson, string defaultModel)
        {
            var model = string.IsNullOrEmpty(modelJson)
                     ? new DateParameterTypeViewModel()
                     : FormatModel(modelJson);
            model.Hash = Guid.NewGuid().ToString();
            return model;
        }

        public override string ProcessParameter(string text, string name, string modelJson)
        {
            var model = FormatModel(modelJson);
            string declare = "BETWEEN " + FormatDate(model.ActionDateFrom) + " AND " + FormatDate(model.ActionDateTo);
            text = text.Replace(name, declare);
            return text;
        }

        public override string DisappearParameter(string text, string name)
            => text.Replace(name, " < CURRENT_TIMESTAMP OR 1=1 ");

        #endregion

        #region Default Value

        public override string DisplayDefault(string modelJson)
        {
            var model = FormatModel(modelJson);
            return FormattingUtility.GetDayTime(model.ActionDateFrom) + "-" + FormattingUtility.GetDayTime(model.ActionDateTo);
        }

        public override ParameterTypeViewModel GetDefaultModel(string modelJson)
           => GetViewModel(modelJson, null);

        #endregion

        #region Formatting

        private string FormatDate(DateTime? date)
            => string.Format("{0:'{{ts \\''yyyy-MM-dd  HH:mm:ss'\\'}}'}", date);

        private DateParameterTypeViewModel FormatModel(string modelJson)
            => FormattingUtility.FormatJson<DateParameterTypeViewModel>(modelJson);

        #endregion
    }
}