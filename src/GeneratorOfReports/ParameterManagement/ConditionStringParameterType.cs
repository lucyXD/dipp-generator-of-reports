﻿using GeneratorOfReports.Resources;
using GeneratorOfReports.Utilities;
using GeneratorOfReports.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace GeneratorOfReports.ParameterManagement
{
    internal class ConditionStringParameterType : ParameterType
    {
        #region Properties

        private readonly List<string> _options = new List<string>() { "= '{0}'", "<> '{0}'", "<= '{0}'", ">= '{0}'", "< '{0}'", "> '{0}'", "NOT LIKE '%{0}%'", "LIKE '%{0}%'" };

        #endregion

        #region Constructor

        public ConditionStringParameterType()
        {
            Keyword = "CONDITION_STRING";
            ViewModel = new ConditionStringParameterTypeViewModel { ComparisonsSelectList = CreateSelectList() };
            PartialName = "Forms/ParameterTypes/_ConditionStringParameterType";
        }

        #endregion

        #region Validation

        public override bool IsEmpty(string modelJson)
        {
            var model = FormatModel(modelJson);
            return model == null || string.IsNullOrEmpty(model.Condition) || string.IsNullOrEmpty(model.Value);
        }

        public override bool IsValid(string modelJson)
        {
            var model = FormatModel(modelJson);
            return model != null && (string.IsNullOrEmpty(model.Condition) || _options.Contains(model.Condition));
        }

        #endregion

        #region Process parameter

        public override ParameterTypeViewModel GetViewModel(string modelJson, string defaultModel)
        {
            var model = string.IsNullOrEmpty(modelJson)
              ? new ConditionStringParameterTypeViewModel()
              : FormatModel(modelJson);
            model.ComparisonsSelectList = CreateSelectList();
            model.Hash = System.Guid.NewGuid().ToString();
            return model;
        }

        public override string ProcessParameter(string text, string name, string modelJson)
        {
            var model = FormatModel(modelJson);
            string declare = string.Format(model.Condition, model.Value);
            text = text.Replace(name, declare);
            return text;
        }

        public override string DisappearParameter(string text, string name)
            => text.Replace(name, "< '' or 1=1 ");

        #endregion

        #region Default Value

        public override string DisplayDefault(string modelJson)
        {
            var model = FormatModel(modelJson);
            return string.Format(model.Condition, model.Value);
        }

        public override ParameterTypeViewModel GetDefaultModel(string modelJson)
            => GetViewModel(modelJson, null);

        #endregion

        #region Formatting

        private List<SelectListItem> CreateSelectList()
            => new List<SelectListItem> {
                new SelectListItem { Text = "=", Value = "= '{0}'" },
                new SelectListItem { Text = "<", Value = "< '{0}'" },
                new SelectListItem { Text = ">", Value = "> '{0}'" },
                new SelectListItem { Text = "<=", Value = "<= '{0}'" },
                new SelectListItem { Text = ">=", Value = ">= '{0}'" },
                new SelectListItem { Text = "<>", Value = "<> '{0}'" },
                new SelectListItem { Text = "=", Value = "= '{0}'" },
                new SelectListItem { Text = Translation.Global_NoContains, Value = "NOT LIKE '%{0}%'" },
                new SelectListItem { Text = Translation.Global_Contains, Value = "LIKE '%{0}%'" } };

        private ConditionStringParameterTypeViewModel FormatModel(string modelJson)
            => FormattingUtility.FormatJson<ConditionStringParameterTypeViewModel>(modelJson);

        #endregion
    }
}