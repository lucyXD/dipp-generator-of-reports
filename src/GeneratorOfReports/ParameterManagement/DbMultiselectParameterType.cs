﻿using GeneratorOfReports.ContextManagement;
using GeneratorOfReports.Utilities;
using GeneratorOfReports.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GeneratorOfReports.ParameterManagement
{
    /// <summary>
    /// Parameter type for giving options over certain column in context.
    /// </summary>
    public class DbMultiselectParameterType : ParameterType
    {
        #region Constructor

        public DbMultiselectParameterType()
        {
            Keyword = "DB_MULTISELECT";
            ViewModel = new DbMultiselectParameterTypeViewModel { Options = new MultiSelectList(new List<string>(), "Id", "Name") };
            PartialName = "Forms/ParameterTypes/_DbMultiselectParameterType";
        }

        #endregion

        #region Validation

        public override bool IsEmpty(string modelJson)
        {
            var model = FormatModel(modelJson);
            return model == null || !model.Selected.Any();
        }

        public override bool IsValid(string modelJson)
        {
            var model = FormatModel(modelJson);
            return (model != null);
        }

        #endregion

        #region Process parameter

        public override string DisappearParameter(string text, string name)
            => text.Replace(name, " ('') OR 1=1 ");

        public override string ProcessParameter(string text, string name, string modelJson)
        {
            var model = FormatModel(modelJson);
            string declare = "('" + string.Join("','", model.Selected) + "') ";
            text = text.Replace(name, declare);
            return text;
        }

        public override ParameterTypeViewModel GetViewModel(string modelJson, string defaultModel)
        {
            var model = string.IsNullOrEmpty(modelJson)
                ? new DbMultiselectParameterTypeViewModel { Options = new MultiSelectList(new List<string>(), "Id", "Name") }
                : FormatModel(modelJson);
            model.Hash = System.Guid.NewGuid().ToString();

            var baseModel = FormatModel(defaultModel);
            if (!string.IsNullOrEmpty(baseModel.Database))
            {
                ContextType context = ContextWrapper.GetValue<ContextType>(baseModel.Database);
                model.Options = context.CreateMultiselect(baseModel.TableIdentifier, baseModel.ValueIdentifier, baseModel.TextIdentifier);
            }

            return model;
        }

        #endregion

        #region Default value

        public override string DisplayDefault(string modelJson)
        {
            var model = FormatModel(modelJson);
            return "('" + string.Join("','", model.Selected) + "') ";
        }

        public override string GetDefaultView()
            => "Forms/ParameterTypes/_DbMultiselectDefaultParameterType";

        public override bool IsValidDefault(string modelJson)
        {
            var model = FormatModel(modelJson);
            return model != null && !string.IsNullOrEmpty(model.ValueIdentifier) &&
                !string.IsNullOrEmpty(model.TableIdentifier) && !string.IsNullOrEmpty(model.TextIdentifier);
        }

        public override ParameterTypeViewModel GetDefaultModel(string modelJson)
        {
            if (string.IsNullOrEmpty(modelJson)) return ViewModel;

            var model = FormatModel(modelJson);
            if (!string.IsNullOrEmpty(model.Database))
            {
                ContextType context = ContextWrapper.GetValue<ContextType>(model.Database);
                model.Options = context.CreateMultiselect(model.TableIdentifier, model.ValueIdentifier, model.TextIdentifier);
            }
            model.Hash = System.Guid.NewGuid().ToString();
            return model;
        }

        #endregion

        #region Formatting

        private DbMultiselectParameterTypeViewModel FormatModel(string modelJson)
                => FormattingUtility.FormatJson<DbMultiselectParameterTypeViewModel>(modelJson);

        #endregion
    }
}