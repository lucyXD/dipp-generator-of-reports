﻿using GeneratorOfReports.ReportGenerator.POCO;
using SautinSoft;

namespace GeneratorOfReports.ReportGenerator
{
    public class PdfGenerator : IGenerator
    {
        public byte[] Generate(GeneratedReport report)
        {
            ExcelGenerator excelGenerator = new ExcelGenerator();
            ExcelToPdf x = new ExcelToPdf();
            x.OutputFormat = ExcelToPdf.eOutputFormat.Pdf;
            byte[] excelBytes = excelGenerator.Generate(report);
            byte[] pdfBytes = x.ConvertBytes(excelBytes);
            return pdfBytes;
        }
    }
}