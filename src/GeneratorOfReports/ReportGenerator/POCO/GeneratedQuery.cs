﻿namespace GeneratorOfReports.ReportGenerator
{
    public class GeneratedQuery
    {
        public string Database { get; set; }
        public string Text { get; set; }
    }
}