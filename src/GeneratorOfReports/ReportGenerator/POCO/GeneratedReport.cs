﻿using System.Collections.Generic;

namespace GeneratorOfReports.ReportGenerator.POCO
{
    public class GeneratedReport
    {
        // Basics Information
        public Settings Settings { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string UserCreated { get; set; }

        // Queries
        public IEnumerable<GeneratedQuery> Queries { get; set; }
    }
}