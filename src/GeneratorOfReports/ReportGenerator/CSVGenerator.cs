﻿using GeneratorOfReports.ContextManagement;
using GeneratorOfReports.ReportGenerator.POCO;
using System.Data;
using System.Text;

namespace GeneratorOfReports.ReportGenerator
{
    public class CSVGenerator : IGenerator
    {
        public byte[] Generate(GeneratedReport report)
        {
            var data = new StringBuilder();
            foreach (var query in report.Queries)
            {
                // fetch data from context
                ContextType context = ContextWrapper.GetValue<ContextType>(query.Database);
                DataTable dt = context.GetDataTable(query.Text);

                if (dt != null)
                {
                    // add header info
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        data.Append(dt.Columns[i].ColumnName);
                        data.Append(",");
                    }
                    data.Append("\n");
                    // add rows
                    foreach (DataRow row in dt.Rows)
                    {
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            data.Append(row[i].ToString());
                            data.Append(",");
                        }
                        data.Append("\n");
                    }
                }
            }

            UTF32Encoding encoding = new UTF32Encoding();
            var output = encoding.GetBytes(data.ToString());

            return output;
        }
    }
}