﻿using GeneratorOfReports.ReportGenerator.POCO;

namespace GeneratorOfReports.ReportGenerator
{
    interface IGenerator
    {
        byte[] Generate(GeneratedReport report);
    }
}
