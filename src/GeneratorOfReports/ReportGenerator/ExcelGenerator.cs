﻿using GeneratorOfReports.ReportGenerator.POCO;
using System.IO;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using GeneratorOfReports.ContextManagement;
using System;
using System.Drawing;
using OfficeOpenXml.Style;
using System.Collections.Generic;

namespace GeneratorOfReports.ReportGenerator
{
    public class ExcelGenerator : IGenerator
    {
        #region Properties

        // pointer to actual row in formated sheet
        private int _pointer = 1;

        // pointer to row in data sheet
        private int _dataPointer = 1;

        #endregion

        #region Generator

        public byte[] Generate(GeneratedReport report)
        {
            byte[] output;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (var excelPackage = new ExcelPackage(new MemoryStream()))
            {
                // CREATE SHEETS
                var sheet = CreateSheet(excelPackage);
                var dataSheet = excelPackage.Workbook.Worksheets.Add("Data");

                // ADD HEADER FOR SHEET
                CreateHeader(sheet, report);
                CreateSeparationLine(sheet);

                // LOAD QUERIES
                QueriesAppend(sheet, dataSheet, report.Queries);

                output = excelPackage.GetAsByteArray();
            }

            return output;
        }

        #endregion

        #region Creating parts

        private static ExcelWorksheet CreateSheet(ExcelPackage excelPackage)
        {
            var sheet = excelPackage.Workbook.Worksheets.Add("Sheet1");
            sheet.View.ShowGridLines = false;
            sheet.DefaultColWidth = 20;
            sheet.DefaultRowHeight = 17;
            return sheet;
        }

        private void CreateHeader(ExcelWorksheet sheet, GeneratedReport report)
        {
            sheet.Cells[1, 1].Value = DateTime.Now.ToString("dd.MM.yyyy HH:mm");
            sheet.Cells[2, 1].Value = report.UserCreated;
            _pointer += 2;
        }

        private void CreateSeparationLine(ExcelWorksheet sheet)
        {
            sheet.Row(_pointer).Height = 9;
            _pointer += 1;
        }

        #endregion

        #region Query proccessing

        private void QueriesAppend(ExcelWorksheet sheet, ExcelWorksheet dataSheet, IEnumerable<GeneratedQuery> queries)
        {
            foreach (var query in queries)
            {
                QueryAppend(sheet, dataSheet, query);
            }
        }

        private void QueryAppend(ExcelWorksheet sheet, ExcelWorksheet dataSheet, GeneratedQuery query)
        {
            ContextType context = ContextWrapper.GetValue<ContextType>(query.Database);
            DataTable dt = context.GetDataTable(query.Text);

            if (dt != null || dt.Rows.Count > 0)
            {
                QueryHeader(sheet);
                QueryBody(sheet, dataSheet, dt);
                CreateSeparationLine(sheet);
            }
        }

        private void QueryHeader(ExcelWorksheet sheet)
        {
            Color colFromHex = ColorTranslator.FromHtml("#B7DEE8");
            sheet.Row(_pointer).Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Row(_pointer).Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Row(_pointer).Height = 20;
            sheet.Row(_pointer).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            sheet.Row(_pointer).Style.Font.Bold = true;
        }

        private void QueryBody(ExcelWorksheet sheet, ExcelWorksheet dataSheet, DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                // load data
                sheet.Cells[_pointer, 1].LoadFromDataTable(dt, true, TableStyles.None);
                dataSheet.Cells[_dataPointer, 1].LoadFromDataTable(dt, true, TableStyles.None);

                // add styling
                var range = sheet.Cells[_pointer, 1, _pointer + dt.Rows.Count, dt.Columns.Count];
                range.Calculate();
                range.AutoFitColumns();
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                // update pointers
                _pointer += dt.Rows.Count + 1;
                _dataPointer += dt.Rows.Count + 1;
            }
        }

        #endregion
    }
}