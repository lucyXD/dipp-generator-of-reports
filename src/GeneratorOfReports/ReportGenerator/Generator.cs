﻿using GeneratorOfReports.ReportGenerator.POCO;

namespace GeneratorOfReports.ReportGenerator
{
    /// <summary>
    /// Universal generator over all possible formats that can be added.
    /// </summary>
    public static class Generator
    {
        public static byte[] Generate(GeneratedReport generatedReport)
        {
            byte[] output = null;
            switch (generatedReport.Settings.Format)
            {
                case Format.pdf:
                    PdfGenerator pdfGenerator = new PdfGenerator();
                    output = pdfGenerator.Generate(generatedReport);
                    break;
                case Format.csv:
                    CSVGenerator csvGenerator = new CSVGenerator();
                    output = csvGenerator.Generate(generatedReport);
                    break;
                case Format.xlsx:
                    ExcelGenerator excelGenerator = new ExcelGenerator();
                    output = excelGenerator.Generate(generatedReport);
                    break;
                default: break;
            }
            return output;
        }
    }
}