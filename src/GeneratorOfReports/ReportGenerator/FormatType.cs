﻿namespace GeneratorOfReports.ReportGenerator
{
    public enum Format
    {
        xlsx,
        pdf,
        csv
    }
}