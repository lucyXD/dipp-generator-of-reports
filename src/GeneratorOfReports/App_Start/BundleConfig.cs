﻿using System.Collections.Generic;
using System.Web.Optimization;

namespace GeneratorOfReports
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                         "~/Scripts/jquery-3.4.1.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/custom-effects").Include(
                      "~/Scripts/Custom/effects.js"));           // basics effects for scroller, toggler, ...

            bundles.Add(new ScriptBundle("~/bundles/library-effects").Include(
                       "~/Scripts/bootstrap.min.js",             // for modal popups, tabs, nav, ...    
                       "~/Scripts/datatables.min.js",            // for listening, lazy loading tables       
                       "~/Scripts/datatables.checkboxes.min.js", // checkboxes in table  
                       "~/Scripts/toastr.min.js",                // pop-up notifications
                       "~/Scripts/popper.min.js",                // tooltip library for Bootstrap
                       "~/Scripts/jquery-ui.js",                 // Widgets: datepicker,... Interactions: sortable, ...
                       "~/Scripts/jquery.datetimepicker.js",     // datetimepicker
                       "~/Scripts/jquery.sumoselect.js"));       // for multiselect

            bundles.Add(new StyleBundle("~/bundles/styles").Include(
                      "~/Content/Styles/datatables.css",
                      "~/Content/Styles/sumoselect.css",
                      "~/Content/Styles/fontawesome-all.css",
                      "~/Content/Styles/jquery.datetimepicker.css",
                      "~/Content/Styles/toastr.css",
                      "~/Content/Styles/theme.css",
                      "~/Content/Styles/jquery-ui.css"));

            #region

            ///
            /// Scripts that requires translations in runtime
            ///
            var cultures = new List<string>() { "en", "de", "cs" };
            foreach (var culture in cultures)
            {
                bundles.Add(TranslatedBundle("~/bundles/translated/tables-{0}", "~/Scripts/Custom/tables.js", culture));
                // QUERY
                bundles.Add(TranslatedBundle("~/bundles/translated/query/validation-{0}", "~/Scripts/Custom/Query/validation.js", culture));
                bundles.Add(TranslatedBundle("~/bundles/translated/query/edit-{0}", "~/Scripts/Custom/Query/edit.js", culture));
                bundles.Add(TranslatedBundle("~/bundles/translated/query/create-{0}", "~/Scripts/Custom/Query/create.js", culture));
                bundles.Add(TranslatedBundle("~/bundles/translated/query/overview-{0}", "~/Scripts/Custom/Query/overview.js", culture));
                // REPORT
                bundles.Add(TranslatedBundle("~/bundles/translated/report/generate-{0}", "~/Scripts/Custom/Report/generate.js", culture));
                bundles.Add(TranslatedBundle("~/bundles/translated/report/modify-{0}", "~/Scripts/Custom/Report/modify.js", culture));
                bundles.Add(TranslatedBundle("~/bundles/translated/report/overview-{0}", "~/Scripts/Custom/Report/overview.js", culture));
                bundles.Add(TranslatedBundle("~/bundles/translated/report/selectQueries-{0}", "~/Scripts/Custom/Report/selectQueries.js", culture));
                // USER
                bundles.Add(TranslatedBundle("~/bundles/translated/user/overview-{0}", "~/Scripts/Custom/user/overview.js", culture));
            }

            #endregion

            BundleTable.EnableOptimizations = true;
        }

        private static Bundle TranslatedBundle(string path, string name, string culture)
        {
            var bundle = new Bundle(string.Format(path, culture)).Include(name);
            bundle.Transforms.Clear();
            bundle.Transforms.Add(new Attributes.JSTranslatorAttribute());
            bundle.Transforms.Add(new JsMinify());
            return bundle;
        }
    }
}