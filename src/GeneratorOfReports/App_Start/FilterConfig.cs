﻿using System.Web.Mvc;
using GeneratorOfReports.Attributes;

namespace GeneratorOfReports
{
    public static class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new AuthorizeAttribute());
            // filters.Add(new HandleErrorAttribute());
        }
    }
}
