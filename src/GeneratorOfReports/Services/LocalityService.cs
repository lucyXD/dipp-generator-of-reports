﻿using GeneratorOfReports.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GeneratorOfReports.Services
{
    public class LocalityService : Service<Locality>
    {
        #region Constructor 

        public LocalityService(UnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.CompanyRepository) { }

        #endregion

        #region Getters 

        internal List<SelectListItem> GetSelectList()
            => GetAll().Select(comapny => new SelectListItem
            {
                Value = comapny.ID.ToString(),
                Text = comapny.Name
            }).ToList();

        internal List<SelectListItem> GetSelectListExcept(List<string> exceptList)
        {
            var localities = GetSelectList();
            return exceptList == null
                ? localities
                : localities.Where(company => !exceptList.Contains(company.Text)).ToList();
        }

        internal List<SelectListItem> GetSelectListExcept(List<Locality> companies)
        {
            if (companies == null)
            {
                return GetSelectList();
            }
            List<string> exceptList = companies.Select(x => x.Name).ToList();
            return GetSelectListExcept(exceptList);
        }

        #endregion
    }
}
