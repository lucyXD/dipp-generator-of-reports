﻿using GeneratorOfReports.Models;
using GeneratorOfReports.Utilities;
using GeneratorOfReports.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;

namespace GeneratorOfReports.Services
{
    public class ReportService : Service<Report>
    {
        #region Constructor

        public ReportService(UnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.ReportRepository) { }

        #endregion

        #region Getters

        internal IEnumerable<Report> GetVisibleReports(int localityID)
            => GetAll().Where(x => x.AuthorizationReports.FirstOrDefault(y => y.LocalityID == localityID) != null);

        internal IEnumerable<AuthorizationReport> GetAuthorizations(int ID)
            => Get(ID).AuthorizationReports;

        #endregion

        #region Order

        internal int GetMaximumOrder()
        {
            var reports = GetAll();
            if (reports.Any())
            {
                return reports.Max(x => x.Order);
            }
            return 0;
        }

        internal void UpdateOrder(int ID, int order)
        {
            Report report = Get(ID);
            UpdateOrder(report, order);
            Save();
        }

        internal void UpdateOrder(Report report, int order)
        {
            if (order == report.Order) return;

            if (order < report.Order)
            {
                var reports = GetAll().Where(x => x.Order >= order && x.Order < report.Order).ToList();
                reports.ForEach(x => x.Order += 1);
            }
            else
            {
                var reports = GetAll().Where(x => x.Order <= order && x.Order > report.Order).ToList();
                reports.ForEach(x => x.Order -= 1);
            }
            report.Order = order;
        }

        #endregion

        #region Searching

        internal IEnumerable<Report> Search(ReportSearchViewModel model, string sortColumn, string sortColumnDir)
            => Search(GetAll(), model, sortColumn, sortColumnDir);

        internal IEnumerable<Report> Search(IEnumerable<Report> reports, ReportSearchViewModel model, string sortColumn, string sortColumnDir)
        {
            if (model != null)
            {
                if (!string.IsNullOrEmpty(model.Name))
                    reports = reports.Where(x => x.Name != null && x.Name.ToLower().Contains(model.Name.ToLower()));

                if (!string.IsNullOrEmpty(model.Description))
                    reports = reports.Where(x => x.Description != null && x.Description.ToLower().Contains(model.Description.ToLower()));

                if (model.Valid != null)
                    reports = reports.Where(x => x.Valid == model.Valid);
            }

            reports = (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDir))
                         ? reports.AsEnumerable().OrderBy(sortColumn + " " + sortColumnDir).ToList()
                         : reports.AsEnumerable().OrderBy("ID asc").ToList();

            return reports;
        }

        #endregion

        #region Check Locality Authorization

        internal bool CanRemove(Report report, int localityID)
        {
            var a = report.AuthorizationReports.FirstOrDefault(x => x.LocalityID == localityID);
            return a != null && a.Authorization.Remove;
        }

        internal bool CanEdit(Report report, int localityID)
        {
            var a = report.AuthorizationReports.FirstOrDefault(x => x.LocalityID == localityID);
            return a != null && a.Authorization.Edit;
        }

        internal bool IsVisible(int reportID, int localityID)
        {
            var a = Get(reportID).AuthorizationReports.FirstOrDefault(x => x.LocalityID == localityID);
            return a != null && a.Authorization.Visibility;
        }

        internal bool CanGenerate(int ID)
        {
            Report report = Get(ID);
            if (report.Valid)
            {
                ReportQuery query = report.ReportQueries.FirstOrDefault(x => !x.Query.Valid);
                return query == null;
            }
            return false;
        }

        #endregion

        #region Modify

        internal bool Remove(int ID, int localityID)
        {
            Report report = Get(ID);
            if (report != null && CanRemove(report, localityID))
            {
                UpdateOrder(ID, GetMaximumOrder());
                Remove(report);
                Save();
                return true;
            }
            return false;
        }

        internal void AddReport(ReportCreateViewModel model, List<AuthorizationReport> authorizations, int userID)
        {
            ICollection<ReportQuery> queries = model.Queries.Select(x => new ReportQuery { QueryID = x.ID, Count = x.Count }).ToList();
            Report report = new Report
            {
                Name = model.Report.Name,
                Order = GetMaximumOrder() + 1,
                Description = model.Report.Description,
                Valid = model.Report.Valid,
                UserCreateID = userID,
                TimeCreate = DateTime.Now,
                ReportQueries = queries,
                AuthorizationReports = authorizations
            };
            UpdateOrder(report, model.Report.Order);
            Add(report);
            Save();
        }

        internal void Update(ReportCreateViewModel model, List<AuthorizationReport> authorizations)
        {
            Report report = Get((int)model.Report.ID);

            report.AuthorizationReports.Clear();
            report.AuthorizationReports = authorizations;

            // query - report
            report.ReportQueries.Clear();
            report.ReportQueries = model.Queries.Select(x => new ReportQuery { QueryID = x.ID, Count = x.Count }).ToList();

            // report
            report.Name = model.Report.Name;
            report.Description = model.Report.Description;
            report.Valid = model.Report.Valid;

            // Set order
            UpdateOrder(report, model.Report.Order);

            Update(report);
            Save();
        }

        internal void CreateSingleReport(Query query)
        {
            Report report = new Report
            {
                Name = query.Name,
                Description = query.Description,
                Valid = query.Valid,
                Order = GetMaximumOrder() + 1,
                ReportQueries = new List<ReportQuery> { new ReportQuery { QueryID = query.ID, Count = 1 } },
                AuthorizationReports = query.AuthorizationQueries.Select(x => new AuthorizationReport
                {
                    LocalityID = x.LocalityID,
                    AuthorizationID = x.AuthorizationID
                }).ToList(),
                TimeCreate = query.TimeCreate,
                UserCreateID = query.UserCreateID
            };
            Add(report);
            Save();
        }

        #endregion

        #region Formators

        internal ReportViewModel CreateReportViewModel(Report report)
        => new ReportViewModel
        {
            ID = report.ID,
            Name = report.Name,
            Order = report.Order,
            Maximum = GetMaximumOrder() + 1,
            Description = report.Description,
            Valid = report.Valid,
            TimeCreate = FormattingUtility.GetDay(report.TimeCreate),
            ValidWord = FormattingUtility.GetBooleanWord(report.Valid)
        };

        internal ReportCreateViewModel CreateReportCreateViewModel()
            => new ReportCreateViewModel
            {
                Report = new ReportViewModel { Maximum = GetMaximumOrder() + 1 }
            };

        #endregion
    }
}