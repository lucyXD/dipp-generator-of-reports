﻿using GeneratorOfReports.Authentication;
using GeneratorOfReports.Models;
using GeneratorOfReports.Repositories;
using GeneratorOfReports.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;

namespace GeneratorOfReports.Services
{
    public class UserService : Service<User>
    {
        private readonly UserRepository _userRepository;

        public UserService(UnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.UserRepository)
        {
            _userRepository = unitOfWork.UserRepository;
        }

        #region Getters

        public User GetUserByEmail(string email)
            => _userRepository.GetByEmail(email);

        public string[] GetRolesInComapny(int ID, string company)
        {
            User user = _userRepository.Get(ID);
            string[] roles = user.UserLocalityRoles.Where(x => x.Locality.Name.Equals(company)).Select(x => x.Role.Name).ToArray();
            return roles;
        }

        #endregion

        #region Validation

        public Member ValidateUser(string username, string password)
        {
            var user = _userRepository.ValidateUser(username, Encryption.GetSHA256Hash(password, username));
            return user == null ? null : new Member(user);
        }

        public bool NumberOfEmail(string value, int number)
            => GetAll().Where(x => x.Email.Equals(value)).Count() <= number;

        public bool NumberOfLogin(string value, int number)
          => GetAll().Where(x => x.Login.Equals(value)).Count() <= number;

        public bool NumberOfPersonalNumber(string value, int number)
          => GetAll().Where(x => x.PersonalNumber.Equals(value)).Count() <= number;

        #endregion

        #region Searching

        public UserLocalityRole WorksInCompany(int ID, string company)
        {
            User user = _userRepository.Get(ID);
            UserLocalityRole userLocalityRole = user.UserLocalityRoles.FirstOrDefault(x => x.Locality.Name.Equals(company));
            return userLocalityRole;
        }

        public bool HasRoleInCompany(int ID, string role)
        {
            User user = _userRepository.Get(ID);
            UserLocalityRole roleUserCompany = user.UserLocalityRoles.FirstOrDefault(x => x.Role.Name.Equals(role));
            return roleUserCompany != null;
        }

        public IEnumerable<User> Search(UserSearchViewModel model, string sortColumn, string sortColumnDir)
        {
            var users = GetAll();

            if (model != null)
            {
                users = Search(users, model);
            }

            users = (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDir))
                         ? users.AsEnumerable().OrderBy(sortColumn + " " + sortColumnDir).ToList()
                         : users.AsEnumerable().OrderBy("ID asc").ToList();

            return users;
        }

        private IEnumerable<User> Search(IEnumerable<User> users, UserSearchViewModel model)
        {
            // companies
            LocalitySelectViewModel localityModel = model.LocalitySelectViewModel;
            if (localityModel != null && localityModel.SelectedList.Any())
            {
                int localitiesNo = localityModel.SelectedList.Count;
                IEnumerable<int> localities = localityModel.SelectedList.Select(x => x.ID);
                users = users.Where(u => u.UserLocalityRoles.Select(x => x.LocalityID).Distinct().Intersect(localities).Count() == localitiesNo);
            }

            // roles
            RoleSelectViewModel roleModel = model.RoleSelectViewModel;
            if (roleModel != null && roleModel.SelectedList.Any())
            {
                int rolesCount = roleModel.SelectedList.Count;
                IEnumerable<int> roles = roleModel.SelectedList.Select(x => x.ID);
                users = users.Where(u => u.UserLocalityRoles.Select(x => x.RoleID).Distinct().Intersect(roles).Count() == rolesCount);
            }

            // information
            UserViewModel user = model.UserViewModel;
            if (user != null)
            {
                if (!string.IsNullOrEmpty(user.PersonalNumber))
                    users = users.Where(u => u.PersonalNumber.ToLower().Contains(user.PersonalNumber.ToLower()));

                if (!string.IsNullOrEmpty(user.Login))
                    users = users.Where(u => u.Login.ToLower().Contains(user.Login.ToLower()));

                if (!string.IsNullOrEmpty(user.Title))
                    users = users.Where(u => u.Title.ToLower().Contains(user.Title.ToLower()));

                if (!string.IsNullOrEmpty(user.FirstName))
                    users = users.Where(u => u.FirstName != null && u.FirstName.ToLower().Contains(user.FirstName.ToLower()));

                if (!string.IsNullOrEmpty(user.Surname))
                    users = users.Where(u => u.Surname != null && u.Surname.ToLower().Contains(user.Surname.ToLower()));

                if (!string.IsNullOrEmpty(user.Email))
                    users = users.Where(u => u.Email != null && u.Email.ToLower().Contains(user.Email.ToLower()));

                if (!string.IsNullOrEmpty(user.PhoneNumber))
                    users = users.Where(u => u.PhoneNumber != null && u.PhoneNumber.ToLower().Contains(user.PhoneNumber.ToLower()));

                if (user.DateOfBirth != null)
                    users = users.Where(u => u.DateOfBirth != null && u.DateOfBirth.Equals(user.DateOfBirth));

                users = users.Where(u => u.IsActive == user.IsActive);
            }

            return users;
        }

        #endregion

        #region Updating 

        internal void CreateUser(UserViewModel model, List<UserLocalityRole> localityRoles, string passwordToken)
        {
            localityRoles = model.LocalityWithRoles?.Select(x => new UserLocalityRole { LocalityID = x.LocalityID, RoleID = x.RoleID }).ToList();

            User user = new User
            {
                PersonalNumber = model.PersonalNumber,
                FirstName = model.FirstName,
                Surname = model.Surname,
                Title = model.Title,
                Login = model.Login,
                Email = model.Email,
                PhoneNumber = model.PhoneNumber,
                DateOfBirth = model.DateOfBirth,
                IsActive = model.IsActive,
                PasswordToken = passwordToken,
                UserLocalityRoles = localityRoles
            };

            Add(user);
            Save();
        }

        // use for user
        internal void UpdateUser(User user, UserViewModel model)
        {
            user.PersonalNumber = model.PersonalNumber;
            user.FirstName = model.FirstName;
            user.Surname = model.Surname;
            user.Title = model.Title;
            user.Login = model.Login;
            user.Email = model.Email;
            user.PhoneNumber = model.PhoneNumber;
            user.DateOfBirth = model.DateOfBirth;
            user.IsActive = model.IsActive;

            user.UserLocalityRoles.Clear();
            user.UserLocalityRoles = model.LocalityWithRoles?.Select(x => new UserLocalityRole { LocalityID = x.LocalityID, RoleID = x.RoleID }).ToList();

            Update(user);
            Save();
        }

        // used for profile
        internal void UpdateUser(int ID, EditProfileViewModel model)
        {
            User user = Get(ID);
            // change user information
            user.FirstName = model.User.FirstName;
            user.Surname = model.User.Surname;
            user.Title = model.User.Title;
            user.Login = model.User.Login;
            user.Email = model.User.Email;
            user.PhoneNumber = model.User.PhoneNumber;
            user.DateOfBirth = model.User.DateOfBirth;
            Update(user);

            // change password
            if (model.ChangePasswordViewModel.ChangePassword)
            {
                ChangePassword(user, model.ChangePasswordViewModel.Password);
            }

            Save();
        }

        public bool ResetPassword(string guid, string password)
        {
            User user = _userRepository.GetByPasswordToken(guid);
            return ChangePassword(user, password);
        }

        public bool ChangePassword(User user, string password)
        {
            if (user == null || password == null)
                return false;

            user.PasswordToken = null;
            user.Password = Encryption.GetSHA256Hash(password, user.Login);
            Change(user);
            return true;
        }

        public void SetPasswordToken(string email, string passwordToken)
        {
            User user = GetUserByEmail(email);
            user.PasswordToken = passwordToken;
            Change(user);
        }

        #endregion
    }
}