﻿using GeneratorOfReports.Authentication;
using GeneratorOfReports.Models;
using GeneratorOfReports.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeneratorOfReports.Services
{
    public class Service<T> : IService<T> where T : class
    {
        #region Properties

        protected UnitOfWork _unitOfWork;
        protected IRepository<T> _repository;

        // For managing log for every change that was made in the context
        private readonly AuditRepository _auditRepository;

        #endregion

        #region Constructor

        public Service(UnitOfWork unitOfWork, IRepository<T> repository)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
            _auditRepository = unitOfWork.AuditRepository;
        }

        #endregion

        #region Base methods implementation

        public virtual T Get(int id) => _repository.Get(id);

        public virtual IEnumerable<T> GetAll() => _repository.GetAll();

        public virtual void Add(T entity)
        {
            _repository.Add(entity);
            _auditRepository.Add(Created(entity));
            Save();
        }

        public virtual void AddRange(IEnumerable<T> entities)
        {
            _repository.AddRange(entities);
            _auditRepository.AddRange(entities.Select(entity => Created(entity)));
            Save();
        }

        public virtual void Remove(T entity) => _repository.Remove(entity);

        public virtual void RemoveRange(IEnumerable<T> entities) => _repository.RemoveRange(entities);

        // Update entity and add to audit log all information about changes
        public virtual void Update(T entity)
        {
            _repository.Update(entity);
            _auditRepository.AddRange(UpdatedFields(entity));
        }

        public virtual void Save() => _unitOfWork.Commit();

        public virtual void Change(T entity)
        {
            _repository.Update(entity);
            Save();
        }

        #endregion

        #region Audits

        private IEnumerable<Audit> UpdatedFields(T entity)
        {
            var dbEntityEntry = _repository.GetEntry(entity);
            List<Audit> updatedFields = new List<Audit>();
            int userID = (HttpContext.Current.User as Principal).ID;
            int ID = 0;

            foreach (var property in dbEntityEntry.OriginalValues.PropertyNames)
            {
                var oldValue = dbEntityEntry.OriginalValues.GetValue<object>(property);
                var newValue = dbEntityEntry.CurrentValues.GetValue<object>(property);

                if (property.Equals("ID")) ID = (int)oldValue;

                if (oldValue != null && !oldValue.Equals(newValue))
                    updatedFields.Add(new Audit
                    {
                        Table = entity.GetType().BaseType.Name,
                        Field = property,
                        RecordID = ID,
                        UserID = userID,
                        OldValue = oldValue?.ToString(),
                        NewValue = newValue?.ToString(),
                        Date = DateTime.Now,
                        Action = (short)AuditService.Action.Edit
                    });
            }
            return updatedFields;
        }

        private Audit Created(T entity)
        {
            int userID = (HttpContext.Current.User as Principal).ID;
            return new Audit
            {
                Table = entity.GetType().Name,
                RecordID = (int)entity.GetType().GetProperty("ID").GetValue(entity),
                UserID = userID,
                Date = DateTime.Now,
                Action = (short)AuditService.Action.Create
            };
        }

        #endregion
    }
}