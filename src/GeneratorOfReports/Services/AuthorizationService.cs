﻿using GeneratorOfReports.Models;
using GeneratorOfReports.Repositories;
using GeneratorOfReports.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace GeneratorOfReports.Services
{
    public class AuthorizationService : Service<Authorization>
    {
        #region Properties

        private readonly AuthorizationRepository _authorizationRepository;

        #endregion

        #region Constructor

        public AuthorizationService(UnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.AuthorizationRepository)
        {
            _authorizationRepository = unitOfWork.AuthorizationRepository;
        }

        #endregion

        #region Getters

        internal Authorization GetDenyAll()
            => GetOrAdd(false, false, false);

        internal Authorization GetAllowAll()
            => GetOrAdd(true, true, true);

        internal AuthorizationQuery GetAuthorizationQuery(AuthorizationViewModel x)
            => new AuthorizationQuery
            {
                Authorization = GetOrAdd(x.Visibility, x.Edit, x.Remove),
                LocalityID = x.LocalityID
            };

        internal AuthorizationReport GetAuthorizationReport(AuthorizationViewModel x)
          => new AuthorizationReport
          {
              Authorization = GetOrAdd(x.Visibility, x.Edit, x.Remove),
              LocalityID = x.LocalityID
          };

        internal Authorization GetOrAdd(bool visibility, bool edit, bool remove)
        {
            Authorization authorization = _authorizationRepository.Get(visibility, edit, remove);
            if (authorization == null)
            {
                authorization = new Authorization { Edit = edit, Remove = remove, Visibility = visibility || edit || remove };
                Add(authorization);
            }
            return authorization;
        }

        internal AuthorizationQuery CreatorQueryAuthorization(int localityID)
            => new AuthorizationQuery
            {
                Authorization = GetAllowAll(),
                LocalityID = localityID
            };

        internal AuthorizationReport CreatorReportAuthorization(int localityID)
           => new AuthorizationReport
           {
               Authorization = GetAllowAll(),
               LocalityID = localityID
           };

        #endregion

        #region Formators

        internal AuthorizationViewModel CreateAuthorization(Authorization x, string locality)
            => new AuthorizationViewModel
            {
                ID = x.ID,
                Locality = locality,
                All = x.Visibility && x.Edit && x.Remove,
                Visibility = x.Visibility,
                Edit = x.Edit,
                Remove = x.Remove
            };

        internal List<AuthorizationQuery> CreateQueryAuthorization(IEnumerable<AuthorizationViewModel> authorization, int localityID)
        {
            List<AuthorizationQuery> queryAuthorizations = authorization
                .Where(x=> x.Visibility)
                .Select(x => GetAuthorizationQuery(x))
                .ToList();
            queryAuthorizations.Add(CreatorQueryAuthorization(localityID));
            return queryAuthorizations;
        }

        internal List<AuthorizationReport> CreateAuthorizationReports(List<AuthorizationViewModel> authorization, int localityID)
        {
            var reportAuthorizations = authorization
                .Where(x => x.Visibility)
                .Select(x => GetAuthorizationReport(x))
                .ToList();
            reportAuthorizations.Add(CreatorReportAuthorization(localityID));
            return reportAuthorizations.ToList();
        }

        internal List<AuthorizationQuery> GetAuthorizationQueries(IEnumerable<AuthorizationViewModel> modelAuthorization, int localityID)
        {
            var authorizations = new List<AuthorizationQuery>();
            if (modelAuthorization != null && modelAuthorization.Any())
            {
                authorizations = modelAuthorization.Select(x => GetAuthorizationQuery(x)).ToList();
            }
            authorizations.Add(CreatorQueryAuthorization(localityID));
            return authorizations.ToList();
        }

        #endregion
    }
}