﻿using GeneratorOfReports.Models;
using GeneratorOfReports.Utilities;
using GeneratorOfReports.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GeneratorOfReports.Services
{
    public class RoleService : Service<Role>
    {

        public RoleService(UnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.RoleRepository) { }

        #region Getters 

        internal List<SelectListItem> GetSelectList()
             => GetAll().Select(role => new SelectListItem
             {
                 Value = role.ID.ToString(),
                 Text = FormattingUtility.GetRole(role.Name)
             }).ToList();

        internal List<SelectListItem> GetSelectListExcept(IEnumerable<string> exceptList)
             => GetSelectList().Where(role => !exceptList.Contains(role.Value)).ToList();

        internal List<SelectListItem> GetSelectListExcept(List<RoleViewModel> roles)
        {
            if (roles == null)
            {
                return GetSelectList();
            }
            List<string> exceptList = roles.Select(x => x.ID.ToString()).ToList();
            return GetSelectListExcept(exceptList);
        }

        #endregion

        internal List<RoleViewModel> ConvertToViewModel(List<Role> roles)
            => roles.Select(role => new RoleViewModel { ID = role.ID, Name = role.Name }).ToList();

    }
}