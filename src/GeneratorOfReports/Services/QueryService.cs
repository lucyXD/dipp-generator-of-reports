﻿using GeneratorOfReports.ContextManagement;
using GeneratorOfReports.Models;
using GeneratorOfReports.ReportGenerator;
using GeneratorOfReports.Utilities;
using GeneratorOfReports.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using GeneratorOfReports.Resources;
using GeneratorOfReports.ParameterManagement;

namespace GeneratorOfReports.Services
{
    public class QueryService : Service<Query>
    {
        #region Constructor

        public QueryService(UnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.QueryRepository) { }

        #endregion

        #region Getters

        internal IEnumerable<Query> GetVisibleQueries(int localityID)
            => GetAll().Where(x => x.AuthorizationQueries.Any(y => y.LocalityID == localityID));

        internal IEnumerable<AuthorizationQuery> GetAuthorizations(int ID)
            => Get(ID)?.AuthorizationQueries;

        #endregion

        #region Searching

        internal IEnumerable<Query> Search(QuerySearchViewModel model, string sortColumn, string sortColumnDir)
           => Search(GetAll(), model, sortColumn, sortColumnDir);

        internal IEnumerable<Query> Search(IEnumerable<Query> queries, QuerySearchViewModel model, string sortColumn, string sortColumnDir)
        {
            if (model != null)
            {
                if (!string.IsNullOrEmpty(model.Name))
                    queries = queries.Where(x => x.Name.ToLower().Contains(model.Name.ToLower()));

                if (model.Valid != null)
                    queries = queries.Where(x => x.Valid == model.Valid);

                if (!string.IsNullOrEmpty(model.Database))
                    queries = queries.Where(x => x.Database.ToLower().Contains(model.Database.ToLower()));
            }

            queries = (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDir))
                         ? queries.AsEnumerable().OrderBy(sortColumn + " " + sortColumnDir).ToList()
                         : queries.AsEnumerable().OrderBy("ID asc").ToList();

            return queries;
        }

        #endregion

        #region Validation

        internal bool ValidCreateQueries(List<QueryReportViewModel> queries)
            => queries.Any() && queries.TrueForAll(x => x.Count > 0);

        #region Correct form of query

        internal Error CheckQuery(QueryModifyViewModel model)
            => model == null || model.Query == null
                ? new Error(Translation.Query_Database, Translation.Query_Database_ErrorMessage_Unknown, Translation.Query_Database)
                : CheckQuery(model.Query.Text, model.Query.Database, model.Parameters);

        internal Error CheckQuery(string text, string database, IEnumerable<ParameterViewModel> parameters)
        {
            // check valid DB
            if (!ContextType.Know<ContextType>(database))
            {
                return new Error(Translation.Query_Database, Translation.Query_Database_ErrorMessage_Unknown, Translation.Query_Database);
            }
            // check query (correct form or affected context)
            ContextType context = ContextWrapper.GetValue<ContextType>(database);
            string query = ProcessQueryDebug(text, parameters);
            (bool correctForm, Error error) = context.IsCorrect(query);
            if (!correctForm)
            {
                return error;
            }

            return null;
        }

        internal object EvaluateQuery(string text, string database, List<ParameterViewModel> parameters)
        {
            ContextType context = ContextWrapper.GetValue<ContextType>(database);
            string query = ProcessQueryDebug(text, parameters);
            (bool correctForm, _) = context.IsCorrect(query);
            if (correctForm)
            {
                return context.GetDataTable(query);
            }
            return null;
        }

        #endregion

        #region Check Locality Authorization

        internal bool CanRemove(Query query, int localityID)
            => query.AuthorizationQueries.Any(x => x.LocalityID == localityID && x.Authorization.Remove);

        internal bool CanEdit(Query query, int localityID)
            => query.AuthorizationQueries.Any(x => x.LocalityID == localityID && x.Authorization.Edit);

        #endregion

        #endregion

        #region Modifiers

        internal bool Remove(int ID, int localityID)
        {
            Query query = Get(ID);
            if (query != null && CanRemove(query, localityID))
            {
                Remove(query);
                Save();
                return true;
            }
            return false;
        }

        internal Query Create(QueryModifyViewModel model, List<AuthorizationQuery> authorizations, int userID)
        {
            // Parameters settings
            List<Parameter> parameters = model.Parameters?
                .Where(x => x.IsParameter)
                .Select(x => CreateParameter(x)).ToList();
            // Query settings
            Query query = new Query
            {
                Name = model.Query.Name,
                Database = model.Query.Database,
                Description = model.Query.Description,
                Text = model.Query.Text,
                Valid = model.Query.Valid,
                Parameters = parameters,
                AuthorizationQueries = authorizations,
                UserCreateID = userID,
                TimeCreate = DateTime.Now
            };

            Add(query);
            Save();
            return query;
        }

        internal void Update(QueryModifyViewModel model, List<AuthorizationQuery> authorizations)
        {
            Query query = Get(model.Query.ID);

            query.AuthorizationQueries.Clear();
            query.AuthorizationQueries = authorizations;

            // query parameters
            query.Parameters.Clear();
            ICollection<Parameter> parameters = model.Parameters?
                .Where(x => x.IsParameter)
                .Select(x => CreateParameter(x)).ToList();

            // report
            query.Name = model.Query.Name;
            query.Description = model.Query.Description;
            query.Valid = model.Query.Valid;
            query.Text = model.Query.Text;
            query.Parameters = parameters;

            Update(query);
            Save();
        }

        #endregion

        #region Formators

        #region ViewModels

        internal QueryViewModel CreateQueryViewModel(Query query)
        => new QueryViewModel
        {
            ID = query.ID,
            Name = query.Name,
            Description = query.Description,
            Database = query.Database,
            Valid = query.Valid,
            Text = query.Text,
            ValidWord = FormattingUtility.GetBooleanWord(query.Valid),
            TimeCreate = FormattingUtility.GetDay(query.TimeCreate)
        };

        internal IEnumerable<ReportQueryViewModel> CreateReportQueryViewModel(IEnumerable<ReportQuery> reportQueries)
        {
            foreach (var x in reportQueries)
            {
                for (int i = 0; i < x.Count; i++)
                    yield return new ReportQueryViewModel
                    {
                        Query = CreateQueryViewModel(x.Query),
                        Parameters = CreateParameters(x.Query.Parameters)
                    };
            }
        }

        internal QueryDetailsViewModel CreateQueryDedails(int ID, int localityID)
        {
            Query query = Get(ID);
            bool canEdit = CanEdit(query, localityID);
            QueryDetailsViewModel model = new QueryDetailsViewModel
            {
                Query = CreateQueryViewModel(query),
                CanEdit = canEdit,
                Parameters = CreateParameters(query.Parameters),
                Authorization = canEdit ? query.AuthorizationQueries.Select(x => CreateAuthorization(x.Authorization, x.Locality.Name)).ToList() : null
            };
            return model;
        }

        #region Modify ViewModel

        internal QueryModifyViewModel CreateModifyViewModel(int ID, int localityID)
        {
            Query query = Get(ID);
            if (query != null && CanEdit(query, localityID))
            {
                return new QueryModifyViewModel
                {
                    Query = CreateQueryViewModel(query),
                    Database = ContextWrapper.GetValue<ContextType>(query.Database).GetDB(),
                    Parameters = CreateParameters(query.Parameters),
                    Authorization = query.AuthorizationQueries.Select(x => CreateAuthorization(x.Authorization, x.Locality.Name))
                };
            }
            return null;
        }

        private IEnumerable<ParameterViewModel> CreateParameters(ICollection<Parameter> parameters)
            => parameters.Select(x => CreateParameterViewModel(x));

        private ParameterViewModel CreateParameterViewModel(Parameter parameter)
            => new ParameterViewModel
            {
                ID = parameter.ID,
                Name = parameter.Name,
                Type = ParameterTypes.GetValue(parameter.Type).Name,
                SQLTypeSelectList = ParameterTypes.GetSelectList(),
                Keyword = parameter.Type,
                Hash = Guid.NewGuid().ToString(),
                Description = parameter.Description,
                DefaultValue = ParameterTypes.GetValue(parameter.Type).Type.DisplayDefault(parameter.DefaultValue),
                DefaultValueModel = parameter.DefaultValue,
         //       ModelJson = parameter.DefaultValue,
                IsParameter = true,
                Required = parameter.Required,
                RequiredWord = FormattingUtility.GetBooleanWord(parameter.Required)
            };

        #endregion

        private AuthorizationViewModel CreateAuthorization(Authorization x, string locality)
            => new AuthorizationViewModel
            {
                ID = x.ID,
                Locality = locality,
                All = x.Visibility && x.Edit && x.Remove,
                Visibility = x.Visibility,
                Edit = x.Edit,
                Remove = x.Remove
            };

        #region Parameter for query

        private Parameter CreateParameter(ParameterViewModel parameter)
            => new Parameter
            {
                Name = parameter.Name,
                Type = parameter.Keyword,
                Description = parameter.Description,
                DefaultValue = parameter.ModelJson,
                Required = parameter.Required
            };

        #endregion

        #endregion

        #region POCO

        internal IEnumerable<GeneratedQuery> CreateGeneratedQuery(IEnumerable<ReportQueryViewModel> queries)
            => queries.Select(x =>
            {
                var query = Get(x.Query.ID);
                return new GeneratedQuery
                {
                    Database = query.Database,
                    Text = ProcessQuery(query.Text, x.Parameters)
                };
            });

        private string ProcessQuery(string text, IEnumerable<ParameterViewModel> parameters)
        {
            if (parameters == null) return text;
            foreach (var p in parameters)
            {
                var type = ParameterTypes.GetValue(p.Keyword).Type;
                if (p.UseDefaultVaue)
                {
                    text = type.DefaultParameter(text, p.Name, p.DefaultValueModel);
                }
                else if (type.IsEmpty(p.ModelJson))
                {
                    text = type.DisappearParameter(text, p.Name);
                }
                else
                {
                    text = type.ProcessParameter(text, p.Name, p.ModelJson);
                }
            }
            return text;
        }

        private string ProcessQueryDebug(string text, IEnumerable<ParameterViewModel> parameters)
        {
            if (parameters == null) return text;
            foreach (var p in parameters)
            {
                if (p.IsParameter)
                {
                    var type = ParameterTypes.GetValue(p.Keyword).Type;
                    if (type.IsEmptyDefaultValue(p.ModelJson))
                    {
                        text = type.DisappearParameter(text, p.Name);
                    }
                    else
                    {
                        text = type.DefaultParameter(text, p.Name, p.ModelJson);
                    }
                }
            }
            return text;
        }

        #endregion

        #endregion
    }
}