﻿using GeneratorOfReports.Models;
using GeneratorOfReports.ParameterManagement;
using GeneratorOfReports.Resources;
using GeneratorOfReports.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace GeneratorOfReports.Services
{
    public class ParameterService : Service<Parameter>
    {
        #region Constructor

        public ParameterService(UnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.ParameterRepository) { }

        #endregion

        #region Validators

        internal bool ValidParameters(IEnumerable<ParameterViewModel> parameters)
            => parameters == null ||
               parameters.ToList().TrueForAll(x =>
              {
                  (bool valid, _) = ValidParametr(x);
                  return valid;
              });

        internal (bool, string) ValidParametr(ParameterViewModel model)
        {
            Parameter parameter = Get(model.ID);

            if (parameter == null)
                return (false, Translation.Parameter_ErrorMessage_Unknown);

            ParameterType type = ParameterTypes.GetValue(parameter.Type).Type;

            if (model.UseDefaultVaue)
            {
                if (type.IsEmptyDefaultValue(model.DefaultValueModel))
                {
                    return (false, Translation.Parameter_ErrorMessage_UseDefaultNull);
                }
                else
                {
                    return (true, "");
                }
            }

            if (!type.IsValid(model.ModelJson))
                return (false, Translation.Parameter_ErrorMessage_WrongValue);

            bool isEmpty = type.IsEmpty(model.ModelJson);
            if (parameter.Required && isEmpty)
                return (false, Translation.Parameter_ErrorMessage_Required);

            return (true, "");
        }

        #endregion
    }
}
