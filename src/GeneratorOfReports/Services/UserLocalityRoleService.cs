﻿using GeneratorOfReports.Models;
using GeneratorOfReports.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace GeneratorOfReports.Services
{
    public class UserLocalityRoleService : Service<UserLocalityRole>
    {
        private readonly UserLocalityRoleRepository _userLocalityRoleRepository;

        public UserLocalityRoleService(UnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.UserLocalityRoleRepository)
        {
            _userLocalityRoleRepository = unitOfWork.UserLocalityRoleRepository;
        }

        #region Getters 

        public List<UserLocalityRole> GetByUserID(int userID)
            => _userLocalityRoleRepository.GetByUserID(userID);

        #endregion

        #region Update 

        internal void Update(int userID, List<UserLocalityRole> localityWithRoles)
        {
            List<UserLocalityRole> all = _userLocalityRoleRepository.GetByUserID(userID);
            _userLocalityRoleRepository.RemoveRange(all);
            if (localityWithRoles != null)
            {
                localityWithRoles.ForEach(x => _userLocalityRoleRepository.Add(new UserLocalityRole
                {
                    LocalityID = x.LocalityID,
                    UserID = userID,
                    RoleID = x.RoleID
                }));
            }
        }

        #endregion

        #region Formattors

        internal Dictionary<Locality, List<Role>> FormatLocalityWithRoles(List<UserLocalityRole> localityWithRoles)
         => localityWithRoles
                .GroupBy(x => x.Locality)
                .ToDictionary(g => g.Key, g => g.Select(x => x.Role).ToList());

        #endregion
    }
}