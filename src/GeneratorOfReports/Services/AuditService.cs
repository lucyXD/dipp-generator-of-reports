﻿using GeneratorOfReports.Models;
using GeneratorOfReports.Repositories;
using GeneratorOfReports.Resources;
using GeneratorOfReports.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GeneratorOfReports.Services
{
    public class AuditService : Service<Audit>
    {
        /// <summary>
        /// Enumeration used to express an action related to reports within the application.
        /// </summary>
        public enum Action : short
        {
            Create = 1,
            Edit = 2
        }

        private readonly AuditRepository _auditRepository;

        public AuditService(UnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.AuditRepository)
        {
            _auditRepository = unitOfWork.AuditRepository;
        }

        #region Getters

        private const int pageSize = 20;

        internal IEnumerable<Audit> GetByID(int ID)
            => _auditRepository.GetByUserID(ID);

        internal IEnumerable<SelectListItem> GetActions()
            => new SelectList(Enum.GetValues(typeof(Action)).Cast<Action>().Select(item => new SelectListItem
            {
                Text = item.ToString(),
                Value = ((short)item).ToString()
            }).ToList(), "Value", "Text");

        public List<Audit> GetAll(int? pageNumber, IEnumerable<Audit> audits)
        {
            var skip = pageNumber * pageSize;
            return audits.Skip(Convert.ToInt32(skip)).Take(pageSize).ToList();
        }

        #endregion

        #region Searching

        public IEnumerable<Audit> Search(int ID, SearchAuditsViewModel model)
        {
            var audits = GetByID(ID);

            if (model != null)
            {
                if (model.ActionDateFrom != null)
                    audits = audits.Where(x => x.Date >= model.ActionDateFrom).ToList();

                if (model.ActionDateTo != null)
                    audits = audits.Where(x => x.Date <= model.ActionDateTo);

                if (model.Action != null)
                    audits = audits.Where(x => x.Action == model.Action);
            }

            return audits.ToList();
        }

        #endregion

        #region Formators

        internal Dictionary<string, List<string>> Format(IEnumerable<Audit> audits)
        {
            Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();

            foreach (Audit audit in audits)
            {
                string date = audit.Date.ToString("MM/dd/yyyy HH:mm");
                if (!result.ContainsKey(date)) result[date] = new List<string>();
                result[date].Add(Format(audit));
            }

            return result;
        }

        internal SearchAuditsViewModel CreateSearchAuditsViewModel(int userID)
            => new SearchAuditsViewModel
            {
                Actions = GetActions(),
                AuditsViewModel = new AuditsViewModel
                {
                    HistoryOfActions = Format(GetByID(userID))
                }
            };

        internal string Format(Audit audit)
        {
            switch (audit.Action)
            {
                case (short)Action.Create:
                    return string.Format(Translation.Audit_AddText, audit.Table, audit.RecordID);
                case (short)Action.Edit:
                    return string.Format(Translation.Audit_EditText, audit.Table, audit.Field, audit.OldValue, audit.NewValue);
                default:
                    return Translation.Global_Error;
            }
        }

        #endregion
    }
}