\contentsline {lstlisting}{\numberline {1}Pridanie spojenia na~dátový model}{35}{lstlisting.1}%
\contentsline {lstlisting}{\numberline {2}Zadefinovanie regexu na~rozpoznanie parametrov}{36}{lstlisting.2}%
\contentsline {lstlisting}{\numberline {3}Ukážka prvej možnosti riešenia parametrov}{37}{lstlisting.3}%
\contentsline {lstlisting}{\numberline {4}Ukážka druhej možnosti riešenia parametrov}{37}{lstlisting.4}%
\contentsline {lstlisting}{\numberline {5}Možné implementácie parametru áno/nie}{38}{lstlisting.5}%
\contentsline {lstlisting}{\numberline {6}Implementácia rozhrania pre~typ parametru}{39}{lstlisting.6}%
\contentsline {lstlisting}{\numberline {7}Ukážka nepovinných parametrov}{40}{lstlisting.7}%
\contentsline {lstlisting}{\numberline {8}Ukážka nahradzovania parametrov}{42}{lstlisting.8}%
\contentsline {lstlisting}{\numberline {9}Dopyt nepoužiteľný pre~reportovanie}{44}{lstlisting.9}%
\contentsline {lstlisting}{\numberline {10}Príklady základných chýb a~chybových hlášok}{46}{lstlisting.10}%
\contentsline {lstlisting}{\numberline {11}Príklady dopytov s~parametrom typu pozitívne číslo}{62}{lstlisting.11}%
\contentsline {lstlisting}{\numberline {12}Príklady dopytov s~parametrom obdobie pre~dátum}{63}{lstlisting.12}%
\contentsline {lstlisting}{\numberline {13}Príklady dopytov s~číselným parametrom pre~podmienky}{63}{lstlisting.13}%
\contentsline {lstlisting}{\numberline {14}Príklady dopytov s~textovým parametrom pre~podmienky}{64}{lstlisting.14}%
\contentsline {lstlisting}{\numberline {15}Príklady dopytov pre~obecný číselník nad~modelom}{64}{lstlisting.15}%
\contentsline {lstlisting}{\numberline {16}Príklady dopytov pre~logický parameter True/False}{65}{lstlisting.16}%
\contentsline {lstlisting}{\numberline {17}Príklady dopytov s~obecným výberom}{65}{lstlisting.17}%
\contentsline {lstlisting}{\numberline {18}Príklad dopytu s~viacerými typmi parametrov s ich nahradením}{65}{lstlisting.18}%
