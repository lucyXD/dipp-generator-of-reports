\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {slovak}{}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Styly pro psaní bakalářských a diplomových prací}{7}{section.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}Požadavky a podprovaná prostředí}{7}{subsection.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2}Přepínače}{7}{subsection.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3}Geometrie stránky}{7}{subsection.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Sazba částí dokumentu}{9}{section.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Sazba úvodní strany či obsahu}{9}{subsection.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Závěry}{9}{subsection.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}Matematika}{9}{subsection.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4}Sazba literatury}{10}{subsection.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.4.1}Sazba bibliografie přes \textsc {Bib}\LaTeX {}}{10}{subsubsection.2.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.4.2}Manuální sazba bibliografie}{11}{subsubsection.2.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5}Drobná makra}{11}{subsection.2.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6}Sazba rejstříku}{11}{subsection.2.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.7}Sazba zdrojových kódů}{11}{subsection.2.7}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{Z\'aver}{16}{section*.6}%
\babel@toc {slovak}{}
\babel@toc {slovak}{}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Conclusions}{17}{section*.8}%
\babel@toc {english}{}
\babel@toc {slovak}{}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A}První příloha}{18}{appendix.A}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {B}Druhá příloha}{18}{appendix.B}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {C}Obsah přiloženého CD/DVD}{18}{appendix.C}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{Zoznam skratiek}{20}{section*.10}%
