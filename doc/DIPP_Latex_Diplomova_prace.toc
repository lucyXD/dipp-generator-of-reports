\babel@toc {slovak}{}
\contentsline {section}{\numberline {1}Úvod}{10}{section.1}%
\contentsline {section}{\numberline {2}Cieľ}{11}{section.2}%
\contentsline {section}{\numberline {3}Analýza existujúcich riešení}{12}{section.3}%
\contentsline {subsection}{\numberline {3.1}Pôvodný OGreport}{12}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Komerčné riešenia}{13}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Google Data Studio}{14}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}AutoGPS}{14}{subsubsection.3.2.2}%
\contentsline {subsection}{\numberline {3.3}Zhrnutie}{14}{subsection.3.3}%
\contentsline {section}{\numberline {4}Nové riešenie}{15}{section.4}%
\contentsline {subsection}{\numberline {4.1}Aktéri systému}{15}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Vlastnosti aplikácie}{16}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Správca dopytov}{16}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}Správca reportov}{17}{subsubsection.4.2.2}%
\contentsline {subsubsection}{\numberline {4.2.3}Správca užívateľov}{18}{subsubsection.4.2.3}%
\contentsline {section}{\numberline {5}Návrh aplikácie}{19}{section.5}%
\contentsline {subsection}{\numberline {5.1}UML}{19}{subsection.5.1}%
\contentsline {subsubsection}{\numberline {5.1.1}Diagramy prípadov použitia}{19}{subsubsection.5.1.1}%
\contentsline {subsubsection}{\numberline {5.1.2}Diagramy tried}{22}{subsubsection.5.1.2}%
\contentsline {subsection}{\numberline {5.2}Dátový model}{24}{subsection.5.2}%
\contentsline {section}{\numberline {6}Použité technológie}{26}{section.6}%
\contentsline {subsection}{\numberline {6.1}ASP.NET MVC}{26}{subsection.6.1}%
\contentsline {subsubsection}{\numberline {6.1.1}ASP.NET}{26}{subsubsection.6.1.1}%
\contentsline {subsubsection}{\numberline {6.1.2}MVC architektúra}{26}{subsubsection.6.1.2}%
\contentsline {subsubsection}{\numberline {6.1.3}ASP.NET MVC 5}{27}{subsubsection.6.1.3}%
\contentsline {subsection}{\numberline {6.2}Repository Pattern}{28}{subsection.6.2}%
\contentsline {subsection}{\numberline {6.3}C\#}{28}{subsection.6.3}%
\contentsline {subsection}{\numberline {6.4}SQL a~MS SQL}{28}{subsection.6.4}%
\contentsline {subsection}{\numberline {6.5}JavaScript}{29}{subsection.6.5}%
\contentsline {subsection}{\numberline {6.6}CSS a~HTML}{30}{subsection.6.6}%
\contentsline {subsection}{\numberline {6.7}NuGet Package Manager}{30}{subsection.6.7}%
\contentsline {section}{\numberline {7}Štruktúra projektu}{31}{section.7}%
\contentsline {section}{\numberline {8}Programátorská dokumentácia}{34}{section.8}%
\contentsline {subsection}{\numberline {8.1}Spracovanie dátových modelov}{34}{subsection.8.1}%
\contentsline {subsubsection}{\numberline {8.1.1}Implementácia}{34}{subsubsection.8.1.1}%
\contentsline {subsubsection}{\numberline {8.1.2}Pridanie nového modelu}{35}{subsubsection.8.1.2}%
\contentsline {subsubsection}{\numberline {8.1.3}Výhody a~nevýhody}{35}{subsubsection.8.1.3}%
\contentsline {subsection}{\numberline {8.2}Parametre}{36}{subsection.8.2}%
\contentsline {subsubsection}{\numberline {8.2.1}Značenie parametrov}{36}{subsubsection.8.2.1}%
\contentsline {subsubsection}{\numberline {8.2.2}Implementácia}{36}{subsubsection.8.2.2}%
\contentsline {subsubsection}{\numberline {8.2.3}Nepovinné parametre}{39}{subsubsection.8.2.3}%
\contentsline {subsubsection}{\numberline {8.2.4}Vytváranie parametrov}{39}{subsubsection.8.2.4}%
\contentsline {subsubsection}{\numberline {8.2.5}Kontrola parametrov}{40}{subsubsection.8.2.5}%
\contentsline {subsubsection}{\numberline {8.2.6}Typy parametrov}{40}{subsubsection.8.2.6}%
\contentsline {subsubsection}{\numberline {8.2.7}Výhody a~nevýhody}{41}{subsubsection.8.2.7}%
\contentsline {subsection}{\numberline {8.3}Spracovanie dopytov}{43}{subsection.8.3}%
\contentsline {subsubsection}{\numberline {8.3.1}Tvar dopytu}{43}{subsubsection.8.3.1}%
\contentsline {subsubsection}{\numberline {8.3.2}Kontrola dopytu}{43}{subsubsection.8.3.2}%
\contentsline {subsubsection}{\numberline {8.3.3}Autorizácia dopytu}{45}{subsubsection.8.3.3}%
\contentsline {subsubsection}{\numberline {8.3.4}Výhody a~nevýhody}{45}{subsubsection.8.3.4}%
\contentsline {subsection}{\numberline {8.4}Zostava}{47}{subsection.8.4}%
\contentsline {subsubsection}{\numberline {8.4.1}Oprávnenie zostáv}{47}{subsubsection.8.4.1}%
\contentsline {subsubsection}{\numberline {8.4.2}Formáty}{47}{subsubsection.8.4.2}%
\contentsline {subsection}{\numberline {8.5}Spracovanie akcií}{48}{subsection.8.5}%
\contentsline {subsection}{\numberline {8.6}Užívateľské rozhranie}{48}{subsection.8.6}%
\contentsline {section}{\numberline {9}Užívateľská dokumentácia}{49}{section.9}%
\contentsline {subsection}{\numberline {9.1}Každý užívateľ}{49}{subsection.9.1}%
\contentsline {subsubsection}{\numberline {9.1.1}Správa profilu}{49}{subsubsection.9.1.1}%
\contentsline {subsection}{\numberline {9.2}Administrátor zostáv}{50}{subsection.9.2}%
\contentsline {subsubsection}{\numberline {9.2.1}Prehľad zostáv}{50}{subsubsection.9.2.1}%
\contentsline {subsubsection}{\numberline {9.2.2}Detaily zostavy}{51}{subsubsection.9.2.2}%
\contentsline {subsubsection}{\numberline {9.2.3}Vytvorenie zostavy}{51}{subsubsection.9.2.3}%
\contentsline {subsubsection}{\numberline {9.2.4}Generovanie zostavy}{52}{subsubsection.9.2.4}%
\contentsline {subsubsection}{\numberline {9.2.5}Upravenie zostavy}{53}{subsubsection.9.2.5}%
\contentsline {subsubsection}{\numberline {9.2.6}Zmena poradia zostavy}{53}{subsubsection.9.2.6}%
\contentsline {subsubsection}{\numberline {9.2.7}Odstránenie zostavy}{54}{subsubsection.9.2.7}%
\contentsline {subsection}{\numberline {9.3}Administrátor dopytov}{54}{subsection.9.3}%
\contentsline {subsubsection}{\numberline {9.3.1}Prehľad dopytov}{54}{subsubsection.9.3.1}%
\contentsline {subsubsection}{\numberline {9.3.2}Vytvorenie dopytu}{54}{subsubsection.9.3.2}%
\contentsline {subsubsection}{\numberline {9.3.3}Editácia dopytu}{56}{subsubsection.9.3.3}%
\contentsline {subsection}{\numberline {9.4}Administrátor užívateľov}{56}{subsection.9.4}%
\contentsline {subsubsection}{\numberline {9.4.1}Prehľad užívateľov}{57}{subsubsection.9.4.1}%
\contentsline {subsubsection}{\numberline {9.4.2}Vytvorenie užívateľa}{57}{subsubsection.9.4.2}%
\contentsline {section}{Z\'aver}{58}{section*.5}%
\babel@toc {slovak}{}
\babel@toc {slovak}{}
\contentsline {section}{Conclusions}{59}{section*.7}%
\babel@toc {english}{}
\babel@toc {slovak}{}
\contentsline {section}{\numberline {A}Spustenie}{60}{appendix.A}%
\contentsline {subsection}{\numberline {A.1}Potrebné technológie}{60}{subsection.A.1}%
\contentsline {subsection}{\numberline {A.2}Obnovenie databázy}{60}{subsection.A.2}%
\contentsline {subsection}{\numberline {A.3}Sprevádzkovanie aplikácie}{60}{subsection.A.3}%
\contentsline {section}{\numberline {B}Testovanie}{61}{appendix.B}%
\contentsline {subsection}{\numberline {B.1}Testovacie databázy}{61}{subsection.B.1}%
\contentsline {subsubsection}{\numberline {B.1.1}Restaurant}{61}{subsubsection.B.1.1}%
\contentsline {subsubsection}{\numberline {B.1.2}TrainTimetable}{61}{subsubsection.B.1.2}%
\contentsline {subsubsection}{\numberline {B.1.3}LWP\_Kartoteka\_Vyvoj}{62}{subsubsection.B.1.3}%
\contentsline {subsection}{\numberline {B.2}Príklady dopytov}{62}{subsection.B.2}%
\contentsline {section}{\numberline {C}Obsah priloženého CD}{66}{appendix.C}%
\contentsline {section}{Literat\'ura}{67}{section*.9}%
