
SPUSTENIE

-------------------------------------------------------------------------------------------------------------

Potrebné technológie

- Visual Studio (Integrované vývojové prostredie (IDE) pre spustenie a testovanie projektu.
  Dostupné z www.visualstudio.microsoft.com/cs/downloads/. Vhodné verzie od VS 2013. 
  Stačí verzia pre vývojárov dostupná zadarmo.)

- MS SQL a SQL Server (Databázový systém pre správu a ukladanie dát. Zadarmo
  k stiahnutiu na www.microsoft.com/en-us/sql-server/sql-server-downloads, 
  vhodná ľubovoľná verzia z dostupných.)

- Windows (Operačný systém (OS), na ktorom bola aplikácia vyvíjaná. Je možné použiť
  aj iný OS. Napríklad, pre MacOS je VS k dispozícii a MS SQL za použitia Dockeru
  je možnosť sprevádzkovania, prípadne pre získanie vhodného OS použite virtuálny
  stroj slúžiaci na emuláciu počítačového systému.)

-------------------------------------------------------------------------------------------------------------

Obnovenie databázy

1. V MS SQL sa pripojíte k lokálnemu SQL serveru.
2. Pravým tlačidlom kliknete na Databases > Restore Database.
3. V dialógovom okne vyplníte ako zdroj zariadenie a kliknete na tlačidlo s tromi bodkami na zadanie cesty.
4. Otvorí sa dialógové okno, v ňom si vyberiete možnosť Add a následne zadáte cestu k súboru z priloženého CD.
5. Automaticky sa vyplní názov databázy, ktorá sa má obnoviť.
6. Pre obnovu databázy následne stlačíte tlačidlo OK.

Tento proces je nutné vykonať pre zálohu GeneratorOfReports.bak v priečinku /data na priloženom CD. 
Nájdete tam aj zálohy testovacích modelov LWP_Kartoteka_vyvoj.bak, TrainTimetable.bak a Restaurant.bak, kde
podľa svojej voľby obnovte aj testovacie databázy.

-------------------------------------------------------------------------------------------------------------

Sprevádzkovanie aplikácie

V súbore Web.config je nutné nastaviť connection string pre pridanú databázu GeneratorOfReports. 
Spojenie k testovacím databázam nastavíte v src > GeneratorOfReports > GeneratorOfReports > ContextManagement
> Context- Type.cs, kde pôvodné pripojovacie reťazce nahradíte novými. Connection string nájdete v MS SQL,
pri pravom kliknutí na názov databázy a otvorením Details.

-------------------------------------------------------------------------------------------------------------

Testovanie

Prihlasovacie údaje k testovaciemu účtu so všetkými  prístupnými rolami v dvoch lokalitách sú

MENO: admin
HESLO: admin

Na vytvorenie vlastného účtu 
prejdite z role administrátor užívateľov na možnosť vytvoriť užívateľa. Tam vyplníte povinné položky
a zadáte príslušné lokality s rolami, nezabudnite prosím vyplniť správne emailovú adresu, na tú vám 
bude doručený odkaz s nastavením hesla do aplikácie.

-------------------------------------------------------------------------------------------------------------

V prípade komplikácií so spustením ma prosím kontaktujte cez email: lucy.harcekova@gmail.com
